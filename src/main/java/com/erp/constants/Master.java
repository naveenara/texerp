package com.erp.constants;

public enum Master {

	TRANSPORT('t'), QUALITY('q'), UNIT('u'), COLOR('c'), PROCESS('p'), Machine('m');
    
    private Character type;
    
    public static Master getByType(char type) {
    	for(Master m: values()) {
    		if(m.getType() == type)
    			return m;
    	}
    	throw new IllegalArgumentException("No master found for type: "+type);
    }
    
    public Character getType(){
        return this.type;
    }
    
    private Master(char i){
        this.type = i;
    }
}
