package com.erp.controller;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.CompanyEntity;
import com.erp.exception.ApplicationException;
import com.erp.helper.Helper;
import com.erp.rest.model.ErpResponse;
import com.erp.service.GenericService;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/company")
public class CompanyController {
	
	@Resource
	private GenericService genericService;

	@RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<String>> save(@RequestBody CompanyEntity entity, HttpServletRequest req) throws ApplicationException, ParseException{
	        Helper.assertMsg(Helper.isEmpty(entity.getName()), "Company name is required");
	        
	        if(entity.getId() == null) {
	        	genericService.add(entity);
	        }else {
	        	genericService.update(entity);
	        }
	        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
    }
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<CompanyEntity>> getEntity(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
		if(id == null) {
			throw new IllegalArgumentException("Company id is required");
		}
		CompanyEntity entity = this.genericService.findLongOne(new CompanyEntity(), id);
		
		if(entity == null) {
			entity = new CompanyEntity();
		}
		
		return new ResponseEntity<ErpResponse<CompanyEntity>>(new ErpResponse<CompanyEntity>(entity, false,"success", "200"), HttpStatus.OK);
	}
	
}
