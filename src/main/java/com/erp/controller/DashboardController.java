package com.erp.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.annotation.Resource;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.GreyInwardEntity;
import com.erp.entity.LotEntryEntity;
import com.erp.helper.Helper;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.service.ChartsService;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/charts")
public class DashboardController {

	@Resource
	private ChartsService chartsService;
	
	@RequestMapping(value="/grey-inward", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<SortedMap<Date, List<Integer>>>> getGreyInwardDayWiseCount(@RequestParam(value="criteria", required=false) String criteria) {
		Date d = null;
		if(Helper.isNotEmpty(criteria)) {
			d = getDate(criteria);
		}
		
		List<Object[]> counts = chartsService.getDayWiseCount(GreyInwardEntity.class, d, "GREY_INWARD");
		List<Object[]> lotcounts = chartsService.getDayWiseCount(LotEntryEntity.class, d, "LOT_ENTRY");
		
		SortedMap<Date, List<Integer>> combined = combineChart(counts, lotcounts);
		
		return new ResponseEntity<ErpResponse<SortedMap<Date, List<Integer>>>>(new ErpResponse<SortedMap<Date, List<Integer>>>(combined, false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="/dyeing-program", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<SortedMap<Date, List<Integer>>>> getDyeingProgramDayWiseCount(@RequestParam(value="criteria", required=false) String criteria) {
		Date d = null;
		if(Helper.isNotEmpty(criteria)) {
			d = getDate(criteria);
		}
		
		List<Object[]> counts = chartsService.getDayWiseCount(GreyInwardEntity.class, d, "DEYING_PROGRAM_V2");
		List<Object[]> lotcounts = chartsService.getDayWiseCount(LotEntryEntity.class, d, "PROCESS_PACKING_V2");
		
		SortedMap<Date, List<Integer>> combined = combineChart(counts, lotcounts);
		
		return new ResponseEntity<ErpResponse<SortedMap<Date, List<Integer>>>>(new ErpResponse<SortedMap<Date, List<Integer>>>(combined, false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="/tasks", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<Map<String, Integer>>> getTasks(@RequestParam(value="criteria", required=false) String criteria) {
		Date d = null;
		if(Helper.isNotEmpty(criteria)) {
			d = getDate(criteria);
		}
		
		Map<String, Integer> counts = chartsService.getTaskChart(d);
		return new ResponseEntity<ErpResponse<Map<String, Integer>>>(new ErpResponse<Map<String, Integer>>(counts, false,"success", "200"), HttpStatus.OK);
	}
	
	
	private SortedMap<Date, List<Integer>> combineChart(List<Object[]> grey, List<Object[]> lot) {
		
		SortedMap<Date, List<Integer>> op = new TreeMap<>();
		
		grey.forEach(obj -> {
			Date d = (Date) obj[0];
			Integer count = ((BigInteger) obj[1]).intValue();
			
			op.put(d, new ArrayList<Integer>() {
				{
					add(count);
					add(0);
				};
			});
		});
		
		lot.forEach(obj -> {
			Date d = (Date) obj[0];
			Integer count = ((BigInteger) obj[1]).intValue();
			
			if(op.containsKey(d)) {
				List<Integer> greyList = op.get(d);
				greyList.set(1, count);
			}else {
				List<Integer> lotList = Arrays.asList(0, count);
				op.put(d, lotList);
			}
		});
		
		return op;
	}
	
	
	private Date getDate(String criteria) {
		
		Date d = null;
		
		switch(criteria) {
			case "1W":
					d = DateUtils.addDays(new Date(),-7);
				break;
			case "15D":
				d = DateUtils.addDays(new Date(),-15);
				break;
			case "1M":
				d = DateUtils.addMonths(new Date(), -1);
				break;
			case "3M":
				d = DateUtils.addMonths(new Date(),-3);
				break;
			case "6M":
				d = DateUtils.addMonths(new Date(),-6);
				break;
			case "1Y":
				d = DateUtils.addMonths(new Date(),-12);
				break;
			default:
				d = DateUtils.addMonths(new Date(), -1);
				break;
		}
		
		return d;
	}
}
