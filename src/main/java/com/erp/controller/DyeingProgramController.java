package com.erp.controller;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.json.JSONArray;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.DyeingProgmDetailEntity;
import com.erp.entity.DyeingProgmEntity;
import com.erp.exception.ApplicationException;
import com.erp.helper.DateUtil;
import com.erp.helper.DyeingProgramHelper;
import com.erp.helper.Helper;
import com.erp.rest.model.DyeingProgmDetailModel;
import com.erp.rest.model.DyeingProgmModel;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.FabricStockReportModel;
import com.erp.rest.model.GreyInwardModel;
import com.erp.rest.model.Grid;
import com.erp.rest.model.LotEntryModel;
import com.erp.rest.model.UserIO;
import com.erp.rest.service.DyeingProgramService;
import com.erp.rest.service.ReportsService;
import com.erp.service.GenericService;
import com.mysql.jdbc.StringUtils;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/dyeing")
public class DyeingProgramController {
	
	@Resource
	private GenericService genericService;
	
	@Resource
	private DyeingProgramService dpService;
	
	@Resource
	private ReportsService reportsService;
	
	@Transactional
	@RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<String>> saveDyeingProgram(@RequestBody DyeingProgmModel model, HttpServletRequest req) throws ApplicationException, ParseException{
			validateRequest(model);
	        UserIO user = (UserIO) req.getAttribute("user");
	        
	        DyeingProgmEntity entity = DyeingProgramHelper.getDyeingProgramEntity(model, dpService, genericService);
	        
	        if(entity.getId() == null) {
	        	Helper helper = new Helper();
		        String yearFormat = helper.getCurrentYearFormat();
		        
		        entity.setDeleted('0');
		        entity.setCreated_by(user.getUserName());
		        entity.setCreated_at(new Date());
		        entity.setYearFormat(yearFormat);
		        entity.setSerialNo(getSerialNo());
		        entity.setShadeCount(entity.getDetails().size());
		        
	        	genericService.add(entity);
	        }else {
	        	entity.setUpdated_at(new Date());
	        	entity.setUpdated_by(user.getUserName());
	        	entity.setShadeCount(entity.getDetails().size());
	        	genericService.update(entity);
	        }
	        
	        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
    }
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<DyeingProgmModel>> getDyeingProgram(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
		if(id == null) {
			throw new IllegalArgumentException("Dyeing Program ID is required");
		}
		DyeingProgmEntity dyeing = this.dpService.getDyeingProgram(id);
		
		return new ResponseEntity<ErpResponse<DyeingProgmModel>>(new ErpResponse<DyeingProgmModel>(getDyeingProgramIoDeep(dyeing), false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/{serialNo}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<DyeingProgmModel>> getDyeingProgramSerialNo(@PathVariable("serialNo") String serialNo) throws NoSuchAlgorithmException, ApplicationException {
		if(org.springframework.util.StringUtils.isEmpty(serialNo)) {
			throw new IllegalArgumentException("Serial number is required");
		}
		DyeingProgmEntity dyeing = this.genericService.getByField(new DyeingProgmEntity(), "serialNo", serialNo);
		
		if(dyeing == null)
			throw new IllegalArgumentException("No record found for: "+serialNo);
		
		return new ResponseEntity<ErpResponse<DyeingProgmModel>>(new ErpResponse<DyeingProgmModel>(getDyeingProgramIo(dyeing), false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="/nextserialNo", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<String>> getSerialNumber() {
    	String nexVal = getSerialNo();
		return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>(nexVal, false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public ResponseEntity<Grid<List<DyeingProgmModel>>> getDyeingProgramList(
							@RequestParam("from") int from,
							@RequestParam("rows") int rows,
							@RequestParam("sortBy") String sortBy,
							@RequestParam("sortOrder") int sortOrder,
							@RequestParam("filter") String filter,
							@RequestParam("globalFilter") String globalFilter
					){
		Grid<List<DyeingProgmModel>> response = new Grid<>();
		List<DyeingProgmModel> dataIo = new ArrayList<>();
		String sortDir = sortOrder == 1 ? "asc": "desc";
		
		JSONArray likeCondition = new JSONArray();
		likeCondition.put("dp.serialNo").put("party.name")
		.put("dp.than-i").put("dp.programDate-dd")
		.put("dp.updated_at-dd").put("dp.created_by").put("dp.updated_by");
		
		JSONArray entitySubAlias = new JSONArray();
		entitySubAlias.put("party");
		
		if(StringUtils.isNullOrEmpty(sortBy)) {
			sortBy = "created_at";
			sortDir = "desc";
		}
		
		List<DyeingProgmEntity> data = genericService.filterResult(new DyeingProgmEntity(), globalFilter, rows, from, sortBy, sortDir, 
				"dp", entitySubAlias, likeCondition);
		Integer count = genericService.getFilteredRecords(new DyeingProgmEntity(), globalFilter, rows, from+1, 
				"dp", entitySubAlias, likeCondition);
		
		dataIo = data.stream().map(master -> getDyeingProgramIo(master)).collect(Collectors.toList());
		
		response.setData(dataIo);
		response.setCount(count);
		
		return new ResponseEntity<Grid<List<DyeingProgmModel>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="search/serialNo", method=RequestMethod.GET)
    public ResponseEntity<ErpResponse<List<String>>> searchDyeingProgram(@RequestParam("serialNo") String serialNo, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
		
    	if(StringUtils.isNullOrEmpty(serialNo))
    		throw new IllegalArgumentException("Serial Number is required");
    	
    	List<String> results = this.genericService.getOneColumn(new DyeingProgmEntity(), serialNo, "serialNo");
		return new ResponseEntity<ErpResponse<List<String>>>(new ErpResponse<List<String>>(results, false,"success", "200"), HttpStatus.OK);
	}
	
	
	@RequestMapping(value="search/detail/report", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<List<DyeingProgmDetailModel>>> searchDyeingDetailsReport(@RequestBody FabricStockReportModel report, HttpServletRequest req) throws ApplicationException, ParseException{
    	List<DyeingProgmDetailEntity> data = this.reportsService.searchDyeingDetailsReport(report);
    	List<DyeingProgmDetailModel> response = data.stream().map(o -> getDyeingDetailIo(o)).collect(Collectors.toList());
		return new ResponseEntity<ErpResponse<List<DyeingProgmDetailModel>>>(new ErpResponse<List<DyeingProgmDetailModel>>(response, false,"success", "200"), HttpStatus.OK);
	}
	
	private DyeingProgmDetailModel getDyeingDetailIo(DyeingProgmDetailEntity entity) {
		entity.getProgram().setDetails(null);
    	DyeingProgmDetailModel model = new DyeingProgmDetailModel();
    	BeanUtils.copyProperties(entity, model);
    	
    	model.setLot(new LotEntryModel());
    	BeanUtils.copyProperties(entity.getLot(), model.getLot());
    	
    	model.getLot().setGrey(new GreyInwardModel());
    	BeanUtils.copyProperties(entity.getLot().getGrey(), model.getLot().getGrey());
    	
    	return model;
    }
	
	private void validateRequest(DyeingProgmModel model) {
		Helper.assertMsg(Helper.isNull(model.getParty()), "Party is required");
    	Helper.assertMsg(Helper.isEmpty(model.getSerialNo()), "Serial number is required");
    	Helper.assertMsg(Helper.isNull(model.getProgramDate()), "Program date is required");
    	Helper.assertMsg(Helper.isNull(model.getThan()), "Total than is required");
	}
	
	private DyeingProgmModel getDyeingProgramIo(DyeingProgmEntity entity)  {
    	DyeingProgmModel model = new DyeingProgmModel();
    	
    	BeanUtils.copyProperties(entity, model);
    	
    	//model = (DyeingProgmModel) SerializationUtils.clone(entity);
    	model.setDetails(null);
    	model.setCreated_at(entity.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getCreated_at()));
    	model.setUpdated_at(entity.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getUpdated_at()));
    	return model;
    }
	
	private DyeingProgmModel getDyeingProgramIoDeep(DyeingProgmEntity entity)  {
		
		for(int i=0; i<entity.getDetails().size(); i++) {
			entity.getDetails().get(i).setProgram(null);
		}
		
    	DyeingProgmModel model = new DyeingProgmModel();
    	
    	BeanUtils.copyProperties(entity, model);
    	
    	//model = (DyeingProgmModel) SerializationUtils.clone(entity);
    	
    	//Set<DyeingProgmDetailEntity> details = entity.getDetails();
    	
    	
    	model.setCreated_at(entity.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getCreated_at()));
    	model.setUpdated_at(entity.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getUpdated_at()));
    	return model;
    }
	
	private String getSerialNo() {
    	Helper helper = new Helper();
        String yearFormat = helper.getCurrentYearFormat();
    	String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "DEYING_PROGRAM", 3); 
    	String nexVal = helper.getNextVal("DP", lastReq, yearFormat);
    	return nexVal;
    }

}
