package com.erp.controller;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.json.JSONArray;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.DyeingProgmDetailEntityV2;
import com.erp.entity.DyeingProgmEntityV2;
import com.erp.entity.ShadeEntity;
import com.erp.exception.ApplicationException;
import com.erp.helper.DateUtil;
import com.erp.helper.DyeingProgramHelper;
import com.erp.helper.Helper;
import com.erp.helper.QueryObject;
import com.erp.rest.model.DyeingProgmDetailModelV2;
import com.erp.rest.model.DyeingProgmModelV2;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.FabricStockReportModel;
import com.erp.rest.model.GreyInwardModel;
import com.erp.rest.model.Grid;
import com.erp.rest.model.LotEntryModel;
import com.erp.rest.model.ShadeModel;
import com.erp.rest.model.UserIO;
import com.erp.rest.service.DyeingProgramService;
import com.erp.rest.service.ReportsService;
import com.erp.service.GenericService;
import com.mysql.jdbc.StringUtils;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/dyeing/v2")
public class DyeingProgramControllerV2 {
	
	@Resource
	private GenericService genericService;
	
	@Resource
	private DyeingProgramService dpService;
	
	@Resource
	private ReportsService reportsService;
	
	@Transactional
	@RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<String>> saveDyeingProgram(@RequestBody DyeingProgmModelV2 model, HttpServletRequest req) throws ApplicationException, ParseException{
			validateRequest(model);
	        UserIO user = (UserIO) req.getAttribute("user");
	        
	        DyeingProgmEntityV2 entity = DyeingProgramHelper.getDyeingProgramEntityV2(model, dpService, genericService);
	        
	        if(entity.getId() == null) {
	        	Helper helper = new Helper();
		        String yearFormat = helper.getCurrentYearFormat();
		        
		        entity.setDeleted('0');
		        entity.setCreated_by(user.getUserName());
		        entity.setCreated_at(new Date());
		        entity.setYearFormat(yearFormat);
		        entity.setSerialNo(getSerialNo());
		        entity.setShadeCount(entity.getDetails().size());
		        updatePentity(entity);
	        	genericService.add(entity);
	        }else {
	        	entity.setUpdated_at(new Date());
	        	entity.setUpdated_by(user.getUserName());
	        	entity.setShadeCount(entity.getDetails().size());
	        	updatePentity(entity);
	        	genericService.update(entity);
	        }
	        
	        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
    }
	
	private void updatePentity(DyeingProgmEntityV2 entity) {
		if(entity.getDetails()==null || entity.getDetails().size() == 0) {
			entity.setShade(null);
			entity.setParty(null);
			entity.setLotNo(null);
			return;
		}
		
		List<DyeingProgmDetailEntityV2> details = entity.getDetails();
		
		StringBuilder shade = new StringBuilder();
		StringBuilder party = new StringBuilder();
		StringBuilder lotNo = new StringBuilder();
		
		List<String> shadeNames = new ArrayList<>();
		List<String> partyNames = new ArrayList<>();
		List<String> lotNames = new ArrayList<>();
		
		for(DyeingProgmDetailEntityV2 detail: details) {
			if(detail.getShade()!=null && Helper.isNotEmpty(detail.getShade().getName())) {
				if(!shadeNames.contains(detail.getShade().getName())) {
					shade.append(detail.getShade().getName()).append(", ");
					shadeNames.add(detail.getShade().getName());
				}
			}
			if(detail.getLot()!=null) {
				if(!partyNames.contains(detail.getLot().getGrey().getParty().getName())) {
					party.append(detail.getLot().getGrey().getParty().getName()).append(", ");
					partyNames.add(detail.getLot().getGrey().getParty().getName());
				}
			}
			if(detail.getLot()!=null) {
				if(!lotNames.contains(detail.getLot().getGrey().getLotNo())) {
					lotNo.append(detail.getLot().getGrey().getLotNo()).append(", ");
					lotNames.add(detail.getLot().getGrey().getLotNo());
				}
			}
		}
		
		entity.setShade(shade.toString().substring(0, shade.toString().length()-2));
		entity.setParty(party.toString().substring(0, party.toString().length()-2));
		entity.setLotNo(lotNo.toString().substring(0, lotNo.toString().length()-2));
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<DyeingProgmModelV2>> getDyeingProgram(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
		if(id == null) {
			throw new IllegalArgumentException("Dyeing Program ID is required");
		}
		DyeingProgmEntityV2 dyeing = this.dpService.getDyeingProgramV2(id);
		
		return new ResponseEntity<ErpResponse<DyeingProgmModelV2>>(new ErpResponse<DyeingProgmModelV2>(getDyeingProgramIoDeep(dyeing), false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="/data/{serialNo}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<DyeingProgmModelV2>> getDyeingProgramSerialNo(@PathVariable("serialNo") String serialNo) throws NoSuchAlgorithmException, ApplicationException {
		if(org.springframework.util.StringUtils.isEmpty(serialNo)) {
			throw new IllegalArgumentException("Serial number is required");
		}
		DyeingProgmEntityV2 dyeing = this.genericService.getByField(new DyeingProgmEntityV2(), "serialNo", serialNo);
		
		if(dyeing == null)
			throw new IllegalArgumentException("No record found for: "+serialNo);
		
		return new ResponseEntity<ErpResponse<DyeingProgmModelV2>>(new ErpResponse<DyeingProgmModelV2>(getDyeingProgramIo(dyeing), false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="/nextserialNo", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<String>> getSerialNumber() {
    	String nexVal = getSerialNo();
		return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>(nexVal, false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public ResponseEntity<Grid<List<DyeingProgmModelV2>>> getDyeingProgramList(
							@RequestParam("from") int from,
							@RequestParam("rows") int rows,
							@RequestParam("sortBy") String sortBy,
							@RequestParam("sortOrder") int sortOrder,
							@RequestParam("filter") String filter,
							@RequestParam("globalFilter") String globalFilter
					){
		Grid<List<DyeingProgmModelV2>> response = new Grid<>();
		List<DyeingProgmModelV2> dataIo = new ArrayList<>();
		String sortDir = sortOrder == 1 ? "asc": "desc";
		
		JSONArray likeCondition = new JSONArray();
		likeCondition.put("dp.serialNo").put("dp.party").put("dp.shade").put("dp.lotNo")
		.put("dp.than-i").put("dp.programDate-dd").put("machine.name")
		.put("dp.updated_at-dd").put("dp.created_by").put("dp.updated_by");
		
		JSONArray entitySubAlias = new JSONArray();
		entitySubAlias.put("machine");
		
		if(StringUtils.isNullOrEmpty(sortBy)) {
			sortBy = "created_at";
			sortDir = "desc";
		}
		
		List<DyeingProgmEntityV2> data = genericService.filterResult(new DyeingProgmEntityV2(), globalFilter, rows, from, sortBy, sortDir, 
				"dp", entitySubAlias, likeCondition);
		Integer count = genericService.getFilteredRecords(new DyeingProgmEntityV2(), globalFilter, rows, from+1, 
				"dp", entitySubAlias, likeCondition);
		
		dataIo = data.stream().map(master -> getDyeingProgramIo(master)).collect(Collectors.toList());
		
		response.setData(dataIo);
		response.setCount(count);
		
		return new ResponseEntity<Grid<List<DyeingProgmModelV2>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="search/serialNo", method=RequestMethod.GET)
    public ResponseEntity<ErpResponse<List<String>>> searchDyeingProgram(@RequestParam("serialNo") String serialNo, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
		
    	if(StringUtils.isNullOrEmpty(serialNo))
    		throw new IllegalArgumentException("Serial Number is required");
    	
    	List<String> results = this.genericService.getOneColumn(new DyeingProgmEntityV2(), serialNo, "serialNo");
		return new ResponseEntity<ErpResponse<List<String>>>(new ErpResponse<List<String>>(results, false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="search/shade", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<List<ShadeModel>>> getShade(@RequestParam("qry") String query) throws ApplicationException {
    	List<ShadeModel> res = new ArrayList<>();
    	
    	QueryObject<String> shadeRestrict = new QueryObject<String>("name", query, "s", "like");
		List<ShadeEntity> data = this.genericService.findByFields(ShadeEntity.class, Arrays.asList(shadeRestrict), "shade", Arrays.asList());
		
    	if(data.size() > 0) {
    		data.forEach(arr -> {
    			ShadeModel shade = new ShadeModel();
    			shade.setName(arr.getName());
    			shade.setId(arr.getId());
    			shade.setColor(arr.getColor());
    			res.add(shade);
    		});
    	}
    	return new ResponseEntity<ErpResponse<List<ShadeModel>>>(new ErpResponse<List<ShadeModel>>(res, false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="search/programForPacking", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<List<DyeingProgmDetailEntityV2>> searchProgramForPacking(@RequestParam("party") Long partyId, @RequestParam(value="qry", required=false) String query){
		if(partyId == null) {
			throw new IllegalArgumentException("Party ID is required");
		}
		/*if(query == null || Helper.isEmpty(query)) {
			return new ResponseEntity<List<DyeingProgmDetailEntityV2>>(new ArrayList<DyeingProgmDetailEntityV2>(), HttpStatus.OK);
		}*/
		List<DyeingProgmDetailEntityV2> result = dpService.searchProgramForPacking(partyId, query);
		result.forEach(x -> {
			x.setShade(this.genericService.getShade(x.getShade().getId()));
			x.getProgram().setDetails(null);
		});
		return new ResponseEntity<List<DyeingProgmDetailEntityV2>>(result, HttpStatus.OK);
	}
	
	
	/*********** TODO fix report for v2 *****************/
	@RequestMapping(value="search/detail/report", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<List<DyeingProgmDetailModelV2>>> searchDyeingDetailsReport(@RequestBody FabricStockReportModel report, HttpServletRequest req) throws ApplicationException, ParseException{
    	List<DyeingProgmDetailEntityV2> data = null;//this.reportsService.searchDyeingDetailsReport(report);
    	List<DyeingProgmDetailModelV2> response = data.stream().map(o -> getDyeingDetailIo(o)).collect(Collectors.toList());
		return new ResponseEntity<ErpResponse<List<DyeingProgmDetailModelV2>>>(new ErpResponse<List<DyeingProgmDetailModelV2>>(response, false,"success", "200"), HttpStatus.OK);
	}
	
	private DyeingProgmDetailModelV2 getDyeingDetailIo(DyeingProgmDetailEntityV2 entity) {
		entity.getProgram().setDetails(null);
    	DyeingProgmDetailModelV2 model = new DyeingProgmDetailModelV2();
    	BeanUtils.copyProperties(entity, model);
    	
    	model.setLot(new LotEntryModel());
    	BeanUtils.copyProperties(entity.getLot(), model.getLot());
    	
    	model.getLot().setGrey(new GreyInwardModel());
    	BeanUtils.copyProperties(entity.getLot().getGrey(), model.getLot().getGrey());
    	
    	return model;
    }
	
	private void validateRequest(DyeingProgmModelV2 model) {
		//Helper.assertMsg(Helper.isNull(model.getParty()) || Helper.isEmpty(model.getParty()), "Party is required");
    	Helper.assertMsg(Helper.isEmpty(model.getSerialNo()), "Serial number is required");
    	Helper.assertMsg(Helper.isNull(model.getProgramDate()), "Program date is required");
    	Helper.assertMsg(Helper.isNull(model.getThan()), "Total than is required");
    	//Helper.assertMsg(Helper.isEmpty(model.getShade()), "Shade is required");
	}
	
	private DyeingProgmModelV2 getDyeingProgramIo(DyeingProgmEntityV2 entity)  {
    	DyeingProgmModelV2 model = new DyeingProgmModelV2();
    	
    	BeanUtils.copyProperties(entity, model);
    	
    	//model = (DyeingProgmModel) SerializationUtils.clone(entity);
    	model.setDetails(null);
    	model.setCreated_at(entity.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getCreated_at()));
    	model.setUpdated_at(entity.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getUpdated_at()));
    	return model;
    }
	
	private DyeingProgmModelV2 getDyeingProgramIoDeep(DyeingProgmEntityV2 entity)  {
		
		for(int i=0; i<entity.getDetails().size(); i++) {
			entity.getDetails().get(i).setProgram(null);
			entity.getDetails().get(i).setShade(this.genericService.getShade(entity.getDetails().get(i).getShade().getId()));
		}
		
    	DyeingProgmModelV2 model = new DyeingProgmModelV2();
    	
    	BeanUtils.copyProperties(entity, model);
    	
    	//model = (DyeingProgmModel) SerializationUtils.clone(entity);
    	
    	//Set<DyeingProgmDetailEntity> details = entity.getDetails();
    	
    	/*model.getDetails().forEach(x -> {
    		x.getShade().setDetails(null);
    	});*/
    	
    	model.setCreated_at(entity.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getCreated_at()));
    	model.setUpdated_at(entity.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getUpdated_at()));
    	return model;
    }
	
	private String getSerialNo() {
    	Helper helper = new Helper();
        String yearFormat = helper.getCurrentYearFormat();
    	String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "DEYING_PROGRAM_V2", 3); 
    	String nexVal = helper.getNextVal("DP", lastReq, yearFormat);
    	return nexVal;
    }

}
