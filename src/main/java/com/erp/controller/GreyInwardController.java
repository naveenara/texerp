package com.erp.controller;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.GreyInwardEntity;
import com.erp.entity.PartyEntity;
import com.erp.exception.ApplicationException;
import com.erp.exception.ApplicationRuntimeException;
import com.erp.helper.Constants;
import com.erp.helper.DateUtil;
import com.erp.helper.Helper;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.GreyInwardModel;
import com.erp.rest.model.GreyInwardReportModel;
import com.erp.rest.model.Grid;
import com.erp.rest.model.PartyModel;
import com.erp.rest.model.UserIO;
import com.erp.rest.service.ReportsService;
import com.erp.service.GenericService;
import com.mysql.jdbc.StringUtils;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/greyinward")
public class GreyInwardController {

	@Resource
	private GenericService genericService;
	
	@Resource
	private ReportsService reportsService;
	
	 @RequestMapping(value="", method=RequestMethod.POST)
     public ResponseEntity<ErpResponse<String>> saveGreyInward(@RequestBody GreyInwardModel greyInward, HttpServletRequest req) throws ApplicationException{
		 	validateRequest(greyInward);
	        UserIO user = (UserIO) req.getAttribute("user");
	        
	        GreyInwardEntity greyEntity = getGreyInwardEntity(greyInward);
	        
	        if(greyEntity.getId() == null) {
	        	Helper helper = new Helper();
		        String yearFormat = helper.getCurrentYearFormat();
		        
	        	greyEntity.setDeleted('0');
	        	greyEntity.setCreated_by(user.getUserName());
	        	greyEntity.setCreated_at(new Date());
	        	greyEntity.setYearFormat(yearFormat);
	        	greyEntity.setSerialNo(getSerialNo());
	        	greyEntity.setLotDone(false);
	        	
	        	genericService.add(greyEntity);
	        }else {
	        	greyEntity.setUpdated_at(new Date());
	        	greyEntity.setUpdated_by(user.getUserName());
	        	genericService.update(greyEntity);
	        }
	        
	        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
	    }
	    
	    @RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<GreyInwardModel>> getGreyInward(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
			if(id == null) {
				throw new IllegalArgumentException("Grey Inward ID is required");
			}
			GreyInwardEntity grey = this.genericService.findLongOne(new GreyInwardEntity(), id);
			
			return new ResponseEntity<ErpResponse<GreyInwardModel>>(new ErpResponse<GreyInwardModel>(getGreyInwardIo(grey), false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="/data/{serialNo}", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<GreyInwardModel>> getGreyInwardBySerialNo(@PathVariable("serialNo") String serialNo) throws NoSuchAlgorithmException, ApplicationException {
			if(org.springframework.util.StringUtils.isEmpty(serialNo)) {
				throw new IllegalArgumentException("Serial number is required");
			}
			GreyInwardEntity grey = this.genericService.getByField(new GreyInwardEntity(), "serialNo", serialNo);
			
			if(grey == null)
				throw new IllegalArgumentException("No record found for: "+serialNo);
			
			return new ResponseEntity<ErpResponse<GreyInwardModel>>(new ErpResponse<GreyInwardModel>(getGreyInwardIo(grey), false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="/nextserialNo", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<String>> getSerialNumber() {
	    	
	    	//Helper helper = new Helper();
	        //String yearFormat = helper.getCurrentYearFormat();
	    	//String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "GREY_INWARD", 3); 
	    	String nexVal = getSerialNo();
	    	
			return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>(nexVal, false,"success", "200"), HttpStatus.OK);
		}
	    
	    private String getSerialNo() {
	    	Helper helper = new Helper();
	        String yearFormat = helper.getCurrentYearFormat();
	    	String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "GREY_INWARD", 3); 
	    	String nexVal = helper.getNextVal("GI", lastReq, yearFormat);
	    	
	    	return nexVal;
	    }
	    
	    @RequestMapping(value="", method=RequestMethod.GET)
		public ResponseEntity<Grid<List<GreyInwardModel>>> getGreyInwardList(
								@RequestParam("from") int from,
								@RequestParam("rows") int rows,
								@RequestParam("sortBy") String sortBy,
								@RequestParam("sortOrder") int sortOrder,
								@RequestParam("filter") String filter,
								@RequestParam("globalFilter") String globalFilter
						){
			Grid<List<GreyInwardModel>> response = new Grid<>();
			List<GreyInwardModel> dataIo = new ArrayList<>();
			String sortDir = sortOrder == 1 ? "asc": "desc";
			
			JSONArray likeCondition = new JSONArray();
			likeCondition.put("gi.voucherNo-l").put("gi.serialNo").put("gi.biltyNo").put("gi.bales-i")
			.put("transport.name").put("party.name").put("gi.lotNo").put("quality.name").put("than-i")
			.put("length-d").put("unit.name").put("tp-i").put("notes").put("gi.created_at-dd")
			.put("gi.updated_at-dd").put("gi.created_by").put("gi.updated_by");
			
			JSONArray entitySubAlias = new JSONArray();
			entitySubAlias.put("transport").put("party").put("quality").put("unit");
			
			if(StringUtils.isNullOrEmpty(sortBy)) {
				sortBy = "created_at";
				sortDir = "desc";
			}
			
			JSONObject filtersObj = null;
					
			if(Helper.isNotEmpty(filter)) {
				filtersObj = new JSONObject(filter);
			}
			
			List<GreyInwardEntity> data = genericService.filterResult(new GreyInwardEntity(), globalFilter, rows, from, sortBy, sortDir, 
					"gi", entitySubAlias, likeCondition);
			Integer count = genericService.getFilteredRecords(new GreyInwardEntity(), globalFilter, rows, from+1, 
					"gi", entitySubAlias, likeCondition);
			
			dataIo = data.stream().map(master -> getGreyInwardIo(master)).collect(Collectors.toList());
			
			response.setData(dataIo);
			response.setCount(count);
			
			return new ResponseEntity<Grid<List<GreyInwardModel>>>(response, HttpStatus.OK);
		}
	    
	    @RequestMapping(value="search/party", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<List<PartyModel>>> searchParty(@RequestBody PartyModel partyModel, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
    		List<PartyEntity> parties = this.genericService.searchGeneric(MasterController.getPartyEntity(partyModel));
    		List<PartyModel> dataIo = parties.stream().map(master -> MasterController.getPartyIo(master)).collect(Collectors.toList());
	        
    		if(dataIo.size() > Constants.MAX_RESULTS) {
    			throw new IllegalArgumentException("Too many records found: "+Constants.MAX_RESULTS+". Please narrow search criteria and try again");
    		}
    		
    		return new ResponseEntity<ErpResponse<List<PartyModel>>>(new ErpResponse<List<PartyModel>>(dataIo, false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="search/serialNo", method=RequestMethod.GET)
	    public ResponseEntity<ErpResponse<List<String>>> searchGreyInward(@RequestParam("serialNo") String serialNo, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
    		
	    	if(StringUtils.isNullOrEmpty(serialNo))
	    		throw new IllegalArgumentException("Serial Number is required");
	    	
	    	List<String> results = this.genericService.getOneColumn(new GreyInwardEntity(), serialNo, "serialNo");
    		return new ResponseEntity<ErpResponse<List<String>>>(new ErpResponse<List<String>>(results, false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="/clone", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<String>> cloneGreyInward(@RequestBody GreyInwardModel model, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
	    	if(model.getId() == null) {
	    		throw new IllegalArgumentException("Grey Inward ID is required");
	    	}
	    	UserIO user = (UserIO) req.getAttribute("user");
	    	
	    	GreyInwardEntity entity = this.genericService.findOne(GreyInwardEntity.class, model.getId());
	    	GreyInwardEntity entityNew = clone(entity, user.getUserName());
	    	this.genericService.add(entityNew);
    		
    		return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>(entityNew.getSerialNo(), false,"success", "200"), HttpStatus.CREATED);
		}
	    
	    @RequestMapping(value="search", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<List<GreyInwardModel>>> searchGreyInwards(@RequestBody GreyInwardReportModel report, HttpServletRequest req) throws ApplicationException, ParseException{
	    	List<GreyInwardEntity> data = this.reportsService.searchGreyInward(report);
	    	List<GreyInwardModel> response = data.stream().map(o -> getGreyInwardIo(o)).collect(Collectors.toList());
    		return new ResponseEntity<ErpResponse<List<GreyInwardModel>>>(new ErpResponse<List<GreyInwardModel>>(response, false,"success", "200"), HttpStatus.OK);
		}
	    
	    
	    /***************************************** Private methods *******************************/
	    private GreyInwardEntity clone(GreyInwardEntity entity, String username) {
	    	GreyInwardEntity entityNew = new GreyInwardEntity();
	    	
	    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
	    	BeanUtils.copyProperties(entity, entityNew, "created_at", "updated_at");
	    	
	    	entityNew.setId(null);
	    	entityNew.setCreated_at(new Date());
	    	entityNew.setUpdated_at(null);
	    	entityNew.setUpdated_by(null);
	    	entityNew.setCreated_by(username);
	    	entityNew.setSerialNo(getSerialNo());
	    	entityNew.setLotDone(false);
	    	
	    	return entityNew;
	    }
	    
	    public static GreyInwardModel getGreyInwardIo(GreyInwardEntity entity)  {
	    	GreyInwardModel model = new GreyInwardModel();
	    	
	    	BeanUtils.copyProperties(entity, model);
	    	
	    	model.setCreated_at(entity.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getCreated_at()));
	    	model.setUpdated_at(entity.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getUpdated_at()));
	    	
	    	return model;
	    }
	    
	    public static GreyInwardEntity getGreyInwardEntity(GreyInwardModel model) {
	    	GreyInwardEntity entity = new GreyInwardEntity();
	    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
	    	BeanUtils.copyProperties(model, entity, "created_at", "updated_at");
	    	return entity;
	    }
	    
	    private void validateRequest(GreyInwardModel model) {
	    	Helper.assertMsg(Helper.isNull(model.getVoucherNo()), "Voucher number is required");
	    	Helper.assertMsg(Helper.isEmpty(model.getSerialNo()), "Serial number is required");
	    	Helper.assertMsg(Helper.isNull(model.getBiltyNo()), "Bilty number is required");
	    	//Helper.assertMsg(Helper.isNull(model.getTransport()), "Transport is required");
	    	Helper.assertMsg(Helper.isNull(model.getParty()), "Party is required");
	    	Helper.assertMsg(Helper.isEmpty(model.getLotNo()), "Lot number is required");
	    	
	    	if(model.getId() == null) {
	    		GreyInwardEntity duplicate = this.genericService.getByField(new GreyInwardEntity(), "lotNo", model.getLotNo());
	    		if(!Helper.isNull(duplicate)) {
	    			throw new ApplicationRuntimeException("This lot number is already used in : "+duplicate.getSerialNo());
	    		}
	    	}else {
	    		
	    	}
	    }
	
}
