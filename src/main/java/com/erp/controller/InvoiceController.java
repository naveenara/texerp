package com.erp.controller;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.CompanyEntity;
import com.erp.entity.InvoiceEntity;
import com.erp.entity.ProcessPackingEntity;
import com.erp.exception.ApplicationException;
import com.erp.helper.DateUtil;
import com.erp.helper.Helper;
import com.erp.helper.InvoiceHelper;
import com.erp.helper.NumberToWord;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.Grid;
import com.erp.rest.model.InvoiceModel;
import com.erp.rest.model.UserIO;
import com.erp.rest.service.InvoiceService;
import com.erp.service.GenericService;
import com.mysql.jdbc.StringUtils;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/invoice")
public class InvoiceController {

	@Resource
	private GenericService genericService;
	
	@Resource
	private InvoiceService invoiceService;

	@RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<String>> save(@RequestBody InvoiceModel model, HttpServletRequest req) throws ApplicationException, ParseException{
			validateRequest(model);
	        UserIO user = (UserIO) req.getAttribute("user");
	        
	        InvoiceEntity entity = InvoiceHelper.getInvoice(model, invoiceService, genericService);
	        
	        if(entity.getId() == null) {
	        	Helper helper = new Helper();
		        String yearFormat = helper.getCurrentYearFormat();
		        
		        entity.setDeleted('0');
		        entity.setCreated_by(user.getUserName());
		        entity.setCreated_at(new Date());
		        entity.setYearFormat(yearFormat);
		        entity.setSerialNo(getSerialNo());
		        if(entity.getDetails().size() > 0)
		        	updateInvoiceStatus(entity);
	        	genericService.add(entity);
	        }else {
	        	entity.setUpdated_at(new Date());
	        	entity.setUpdated_by(user.getUserName());
	        	if(entity.getDetails().size() > 0)
	        		updateInvoiceStatus(entity);
	        	genericService.update(entity);
	        }
	        
	        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
    }
	
	private void updateInvoiceStatus(InvoiceEntity entity) {
		
		if(entity.getId()!=null) {
			InvoiceEntity invoice = this.invoiceService.get(entity.getId());
			invoice.getDetails().forEach(x -> {
				ProcessPackingEntity ppe = this.genericService.findLongOne(new ProcessPackingEntity(), x.getPp().getId());
				ppe.setInvoiceDone(false);
				this.genericService.update(ppe);
			});
		}
		
		if(entity.getDetails().size() > 0) {
			entity.getDetails().forEach(x -> {
				ProcessPackingEntity ppe = this.genericService.findLongOne(new ProcessPackingEntity(), x.getPp().getId());
				ppe.setInvoiceDone(true);
				this.genericService.update(ppe);
			});
		}
		
		Double totalAmt = entity.getAmount();
		Double totalAmountWithTax = entity.getAmount();
		if(totalAmt!=null) {
			CompanyEntity company = this.genericService.findLongOne(new CompanyEntity(), 1L);
			if(company!=null) {
				if(company.getIgst()!=null) {
					Float igstAmtPerc = totalAmt.floatValue() * company.getIgst() / 100;
					Float decimalNum = (float) (Math.round(igstAmtPerc * 100.0) / 100.0);
					entity.setIgstAmt(decimalNum);
					if(!"local".equalsIgnoreCase(entity.getPlaceOfSupply())) {
						totalAmountWithTax += decimalNum;
					}
				}
				if(company.getCgst()!=null) {
					Float cgstAmtPerc = totalAmt.floatValue() * company.getCgst() / 100;
					Float decimalNum = (float) (Math.round(cgstAmtPerc * 100.0) / 100.0);
					entity.setCgstAmt(decimalNum);
					if("local".equalsIgnoreCase(entity.getPlaceOfSupply())) {
						totalAmountWithTax += decimalNum;
					}
				}
				if(company.getSgst()!=null) {
					Float sgstAmtPerc = totalAmt.floatValue() * company.getSgst() / 100;
					Float decimalNum = (float) (Math.round(sgstAmtPerc * 100.0) / 100.0);
					entity.setSgstAmt(decimalNum);
					if("local".equalsIgnoreCase(entity.getPlaceOfSupply())) {
						totalAmountWithTax += decimalNum;
					}
				}
			}
		}
		entity.setTotalAmount(totalAmountWithTax.intValue());
		entity.setTotalAmtWords(NumberToWord.convert(totalAmountWithTax.intValue()).toUpperCase());
	}
	
	@RequestMapping(value="/nextserialNo", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<String>> getSerialNumber() {
    	String nexVal = getSerialNo();
		return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>(nexVal, false,"success", "200"), HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<InvoiceModel>> getInvoice(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
		if(id == null) {
			throw new IllegalArgumentException("Invoice ID is required");
		}
		InvoiceEntity invoice = this.invoiceService.get(id);
		invoice.getDetails().forEach(x -> {
			x.getPp().getDetail().setProgram(null);
			x.getPp().getDetail().getShade().setDetails(null);
		});
		return new ResponseEntity<ErpResponse<InvoiceModel>>(new ErpResponse<InvoiceModel>(getInvoiceDeep(invoice), false,"success", "200"), HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public ResponseEntity<Grid<List<InvoiceModel>>> getInvoiceList(
							@RequestParam("from") int from,
							@RequestParam("rows") int rows,
							@RequestParam("sortBy") String sortBy,
							@RequestParam("sortOrder") int sortOrder,
							@RequestParam("filter") String filter,
							@RequestParam("globalFilter") String globalFilter
					){
		Grid<List<InvoiceModel>> response = new Grid<>();
		List<InvoiceModel> dataIo = new ArrayList<>();
		String sortDir = sortOrder == 1 ? "asc": "desc";
		
		JSONArray likeCondition = new JSONArray();
		likeCondition.put("i.serialNo").put("i.ewayBillNo").put("billTo.name").put("shipTo.name")
		.put("i.than-i").put("i.invoiceDate-dd").put("transport.name")
		.put("i.updated_at-dd").put("i.created_by").put("i.updated_by");
		
		JSONArray entitySubAlias = new JSONArray();
		entitySubAlias.put("transport");
		entitySubAlias.put("billTo");
		entitySubAlias.put("shipTo");
		
		if(StringUtils.isNullOrEmpty(sortBy)) {
			sortBy = "created_at";
			sortDir = "desc";
		}
		
		List<InvoiceEntity> data = genericService.filterResult(new InvoiceEntity(), globalFilter, rows, from, sortBy, sortDir, 
				"i", entitySubAlias, likeCondition);
		Integer count = genericService.getFilteredRecords(new InvoiceEntity(), globalFilter, rows, from+1, 
				"i", entitySubAlias, likeCondition);
		
		dataIo = data.stream().map(master -> getInvoiceIo(master)).collect(Collectors.toList());
		
		response.setData(dataIo);
		response.setCount(count);
		
		return new ResponseEntity<Grid<List<InvoiceModel>>>(response, HttpStatus.OK);
	}
	
	
	private InvoiceModel getInvoiceDeep(InvoiceEntity entity)  {
		for(int i=0; i<entity.getDetails().size(); i++) {
			entity.getDetails().get(i).setInvoice(null);
		}
		
    	InvoiceModel model = new InvoiceModel();
    	BeanUtils.copyProperties(entity, model);
    	
    	model.setCreated_at(entity.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getCreated_at()));
    	model.setUpdated_at(entity.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getUpdated_at()));
    	return model;
    }
	
	private InvoiceModel getInvoiceIo(InvoiceEntity entity)  {
    	InvoiceModel model = new InvoiceModel();
    	
    	BeanUtils.copyProperties(entity, model);
    	
    	//model = (DyeingProgmModel) SerializationUtils.clone(entity);
    	model.setDetails(null);
    	model.setCreated_at(entity.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getCreated_at()));
    	model.setUpdated_at(entity.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getUpdated_at()));
    	return model;
    }
	
	private String getSerialNo() {
    	Helper helper = new Helper();
        String yearFormat = helper.getCurrentYearFormat();
    	String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "INVOICE", 4); 
    	String nexVal = helper.getNextVal("INV", lastReq, yearFormat);
    	return nexVal;
    }
	
	private void validateRequest(InvoiceModel model) {
    	Helper.assertMsg(Helper.isEmpty(model.getSerialNo()), "Serial number is required");
    	Helper.assertMsg(Helper.isNull(model.getInvoiceDate()), "Invoice date is required");
    	Helper.assertMsg(Helper.isNull(model.getShipTo()), "Ship to party is required");
	}
	
}
