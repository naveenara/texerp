package com.erp.controller;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.json.JSONArray;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.GreyInwardEntity;
import com.erp.entity.LotEntryEntity;
import com.erp.exception.ApplicationException;
import com.erp.exception.ApplicationRuntimeException;
import com.erp.helper.Constants;
import com.erp.helper.DateUtil;
import com.erp.helper.Helper;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.GreyInwardModel;
import com.erp.rest.model.Grid;
import com.erp.rest.model.LotEntryModel;
import com.erp.rest.model.LotEntryReportModel;
import com.erp.rest.model.UserIO;
import com.erp.rest.service.ReportsService;
import com.erp.service.GenericService;
import com.mysql.jdbc.StringUtils;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/lotEntry")
public class LotEntryController {

	@Resource
	private GenericService genericService;
	
	@Resource
	private ReportsService reportsService;
	
	 @Transactional
	 @RequestMapping(value="", method=RequestMethod.POST)
     public ResponseEntity<ErpResponse<String>> saveLotEntry(@RequestBody LotEntryModel lotEntry, HttpServletRequest req) throws ApplicationException, ParseException{
		    GreyInwardEntity grey = validateRequest(lotEntry);
	        UserIO user = (UserIO) req.getAttribute("user");
	        
	        LotEntryEntity entity = getLotEntryEntity(lotEntry);
	        
	        if(entity.getId() == null) {
	        	Helper helper = new Helper();
		        String yearFormat = helper.getCurrentYearFormat();
		        
		        entity.setBalance(entity.getReceived());
		        
		        entity.setDeleted('0');
		        entity.setCreated_by(user.getUserName());
		        entity.setCreated_at(new Date());
		        entity.setYearFormat(yearFormat);
		        entity.setSerialNo(getSerialNo());
		        entity.setCompleted(false);
	        	
		        grey.setLotDone(true);
		        genericService.update(grey);
		        
	        	genericService.add(entity);
	        }else {
	        	updateBalance(entity);
	        	entity.setUpdated_at(new Date());
	        	entity.setUpdated_by(user.getUserName());
	        	
	        	if(entity.getBalance()!=null && entity.getBalance() == 0) {
	        		entity.setCompleted(true);
	        	}
	        	
	        	genericService.update(entity);
	        }
	        
	        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
	    }
	    
	 	private void updateBalance(LotEntryEntity entity) throws ApplicationException {
	 		//LotEntryEntity entityDb = this.genericService.findOne(LotEntryEntity.class, entity.getId());
	 		
	 		List<Object[]> data = this.genericService.getDateOneRow(new LotEntryEntity(), entity.getId(), Arrays.asList("received", "balance"));
	 		
	 		Integer dbLength = (Integer) data.get(0)[0];
	 		Integer uiLength = entity.getReceived();
	 		Integer currentBalance = entity.getBalance();
	 		
	 		Integer currentUsed = dbLength - currentBalance;
	 		
	 		if(uiLength < currentUsed) {
	 			throw new ApplicationRuntimeException("Received thans cannot be less than: "+currentUsed+", as those thans are already used");
	 		}
	 		
	 		if(dbLength > uiLength) {
	 			Integer diff = dbLength - uiLength;
	 			entity.setBalance(entity.getBalance() - diff);
	 		}else if(dbLength < uiLength) {
	 			Integer diff = uiLength - dbLength;
	 			entity.setBalance(entity.getBalance() + diff);
	 		}
	 	}
	 
	    @RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<LotEntryModel>> getLotEntry(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
			if(id == null) {
				throw new IllegalArgumentException("Lot Entry ID is required");
			}
			LotEntryEntity grey = this.genericService.findLongOne(new LotEntryEntity(), id);
			
			return new ResponseEntity<ErpResponse<LotEntryModel>>(new ErpResponse<LotEntryModel>(getLotEntryIo(grey), false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="/data/{serialNo}", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<LotEntryModel>> getGreyInwardBySerialNo(@PathVariable("serialNo") String serialNo) throws NoSuchAlgorithmException, ApplicationException {
			if(org.springframework.util.StringUtils.isEmpty(serialNo)) {
				throw new IllegalArgumentException("Serial number is required");
			}
			LotEntryEntity lot = this.genericService.getByField(new LotEntryEntity(), "serialNo", serialNo);
			
			if(lot == null)
				throw new IllegalArgumentException("No record found for: "+serialNo);
			
			return new ResponseEntity<ErpResponse<LotEntryModel>>(new ErpResponse<LotEntryModel>(getLotEntryIo(lot), false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="/nextserialNo", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<String>> getSerialNumber() {
	    	
	    	//Helper helper = new Helper();
	        //String yearFormat = helper.getCurrentYearFormat();
	    	//String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "GREY_INWARD", 3); 
	    	String nexVal = getSerialNo();
	    	
			return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>(nexVal, false,"success", "200"), HttpStatus.OK);
		}
	    
	    private String getSerialNo() {
	    	Helper helper = new Helper();
	        String yearFormat = helper.getCurrentYearFormat();
	    	String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "LOT_ENTRY", 3); 
	    	String nexVal = helper.getNextVal("LE", lastReq, yearFormat);
	    	
	    	return nexVal;
	    }
	    
	    @RequestMapping(value="", method=RequestMethod.GET)
		public ResponseEntity<Grid<List<LotEntryModel>>> getLotEntryList(
								@RequestParam("from") int from,
								@RequestParam("rows") int rows,
								@RequestParam("sortBy") String sortBy,
								@RequestParam("sortOrder") int sortOrder,
								@RequestParam("filter") String filter,
								@RequestParam("globalFilter") String globalFilter
						){
			Grid<List<LotEntryModel>> response = new Grid<>();
			List<LotEntryModel> dataIo = new ArrayList<>();
			String sortDir = sortOrder == 1 ? "asc": "desc";
			
			JSONArray likeCondition = new JSONArray();
			likeCondition.put("le.serialNo").put("le.received-i").put("le-balance-i")
			.put("grey.lotNo").put("grey.serialNo").put("grey.biltyNo").put("party.name")
			.put("le.created_at-dd")
			.put("le.updated_at-dd").put("le.created_by").put("le.updated_by");
			
			JSONArray entitySubAlias = new JSONArray();
			entitySubAlias.put("grey").put("grey.party").put("grey.quality");
			
			if(StringUtils.isNullOrEmpty(sortBy)) {
				sortBy = "created_at";
				sortDir = "desc";
			}
			
			List<LotEntryEntity> data = genericService.filterResult(new LotEntryEntity(), globalFilter, rows, from, sortBy, sortDir, 
					"le", entitySubAlias, likeCondition);
			Integer count = genericService.getFilteredRecords(new LotEntryEntity(), globalFilter, rows, from+1, 
					"le", entitySubAlias, likeCondition);
			
			dataIo = data.stream().map(master -> getLotEntryIo(master)).collect(Collectors.toList());
			
			response.setData(dataIo);
			response.setCount(count);
			
			return new ResponseEntity<Grid<List<LotEntryModel>>>(response, HttpStatus.OK);
		}
	    
	    @RequestMapping(value="search/grey", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<List<GreyInwardModel>>> searchGrey(@RequestBody GreyInwardModel model, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
    		List<GreyInwardEntity> parties = this.genericService.searchGeneric(GreyInwardController.getGreyInwardEntity(model));
    		List<GreyInwardModel> dataIo = parties.stream().map(master -> GreyInwardController.getGreyInwardIo(master)).collect(Collectors.toList());
	        
    		if(dataIo.size() > Constants.MAX_RESULTS) {
    			throw new IllegalArgumentException("Too many records found: "+Constants.MAX_RESULTS+". Please narrow search criteria and try again");
    		}
    		
    		return new ResponseEntity<ErpResponse<List<GreyInwardModel>>>(new ErpResponse<List<GreyInwardModel>>(dataIo, false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="search/serialNo", method=RequestMethod.GET)
	    public ResponseEntity<ErpResponse<List<String>>> searchGreyInward(@RequestParam("serialNo") String serialNo, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
    		
	    	if(StringUtils.isNullOrEmpty(serialNo))
	    		throw new IllegalArgumentException("Serial Number is required");
	    	
	    	List<String> results = this.genericService.getOneColumn(new LotEntryEntity(), serialNo, "serialNo");
    		return new ResponseEntity<ErpResponse<List<String>>>(new ErpResponse<List<String>>(results, false,"success", "200"), HttpStatus.OK);
		}
	    
	    /**
	     * Search lot while creating dyeing program for a party
	     * @param model
	     * @param req
	     * @return
	     * @throws ApplicationException
	     * @throws ParseException
	     */
	    @RequestMapping(value="search/lot", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<List<LotEntryModel>>> searchLotEntry(@RequestBody LotEntryModel model, HttpServletRequest req) throws ApplicationException, ParseException{
	    	List<LotEntryEntity> data = this.reportsService.searchLotEntry(model);
	    	List<LotEntryModel> response = data.stream().map(o -> getLotEntryIo(o)).collect(Collectors.toList());
    		return new ResponseEntity<ErpResponse<List<LotEntryModel>>>(new ErpResponse<List<LotEntryModel>>(response, false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="status", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<Boolean>> lotAction(@RequestBody LotEntryModel model, HttpServletRequest req) throws ApplicationException, ParseException{
	    	
	    	if(Helper.isNull(model.getCompleted())) {
	    		throw new ApplicationRuntimeException("Lot status is required");
	    	}if(Helper.isNull(model.getId())) {
	    		throw new ApplicationRuntimeException("Lot id is required");
	    	}
	    	
	    	LotEntryEntity lot = this.genericService.findLongOne(new LotEntryEntity(), model.getId());
	    	lot.setCompleted(model.getCompleted());
	    	genericService.update(lot);
	    	
    		return new ResponseEntity<ErpResponse<Boolean>>(new ErpResponse<Boolean>(model.getCompleted(), false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="search/detail/report", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<List<LotEntryModel>>> searchDyeingDetailsReport(@RequestBody LotEntryReportModel report, HttpServletRequest req) throws ApplicationException, ParseException{
	    	List<LotEntryEntity> data = this.reportsService.searchLotEntryReport(report);
	    	List<LotEntryModel> response = data.stream().map(o -> getLotEntryIo(o)).collect(Collectors.toList());
			return new ResponseEntity<ErpResponse<List<LotEntryModel>>>(new ErpResponse<List<LotEntryModel>>(response, false,"success", "200"), HttpStatus.OK);
		}
	    
	    /*@RequestMapping(value="/clone", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<String>> cloneGreyInward(@RequestBody GreyInwardModel model, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
	    	if(model.getId() == null) {
	    		throw new IllegalArgumentException("Grey Inward ID is required");
	    	}
	    	UserIO user = (UserIO) req.getAttribute("user");
	    	
	    	GreyInwardEntity entity = this.genericService.findOne(GreyInwardEntity.class, model.getId());
	    	GreyInwardEntity entityNew = clone(entity, user.getUserName());
	    	this.genericService.add(entityNew);
    		
    		return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>(entityNew.getSerialNo(), false,"success", "200"), HttpStatus.CREATED);
		}*/
	    
	    /*@RequestMapping(value="search", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<List<LotEntryModel>>> searchGreyInwards(@RequestBody GreyInwardReportModel report, HttpServletRequest req) throws ApplicationException, ParseException{
	    	List<GreyInwardEntity> data = this.reportsService.searchGreyInward(report);
	    	List<GreyInwardModel> response = data.stream().map(o -> getGreyInwardIo(o)).collect(Collectors.toList());
    		return new ResponseEntity<ErpResponse<List<LotEntryModel>>>(new ErpResponse<List<LotEntryModel>>(response, false,"success", "200"), HttpStatus.OK);
		}*/
	    
	    
	    /***************************************** Private methods *******************************/
	    /*private GreyInwardEntity clone(GreyInwardEntity entity, String username) {
	    	GreyInwardEntity entityNew = new GreyInwardEntity();
	    	
	    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
	    	BeanUtils.copyProperties(entity, entityNew, "created_at", "updated_at");
	    	
	    	entityNew.setId(null);
	    	entityNew.setCreated_at(new Date());
	    	entityNew.setUpdated_at(null);
	    	entityNew.setUpdated_by(null);
	    	entityNew.setCreated_by(username);
	    	entityNew.setSerialNo(getSerialNo());
	    	
	    	return entityNew;
	    }*/
	    
	    private LotEntryModel getLotEntryIo(LotEntryEntity entity)  {
	    	LotEntryModel model = new LotEntryModel();
	    	
	    	BeanUtils.copyProperties(entity, model);
	    	
	    	model.setCreated_at(entity.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getCreated_at()));
	    	model.setUpdated_at(entity.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(entity.getUpdated_at()));
	    	
	    	model.setGrey(GreyInwardController.getGreyInwardIo(entity.getGrey()));
	    	
	    	return model;
	    }
	    
	    public static LotEntryEntity getLotEntryEntity(LotEntryModel model) throws ParseException {
	    	LotEntryEntity entity = new LotEntryEntity();
	    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
	    	BeanUtils.copyProperties(model, entity, "created_at", "updated_at");
	    	
	    	//entity.setReceivedDate(DateUtil.getDate(model.getReceivedDate(), "dd/MM/yyyy"));
	    	entity.setGrey(GreyInwardController.getGreyInwardEntity(model.getGrey()));
	    	
	    	return entity;
	    }
	    
	    private GreyInwardEntity validateRequest(LotEntryModel model) {
	    	Helper.assertMsg(Helper.isEmpty(model.getSerialNo()), "Serial number is required");
	    	Helper.assertMsg(Helper.isNull(model.getGrey()) || Helper.isEmpty(model.getGrey().getSerialNo()), "Grey details are required");
	    	Helper.assertMsg(Helper.isEmpty(model.getGrey().getLotNo()), "Grey lot number is required");
	    	Helper.assertMsg(Helper.isNull(model.getReceived()), "Received meter is required");
	    	Helper.assertMsg(Helper.isNull(model.getGrey().getId()), "Please select a valid grey entry");
	    	Helper.assertMsg(Helper.isNull(model.getReceivedDate()), "Grey inward date is required");
	    	
	    	GreyInwardEntity grey = this.genericService.getByField(new GreyInwardEntity(), "serialNo", model.getGrey().getSerialNo());
	    	Helper.assertMsg(Helper.isNull(grey), "No grey details found for: "+model.getGrey().getSerialNo());
	    	
	    	Helper.assertMsg(!model.getGrey().getLotNo().equals(grey.getLotNo()), "Provided lot number: "+model.getGrey().getLotNo()+" does not match with: "+grey.getLotNo());
	    	
	    	if(model.getId()==null) {
	    		Helper.assertMsg(grey.getLotDone() == true, "Lot already done for: "+grey.getSerialNo());
	    	}
	    	
	    	return grey;
	    }
	    
}
