package com.erp.controller;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.erp.constants.Master;
import com.erp.entity.MasterEntity;
import com.erp.entity.PartyEntity;
import com.erp.entity.ShadeDetailEntity;
import com.erp.entity.ShadeEntity;
import com.erp.exception.ApplicationException;
import com.erp.helper.DateUtil;
import com.erp.helper.Helper;
import com.erp.helper.QueryObject;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.Grid;
import com.erp.rest.model.MasterModel;
import com.erp.rest.model.PartyModel;
import com.erp.rest.model.ShadeModel;
import com.erp.rest.model.UserIO;
import com.erp.service.GenericService;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/master")
public class MasterController {

    @Autowired
    private GenericService genericService;
    
    private static final Logger logger = LoggerFactory.getLogger(MasterController.class);
    
    
    @RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<String>> saveEntity(@RequestBody MasterEntity master, HttpServletRequest req) throws ApplicationException{
        validate(master);
        UserIO user = (UserIO) req.getAttribute("user");
        if(master.getId() == null) {
        	prepareMasterEntity(master);
        	master.setCreated_by(user.getUserName());
        	genericService.add(master);
        }else {
        	MasterEntity masterDb = genericService.findOne(MasterEntity.class, master.getId());
        	masterDb.setName(master.getName());
        	masterDb.setCode(master.getCode());
        	masterDb.setUpdated_at(new Date());
        	masterDb.setUpdated_by(user.getUserName());
        	genericService.update(masterDb);
        }
        
        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
    }
    
    @RequestMapping(value="{id}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<MasterEntity>> getMaster(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
		if(id == null) {
			throw new IllegalArgumentException("Entity ID is required");
		}
    	MasterEntity master = this.genericService.findLongOne(new MasterEntity(), id);
		return new ResponseEntity<ErpResponse<MasterEntity>>(new ErpResponse<MasterEntity>(master, false,"success", "200"), HttpStatus.OK);
	}
    
    @RequestMapping(value="", method=RequestMethod.GET)
	public ResponseEntity<Grid<List<MasterModel>>> getUsers(
							@RequestParam("from") int from,
							@RequestParam("rows") int rows,
							@RequestParam("sortBy") String sortBy,
							@RequestParam("sortOrder") int sortOrder,
							@RequestParam("filter") String filter,
							@RequestParam("globalFilter") String globalFilter,
							@RequestParam("type") Character type
					){
		Grid<List<MasterModel>> response = new Grid<>();
		List<MasterModel> dataIo = new ArrayList<>();
		String sortDir = sortOrder == 1 ? "asc": "desc";
		
		JSONArray likeCondition = new JSONArray();
		likeCondition.put("name").put("code");
		
		/*if(sortBy!=null && "active".equals(sortBy)) {
			sortBy = "deleted";
		}*/
		
		if(Helper.isEmpty(sortBy)) {
			sortBy = "created_at";
			sortDir = "desc";
		}
		
		List<MasterEntity> data = genericService.filterResult(new MasterEntity(), globalFilter, rows, from, sortBy, sortDir, 
				"master", new JSONArray(), likeCondition, "type", Master.getByType(type).getType().toString());
		Integer count = genericService.getFilteredRecords(new MasterEntity(), globalFilter, rows, from+1, 
				"master", new JSONArray(), likeCondition, "type", Master.getByType(type).getType().toString());
		
		dataIo = data.stream().map(master -> getMasterIo(master)).collect(Collectors.toList());
		
		response.setData(dataIo);
		response.setCount(count);
		
		return new ResponseEntity<Grid<List<MasterModel>>>(response, HttpStatus.OK);
	}
    
    @RequestMapping(value="/type/{type}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<List<MasterEntity>>> getTransport(@PathVariable("type") Character type) {
    	List<MasterEntity> transports = genericService.getAll(MasterEntity.class, "type", type); 
		return new ResponseEntity<ErpResponse<List<MasterEntity>>>(new ErpResponse<List<MasterEntity>>(transports, false,"success", "200"), HttpStatus.OK);
	}
    
    
    @RequestMapping(value="/{type}/search", method=RequestMethod.GET, produces="application/json")
   	public ResponseEntity<ErpResponse<List<MasterModel>>> searchMaster(@PathVariable("type") Character type, @RequestParam("qry") String query) {
       	List<MasterEntity> data = new ArrayList<>();
       	List<MasterModel> res = new ArrayList<>();
       	
   		data = this.genericService.searchMaster(query, type);
       	if(data.size() > 0) {
       		data.forEach(arr -> {
       			MasterModel master = new MasterModel();
       			master.setName(arr.getName());
       			master.setId(arr.getId());
       			res.add(master);
       		});
       	}
       	return new ResponseEntity<ErpResponse<List<MasterModel>>>(new ErpResponse<List<MasterModel>>(res, false,"success", "200"), HttpStatus.OK);
   	}
    
    /*@RequestMapping(value="/deleteEntity", method=RequestMethod.DELETE)
    public ResponseEntity deleteEntity(@RequestBody Object entity){
        ResponseEntity response = null;
        
        try{
            gservice.delete(entity);
            RestResponse success = new RestResponse<>(true, "success", entity);
            response = new ResponseEntity<>(success, HttpStatus.OK);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("*********** Error deleting entity ********", entity);
            RestResponse error = new RestResponse<>(false, "error", entity);
            response = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
        return response;
    }*/
    
    private MasterModel getMasterIo(MasterEntity master)  {
    	MasterModel model = new MasterModel();
    	
    	BeanUtils.copyProperties(master, model);
    	
    	model.setCreated_at(master.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(master.getCreated_at()));
    	model.setUpdated_at(master.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(master.getUpdated_at()));
    	
    	return model;
    }
    
    private void validate(MasterEntity master) {
    	if(Helper.isEmpty(master.getCode())) {
    		throw new IllegalArgumentException("Code is required");
    	}else if(Helper.isEmpty(master.getName())) {
    		throw new IllegalArgumentException("Name is required");
    	}
    	else if(master.getType() == null) {
    		throw new IllegalArgumentException("Master type is required");
    	}
    }
    void prepareMasterEntity(MasterEntity master) {
    	master.setCreated_at(new Date());
    	master.setDeleted('0');
    }
    
    
    /*********************** Party controller *************************/
    
    @RequestMapping(value="/party", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<String>> saveParty(@RequestBody PartyModel partyModel, HttpServletRequest req) throws ApplicationException{
        if(Helper.isEmpty(partyModel.getName())) {
        	throw new IllegalArgumentException("Party Name is required");
        }else if(!Helper.isEmpty(partyModel.getEmail())) {
        	Helper.assertMsg(!Helper.isValid(partyModel.getEmail()), "Invalid email ID");
        }
        UserIO user = (UserIO) req.getAttribute("user");
        
        PartyEntity party = getPartyEntity(partyModel);
        
        if(party.getId() == null) {
        	party.setDeleted('0');
        	party.setCreated_by(user.getUserName());
        	party.setCreated_at(new Date());
        	genericService.add(party);
        }else {
        	party.setUpdated_at(new Date());
        	party.setUpdated_by(user.getUserName());
        	genericService.update(party);
        }
        
        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
    }
    
    @RequestMapping(value="/party/{id}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<PartyModel>> getParty(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
		if(id == null) {
			throw new IllegalArgumentException("Party ID is required");
		}
		PartyEntity party = this.genericService.findLongOne(new PartyEntity(), id);
		
		return new ResponseEntity<ErpResponse<PartyModel>>(new ErpResponse<PartyModel>(getPartyIo(party), false,"success", "200"), HttpStatus.OK);
	}
    
    @RequestMapping(value="/party", method=RequestMethod.GET, params ="!qry")
	public ResponseEntity<Grid<List<PartyModel>>> getParties(
							@RequestParam("from") int from,
							@RequestParam("rows") int rows,
							@RequestParam("sortBy") String sortBy,
							@RequestParam("sortOrder") int sortOrder,
							@RequestParam("filter") String filter,
							@RequestParam("globalFilter") String globalFilter
					){
		Grid<List<PartyModel>> response = new Grid<>();
		List<PartyModel> dataIo = new ArrayList<>();
		String sortDir = sortOrder == 1 ? "asc": "desc";
		
		JSONArray likeCondition = new JSONArray();
		likeCondition.put("name").put("panNo").put("vatNo").put("cstNo")
		.put("tanNo").put("brokerage").put("address").put("area").put("city")
		.put("state").put("country").put("pinCode").put("telNo").put("mobNo")
		.put("email").put("payterm").put("cinNo").put("hsnCode").put("gstNo")
		.put("created_by").put("updated_by");
		
		if(Helper.isEmpty(sortBy)) {
			sortBy = "created_at";
			sortDir = "desc";
		}
		
		List<PartyEntity> data = genericService.filterResult(new PartyEntity(), globalFilter, rows, from, sortBy, sortDir, 
				"party", new JSONArray(), likeCondition);
		Integer count = genericService.getFilteredRecords(new PartyEntity(), globalFilter, rows, from+1, 
				"party", new JSONArray(), likeCondition);
		
		dataIo = data.stream().map(master -> getPartyIo(master)).collect(Collectors.toList());
		
		response.setData(dataIo);
		response.setCount(count);
		
		return new ResponseEntity<Grid<List<PartyModel>>>(response, HttpStatus.OK);
	}
    
    @RequestMapping(value="/party", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<List<PartyModel>>> getParties(@RequestParam("qry") String query) {
    	List<Object[]> data = new ArrayList<>();
    	List<PartyModel> res = new ArrayList<>();
    	
    	if(Helper.isNotEmpty(query)) {
    		data = this.genericService.searchAndRestrict(PartyEntity.class, Arrays.asList("name"), 
    																query, Arrays.asList("name", "id"));
    	}
    	if(data.size() > 0) {
    		data.forEach(arr -> {
    			PartyModel party = new PartyModel();
    			party.setName((String) arr[0]);
    			party.setId((Long) arr[1]);
    			res.add(party);
    		});
    	}
    	return new ResponseEntity<ErpResponse<List<PartyModel>>>(new ErpResponse<List<PartyModel>>(res, false,"success", "200"), HttpStatus.OK);
	}
    
    @RequestMapping(value="/party/clone", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<String>> cloneParty(@RequestBody PartyModel model, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
    	if(model.getId() == null) {
    		throw new IllegalArgumentException("Party ID is required");
    	}
    	UserIO user = (UserIO) req.getAttribute("user");
    	
    	PartyEntity entity = this.genericService.findOne(PartyEntity.class, model.getId());
    	PartyEntity entityNew = clone(entity, user.getUserName());
    	this.genericService.add(entityNew);
		
		return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "200"), HttpStatus.CREATED);
	}
    
    private PartyEntity clone(PartyEntity entity, String username) {
    	PartyEntity entityNew = new PartyEntity();
    	
    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
    	BeanUtils.copyProperties(entity, entityNew, "created_at", "updated_at");
    	
    	entityNew.setId(null);
    	entityNew.setCreated_at(new Date());
    	entityNew.setUpdated_at(null);
    	entityNew.setUpdated_by(null);
    	entityNew.setCreated_by(username);
    	
    	return entityNew;
    }
    
    public static PartyModel getPartyIo(PartyEntity party)  {
    	PartyModel model = new PartyModel();
    	
    	BeanUtils.copyProperties(party, model);
    	
    	model.setCreated_at(party.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(party.getCreated_at()));
    	model.setUpdated_at(party.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(party.getUpdated_at()));
    	
    	return model;
    }
    
    public static PartyEntity getPartyEntity(PartyModel model) {
    	PartyEntity entity = new PartyEntity();
    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
    	BeanUtils.copyProperties(model, entity, "created_at", "updated_at");
    	
    	return entity;
    }
    
    
    /************************ Shade Controller ***************************/
    
    @RequestMapping(value="/shade", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<String>> saveShade(@RequestBody ShadeModel shadeModel, HttpServletRequest req) throws ApplicationException{
        if(Helper.isEmpty(shadeModel.getName())) {
        	throw new IllegalArgumentException("Shade Name is required");
        }if(shadeModel.getParty() == null) {
        	throw new IllegalArgumentException("Party is required");
        }if(shadeModel.getColor() == null) {
        	throw new IllegalArgumentException("Color is required");
        }
        
        UserIO user = (UserIO) req.getAttribute("user");
        
        ShadeEntity shade = getShadeEntity(shadeModel, this.genericService);
        
        if(shade.getId() == null) {
        	shade.setDeleted('0');
        	shade.setCreated_by(user.getUserName());
        	shade.setCreated_at(new Date());
        	genericService.saveOrUpdate(shade);
        }else {
        	shade.setUpdated_at(new Date());
        	shade.setUpdated_by(user.getUserName());
        	removeDeletedShades(shade);
        	genericService.update(shade);
        }
        
        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
    }
    
    private void removeDeletedShades(ShadeEntity shade) throws ApplicationException {
    	List<ShadeDetailEntity> dbshades = this.genericService.getShade(shade.getId()).getDetails();
    	
    	for(ShadeDetailEntity dbshade: dbshades) {
    		ShadeDetailEntity matched = shade.getDetails().stream().filter(x -> x.getId() == dbshade.getId()).findFirst().orElse(null);
    		if(matched == null) {
    			this.genericService.delete(dbshade);
    		}
    	}
    }
    
    @RequestMapping(value="/shade/{id}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<ShadeModel>> getShade(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
		if(id == null) {
			throw new IllegalArgumentException("Shade ID is required");
		}
		ShadeEntity entity = this.genericService.getShade(id);
		
		return new ResponseEntity<ErpResponse<ShadeModel>>(new ErpResponse<ShadeModel>(getShadeIo(entity), false,"success", "200"), HttpStatus.OK);
	}
    
    @RequestMapping(value="/shade", method=RequestMethod.GET, params ="!qry")
	public ResponseEntity<Grid<List<ShadeEntity>>> getShades(
							@RequestParam("from") int from,
							@RequestParam("rows") int rows,
							@RequestParam("sortBy") String sortBy,
							@RequestParam("sortOrder") int sortOrder,
							@RequestParam("filter") String filter,
							@RequestParam("globalFilter") String globalFilter
					){
		Grid<List<ShadeEntity>> response = new Grid<>();
		//List<ShadeModel> dataIo = new ArrayList<>();
		String sortDir = sortOrder == 1 ? "asc": "desc";
		
		JSONArray likeCondition = new JSONArray();
		likeCondition.put("name").put("party.name").put("color.name")
		.put("created_by").put("updated_by");
		
		JSONArray entitySubAlias = new JSONArray();
		entitySubAlias.put("party").put("color");
		
		if(Helper.isEmpty(sortBy)) {
			sortBy = "created_at";
			sortDir = "desc";
		}
		
		List<ShadeEntity> data = genericService.filterResult(new ShadeEntity(), globalFilter, rows, from, sortBy, sortDir, 
				"shade", entitySubAlias, likeCondition);
		Integer count = genericService.getFilteredRecords(new ShadeEntity(), globalFilter, rows, from+1, 
				"shade", entitySubAlias, likeCondition);
		
		//dataIo = data.stream().map(master -> getShadeIo(master)).collect(Collectors.toList());
		data.forEach(x -> {
			x.setDetails(null);
		});
		response.setData(data);
		response.setCount(count);
		
		return new ResponseEntity<Grid<List<ShadeEntity>>>(response, HttpStatus.OK);
	}
    
    @RequestMapping(value="/shade", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<List<ShadeModel>>> getShade(@RequestParam("qry") String query) {
    	List<Object[]> data = new ArrayList<>();
    	List<ShadeModel> res = new ArrayList<>();
    	
		data = this.genericService.searchAndRestrict(ShadeEntity.class, Arrays.asList("name"), 
    																query, Arrays.asList("name", "id"));
    	if(data.size() > 0) {
    		data.forEach(arr -> {
    			ShadeModel party = new ShadeModel();
    			party.setName((String) arr[0]);
    			party.setId((Long) arr[1]);
    			res.add(party);
    		});
    	}
    	return new ResponseEntity<ErpResponse<List<ShadeModel>>>(new ErpResponse<List<ShadeModel>>(res, false,"success", "200"), HttpStatus.OK);
	}
    
    @RequestMapping(value="/shade/clone", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<String>> cloneShade(@RequestBody ShadeModel model, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
    	if(model.getId() == null) {
    		throw new IllegalArgumentException("Shade ID is required");
    	}
    	UserIO user = (UserIO) req.getAttribute("user");
    	
    	ShadeEntity entity = this.genericService.findOne(ShadeEntity.class, model.getId());
    	ShadeEntity entityNew = clone(entity, user.getUserName());
    	this.genericService.add(entityNew);
		
		return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "200"), HttpStatus.CREATED);
	}
    
    @RequestMapping(value="/party/shade", method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<ErpResponse<List<ShadeModel>>> getPartyShade(@RequestParam("partyId") Long partyId) throws ApplicationException{
    	QueryObject<Long> partyRestrict = new QueryObject<Long>("party.id", partyId, "l", "eq");
    	List<String> subAliasList = Arrays.asList("party");
    	
    	List<ShadeEntity> data = this.genericService.findByFields(ShadeEntity.class, Arrays.asList(partyRestrict), "shade", subAliasList);
    	List<ShadeModel> res = new ArrayList<>();
    	
    	if(data.size() > 0) {
    		data.forEach(arr -> {
    			ShadeModel shade = new ShadeModel();
    			shade.setName(arr.getName());
    			shade.setId(arr.getId());
    			res.add(shade);
    		});
    	}
    	return new ResponseEntity<ErpResponse<List<ShadeModel>>>(new ErpResponse<List<ShadeModel>>(res, false,"success", "200"), HttpStatus.OK);
    	
    }
    
    private ShadeEntity clone(ShadeEntity entity, String username) {
    	ShadeEntity entityNew = new ShadeEntity();
    	
    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
    	BeanUtils.copyProperties(entity, entityNew, "created_at", "updated_at");
    	
    	entityNew.setId(null);
    	entityNew.setCreated_at(new Date());
    	entityNew.setUpdated_at(null);
    	entityNew.setUpdated_by(null);
    	entityNew.setCreated_by(username);
    	
    	return entityNew;
    }
    
    public static ShadeModel getShadeIo(ShadeEntity shade)  {
    	ShadeModel model = new ShadeModel();
    	
    	BeanUtils.copyProperties(shade, model);
    	
    	model.setCreated_at(shade.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(shade.getCreated_at()));
    	model.setUpdated_at(shade.getUpdated_at() == null ? "" : DateUtil.formateDateWithTime(shade.getUpdated_at()));
    	
    	return model;
    }
    
    public static ShadeEntity getShadeEntity(ShadeModel model, GenericService genericService) {
    	ShadeEntity entity = new ShadeEntity();
    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
    	BeanUtils.copyProperties(model, entity, "created_at", "updated_at", "details");
    	
    	if(model.getDetails()!=null && model.getDetails().size() > 0) {
    		
    		List<ShadeDetailEntity> models = model.getDetails();
    		entity.setDetails(new ArrayList<>());
    		models.forEach(x -> {
    			
    			if(x.getId()==null) {
    				ShadeDetailEntity detailmodel = new ShadeDetailEntity();
    				detailmodel.setColor(x.getColor());
    				detailmodel.setCreated_at(new Date());
    				detailmodel.setDeleted('0');
    				detailmodel.setShade(entity);
    				detailmodel.setWeight(x.getWeight());
    				entity.getDetails().add(detailmodel);
    			}else {
    				ShadeDetailEntity detailmodel = genericService.findLongOne(new ShadeDetailEntity(), x.getId());
    				detailmodel.setColor(x.getColor());
    				detailmodel.setWeight(x.getWeight());
    				detailmodel.setUpdated_at(new Date());
    				entity.getDetails().add(detailmodel);
    			}
    		});
    		
    	}else {
    		entity.setDetails(new ArrayList<>());
    	}
    	
    	return entity;
    }
    

}
