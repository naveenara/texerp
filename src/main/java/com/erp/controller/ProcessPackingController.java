package com.erp.controller;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.json.JSONArray;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.DyeingProgmDetailEntity;
import com.erp.entity.DyeingProgmDetailEntityV2;
import com.erp.entity.DyeingProgmEntityV2;
import com.erp.entity.GreyInwardEntity;
import com.erp.entity.LotEntryEntity;
import com.erp.entity.MasterEntity;
import com.erp.entity.PartyEntity;
import com.erp.entity.ProcessPackingEntity;
import com.erp.entity.ShadeEntity;
import com.erp.exception.ApplicationException;
import com.erp.helper.Helper;
import com.erp.rest.model.DyeingProgmDetailModel;
import com.erp.rest.model.DyeingProgmDetailModelV2;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.GreyInwardModel;
import com.erp.rest.model.Grid;
import com.erp.rest.model.LotEntryModel;
import com.erp.rest.model.ProcessPackingModel;
import com.erp.rest.model.UserIO;
import com.erp.rest.service.ProcessPackingService;
import com.erp.service.GenericService;
import com.mysql.jdbc.StringUtils;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/packing")
public class ProcessPackingController {

	@Resource
	private GenericService genericService;
	
	@Resource
	private ProcessPackingService ppService;
	
	 @Transactional
	 @RequestMapping(value="", method=RequestMethod.POST)
     public ResponseEntity<ErpResponse<String>> save(@RequestBody ProcessPackingModel model, HttpServletRequest req) throws ApplicationException, ParseException{
		    validateRequest(model);
	        UserIO user = (UserIO) req.getAttribute("user");
	        
	        ProcessPackingEntity entity = getEntity(model);
	        
	        if(entity.getId() == null) {
	        	Helper helper = new Helper();
		        String yearFormat = helper.getCurrentYearFormat();
		        
		        entity.setDeleted('0');
		        entity.setCreated_by(user.getUserName());
		        entity.setCreated_at(new Date());
		        entity.setYearFormat(yearFormat);
		        entity.setSerialNo(getSerialNo());
		        entity.setInvoiceDone(false);
		        
		        DyeingProgmDetailEntityV2 detailEntity = this.genericService.findLongOne(new DyeingProgmDetailEntityV2(), entity.getDetail().getId());
		        detailEntity.setPackingDone(true);
		        genericService.update(detailEntity);
		        
	        	genericService.add(entity);
	        }else {
	        	entity.setUpdated_at(new Date());
	        	entity.setUpdated_by(user.getUserName());
	        	
	        	if(model.getDetailId() != model.getDetail().getId()) {
	        		DyeingProgmDetailEntityV2 previousEntity = this.genericService.findLongOne(new DyeingProgmDetailEntityV2(), model.getDetailId());
	        		previousEntity.setPackingDone(false);
			        genericService.update(previousEntity);
	        		
			        DyeingProgmDetailEntityV2 detailEntity = this.genericService.findLongOne(new DyeingProgmDetailEntityV2(), entity.getDetail().getId());
			        detailEntity.setPackingDone(true);
			        genericService.update(detailEntity);
	        	}
	        	
	        	genericService.update(entity);
	        }
	        
	        
	        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
	    }
	    
	    @RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<ProcessPackingModel>> getEntity(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
			if(id == null) {
				throw new IllegalArgumentException("Process packing id is required");
			}
			ProcessPackingEntity entity = this.genericService.findLongOne(new ProcessPackingEntity(), id);
			
			return new ResponseEntity<ErpResponse<ProcessPackingModel>>(new ErpResponse<ProcessPackingModel>(getIoLazy(entity), false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="/data/{serialNo}", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<ProcessPackingModel>> getProcessPackingSerialNo(@PathVariable("serialNo") String serialNo) throws NoSuchAlgorithmException, ApplicationException {
			if(org.springframework.util.StringUtils.isEmpty(serialNo)) {
				throw new IllegalArgumentException("Serial number is required");
			}
			ProcessPackingEntity entity = this.genericService.getByField(new ProcessPackingEntity(), "serialNo", serialNo);
			
			if(entity == null)
				throw new IllegalArgumentException("No record found for: "+serialNo);
			
			return new ResponseEntity<ErpResponse<ProcessPackingModel>>(new ErpResponse<ProcessPackingModel>(getIo(entity), false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="/nextserialNo", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<String>> getSerialNumber() {
	    	
	    	//Helper helper = new Helper();
	        //String yearFormat = helper.getCurrentYearFormat();
	    	//String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "GREY_INWARD", 3); 
	    	String nexVal = getSerialNo();
	    	
			return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>(nexVal, false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="/baleNo", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<Long>> getBaleNo() {
	    	
	    	//Helper helper = new Helper();
	        //String yearFormat = helper.getCurrentYearFormat();
	    	//String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "GREY_INWARD", 3); 
	    	Long nexVal = this.ppService.getNextBaleNo() + 1;
	    	
			return new ResponseEntity<ErpResponse<Long>>(new ErpResponse<Long>(nexVal, false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="/searchDyeing", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<List<DyeingProgmDetailModelV2>>> searchDyeing(@RequestParam("qry") String qry) {
	    	
	    	List<Object[]> data = ppService.searchDyeingDetails(qry);
	    	
	    	List<DyeingProgmDetailModelV2> dataIo = data.stream().map(mapper -> getModelFromObjectArr(mapper)).collect(Collectors.toList());
			return new ResponseEntity<ErpResponse<List<DyeingProgmDetailModelV2>>>(new ErpResponse<List<DyeingProgmDetailModelV2>>(dataIo, false,"success", "200"), HttpStatus.OK);
		}
	    
	    @RequestMapping(value="/search", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<List<ProcessPackingModel>>> searchPP(@RequestParam("qry") String qry,
																				@RequestParam("pid") Long pid) {
	    	
	    	List<Object[]> data = ppService.searchProcessPacking(qry, pid);
	    	
	    	List<ProcessPackingModel> dataIo = data.stream().map(mapper -> getModelFromObjectArrPP(mapper)).collect(Collectors.toList());
			return new ResponseEntity<ErpResponse<List<ProcessPackingModel>>>(new ErpResponse<List<ProcessPackingModel>>(dataIo, false,"success", "200"), HttpStatus.OK);
		}
	    
	    private ProcessPackingModel getModelFromObjectArrPP(Object[] data) {
	    	ProcessPackingModel model = new ProcessPackingModel();
	    	
	    	DyeingProgmDetailEntityV2 detail = new DyeingProgmDetailEntityV2();
	    	detail.setProgram(new DyeingProgmEntityV2());
	    	
	    	model.setDetail(detail);
	    	model.getDetail().setLot(new LotEntryEntity());
	    	model.getDetail().getLot().setGrey(new GreyInwardEntity());
	    	
	    	model.getDetail().setShade(new ShadeEntity());
	    	model.getDetail().getLot().getGrey().setQuality(new MasterEntity());
	    	model.getDetail().getLot().getGrey().setParty(new PartyEntity());
	    	
	    	model.setId((Long) data[0]);
	    	model.setSerialNo((String) data[1]);
	    	model.getDetail().getLot().getGrey().setLotNo((String) data[2]);
	    	model.setLrNo((String) data[3]);
	    	model.getDetail().setAddedThan((Integer) data[4]);
	    	model.setLength((Double) data[5]);
	    	model.setDeliveryDate((Date) data[6]);
	    	model.setBaleNo((Long) data[7]);
	    	model.getDetail().getLot().getGrey().getQuality().setName((String) data[8]);
	    	
	    	return model;
	    }
	    
	    private String getSerialNo() {
	    	Helper helper = new Helper();
	        String yearFormat = helper.getCurrentYearFormat();
	    	String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "PROCESS_PACKING", 3); 
	    	String nexVal = helper.getNextVal("PP", lastReq, yearFormat);
	    	
	    	return nexVal;
	    }
	    
	    @RequestMapping(value="", method=RequestMethod.GET)
		public ResponseEntity<Grid<List<ProcessPackingModel>>> getLotEntryList(
								@RequestParam("from") int from,
								@RequestParam("rows") int rows,
								@RequestParam("sortBy") String sortBy,
								@RequestParam("sortOrder") int sortOrder,
								@RequestParam("filter") String filter,
								@RequestParam("globalFilter") String globalFilter
						){
			Grid<List<ProcessPackingModel>> response = new Grid<>();
			List<ProcessPackingModel> dataIo = new ArrayList<>();
			String sortDir = sortOrder == 1 ? "asc": "desc";
			
			JSONArray likeCondition = new JSONArray();
			likeCondition.put("pp.serialNo").put("pp.lrNo").put("pp-fold")
			.put("pp.notes").put("pp.deliveryDate-dd")
			.put("party.name")
			.put("grey.lotNo")
			.put("detail.addedThan-i")
			.put("process.name")
			.put("shade.name")
			.put("program.serialNo")
			.put("program.programDate-dd")
			.put("pp.created_by").put("pp.updated_by").put("pp.created_at-dd");
			
			JSONArray entitySubAlias = new JSONArray();
			entitySubAlias.put("process").put("detail").put("detail.shade")
			.put("detail.lot").put("detail.program").put("lot.grey").put("grey.party")
			.put("grey.quality");
			
			if(StringUtils.isNullOrEmpty(sortBy)) {
				sortBy = "created_at";
				sortDir = "desc";
			}
			
			List<ProcessPackingEntity> data = genericService.filterResult(new ProcessPackingEntity(), globalFilter, rows, from, sortBy, sortDir, 
					"pp", entitySubAlias, likeCondition);
			Integer count = genericService.getFilteredRecords(new ProcessPackingEntity(), globalFilter, rows, from+1, 
					"pp", entitySubAlias, likeCondition);
			
			dataIo = data.stream().map(master -> getIoLazy(master)).collect(Collectors.toList());
			
			response.setData(dataIo);
			response.setCount(count);
			
			return new ResponseEntity<Grid<List<ProcessPackingModel>>>(response, HttpStatus.OK);
		}
	    
	    /*@RequestMapping(value="search/processPacking", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<List<GreyInwardModel>>> searchGrey(@RequestBody GreyInwardModel model, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
    		List<GreyInwardEntity> parties = this.genericService.searchGeneric(GreyInwardController.getGreyInwardEntity(model));
    		List<GreyInwardModel> dataIo = parties.stream().map(master -> GreyInwardController.getGreyInwardIo(master)).collect(Collectors.toList());
	        
    		if(dataIo.size() > Constants.MAX_RESULTS) {
    			throw new IllegalArgumentException("Too many records found: "+Constants.MAX_RESULTS+". Please narrow search criteria and try again");
    		}
    		
    		return new ResponseEntity<ErpResponse<List<GreyInwardModel>>>(new ErpResponse<List<GreyInwardModel>>(dataIo, false,"success", "200"), HttpStatus.OK);
		}*/
	    
	    /*@RequestMapping(value="search/serialNo", method=RequestMethod.GET)
	    public ResponseEntity<ErpResponse<List<String>>> searchGreyInward(@RequestParam("serialNo") String serialNo, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
    		
	    	if(StringUtils.isNullOrEmpty(serialNo))
	    		throw new IllegalArgumentException("Serial Number is required");
	    	
	    	List<String> results = this.genericService.getOneColumn(new LotEntryEntity(), serialNo, "serialNo");
    		return new ResponseEntity<ErpResponse<List<String>>>(new ErpResponse<List<String>>(results, false,"success", "200"), HttpStatus.OK);
		}*/
	    
	   /* @RequestMapping(value="search/lot", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<List<LotEntryModel>>> searchLotEntry(@RequestBody LotEntryModel model, HttpServletRequest req) throws ApplicationException, ParseException{
	    	List<LotEntryEntity> data = this.reportsService.searchLotEntry(model);
	    	List<LotEntryModel> response = data.stream().map(o -> getLotEntryIo(o)).collect(Collectors.toList());
    		return new ResponseEntity<ErpResponse<List<LotEntryModel>>>(new ErpResponse<List<LotEntryModel>>(response, false,"success", "200"), HttpStatus.OK);
		O}*/
	    
	    /*@RequestMapping(value="/clone", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<String>> cloneGreyInward(@RequestBody GreyInwardModel model, HttpServletRequest req) throws ApplicationException, IllegalArgumentException, IllegalAccessException{
	    	if(model.getId() == null) {
	    		throw new IllegalArgumentException("Grey Inward ID is required");
	    	}
	    	UserIO user = (UserIO) req.getAttribute("user");
	    	
	    	GreyInwardEntity entity = this.genericService.findOne(GreyInwardEntity.class, model.getId());
	    	GreyInwardEntity entityNew = clone(entity, user.getUserName());
	    	this.genericService.add(entityNew);
    		
    		return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>(entityNew.getSerialNo(), false,"success", "200"), HttpStatus.CREATED);
		}*/
	    
	    /*@RequestMapping(value="search", method=RequestMethod.POST)
	    public ResponseEntity<ErpResponse<List<LotEntryModel>>> searchGreyInwards(@RequestBody GreyInwardReportModel report, HttpServletRequest req) throws ApplicationException, ParseException{
	    	List<GreyInwardEntity> data = this.reportsService.searchGreyInward(report);
	    	List<GreyInwardModel> response = data.stream().map(o -> getGreyInwardIo(o)).collect(Collectors.toList());
    		return new ResponseEntity<ErpResponse<List<LotEntryModel>>>(new ErpResponse<List<LotEntryModel>>(response, false,"success", "200"), HttpStatus.OK);
		}*/
	    
	    
	    /***************************************** Private methods *******************************/
	    /*private GreyInwardEntity clone(GreyInwardEntity entity, String username) {
	    	GreyInwardEntity entityNew = new GreyInwardEntity();
	    	
	    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
	    	BeanUtils.copyProperties(entity, entityNew, "created_at", "updated_at");
	    	
	    	entityNew.setId(null);
	    	entityNew.setCreated_at(new Date());
	    	entityNew.setUpdated_at(null);
	    	entityNew.setUpdated_by(null);
	    	entityNew.setCreated_by(username);
	    	entityNew.setSerialNo(getSerialNo());
	    	
	    	return entityNew;
	    }*/
	    
	    private DyeingProgmDetailModel getDyeingDetailIo(DyeingProgmDetailEntity entity) {
	    	DyeingProgmDetailModel model = new DyeingProgmDetailModel();
	    	BeanUtils.copyProperties(entity, model);
	    	return model;
	    }
	    
	    private DyeingProgmDetailModelV2 getModelFromObjectArr(Object[] data){
	    	DyeingProgmDetailModelV2 model = new DyeingProgmDetailModelV2();
	    	model.setProgram(new DyeingProgmEntityV2());
	    	
	    	model.setLot(new LotEntryModel());
	    	model.getLot().setGrey(new GreyInwardModel());
	    	model.setShade(new ShadeEntity());
	    	model.getLot().getGrey().setQuality(new MasterEntity());
	    	model.getLot().getGrey().setParty(new PartyEntity());
	    	
	    	
	    	model.setId((Long) data[0]);
	    	//model.getProgram().getParty().setName((String) data[1]);
	    	model.getLot().getGrey().getParty().setName((String) data[1]);
	    	model.getLot().getGrey().setLotNo((String) data[2]);
	    	model.setAddedThan((Integer) data[3]);
	    	model.getShade().setName((String) data[4]);
	    	model.getProgram().setSerialNo((String) data[5]);
	    	model.getProgram().setProgramDate((Date) data[6]);
	    	model.getLot().getGrey().setBiltyNo((String) data[7]);
	    	model.getLot().getGrey().getQuality().setName((String) data[8]);
	    	
	    	return model;
	    }
	    
	    private ProcessPackingModel getIoLazy(ProcessPackingEntity entity)  {
	    	entity.getDetail().getProgram().setDetails(null);
	    	entity.getDetail().getShade().setDetails(null);
	    	ProcessPackingModel model = new ProcessPackingModel();
	    	BeanUtils.copyProperties(entity, model);
	    	return model;
	    }
	    
	    private ProcessPackingModel getIo(ProcessPackingEntity entity)  {
	    	ProcessPackingModel model = new ProcessPackingModel();
	    	BeanUtils.copyProperties(entity, model);
	    	return model;
	    }
	    
	    public static ProcessPackingEntity getEntity(ProcessPackingModel model) throws ParseException {
	    	ProcessPackingEntity entity = new ProcessPackingEntity();
	    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
	    	BeanUtils.copyProperties(model, entity, "created_at", "updated_at");
	    	
	    	return entity;
	    }
	    
	    private void validateRequest(ProcessPackingModel model) {
	    	Helper.assertMsg(Helper.isEmpty(model.getSerialNo()), "Serial number is required");
	    	Helper.assertMsg(Helper.isNull(model.getDeliveryDate()), "Delivery date is required");
	    	Helper.assertMsg(Helper.isNull(model.getLength()), "Length is required");
	    	Boolean packingDone = model.getDetail().getPackingDone();
	    	if(packingDone!=null && model.getId()==null) {
	    		Helper.assertMsg(packingDone, "Packing already done for this lot");
	    	}
	    }
	    
	
}
