package com.erp.controller;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.DyeingProgmDetailEntityV2;
import com.erp.entity.ProcessPackingDetailEntityV2;
import com.erp.entity.ProcessPackingEntityV2;
import com.erp.exception.ApplicationException;
import com.erp.helper.Helper;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.Grid;
import com.erp.rest.model.UserIO;
import com.erp.rest.service.ProcessPackingService;
import com.erp.service.GenericService;
import com.mysql.jdbc.StringUtils;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/packing/v2")
public class ProcessPackingControllerV2 {

	@Resource
	private GenericService genericService;
	
	@Resource
	private ProcessPackingService ppService;
	
	@RequestMapping(value="", method=RequestMethod.POST)
    public ResponseEntity<ErpResponse<String>> save(@RequestBody ProcessPackingEntityV2 entity, HttpServletRequest req) throws ApplicationException, ParseException{
		    validateRequest(entity);
	        UserIO user = (UserIO) req.getAttribute("user");
	        
	        updatePentity(entity, user);
	        
	        if(entity.getId() == null) {
	        	Helper helper = new Helper();
		        String yearFormat = helper.getCurrentYearFormat();
		        
		        entity.setDeleted('0');
		        entity.setCreated_by(user.getUserName());
		        entity.setCreated_at(new Date());
		        entity.setYearFormat(yearFormat);
		        entity.setSerialNo(getSerialNo());
		        entity.setInvoiceDone(false);
		        
	        	genericService.add(entity);
	        }else {
	        	entity.setUpdated_at(new Date());
	        	entity.setUpdated_by(user.getUserName());
	        	removeDeletedLots(entity);
	        	
	        	genericService.update(entity);
	        }
	        
	        updatePackingStatus(entity);
	        
	        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
	    }
	
	 @RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
	 public ResponseEntity<ErpResponse<ProcessPackingEntityV2>> getEntity(@PathVariable("id") Long id) throws NoSuchAlgorithmException, ApplicationException {
			if(id == null) {
				throw new IllegalArgumentException("Process packing id is required");
			}
			ProcessPackingEntityV2 entity = this.ppService.get(id);
			
			for(ProcessPackingDetailEntityV2 d: entity.getDetails()) {
				d.getDetail().getProgram().setDetails(null);
				d.setProcessEntity(null);
			}
			
			return new ResponseEntity<ErpResponse<ProcessPackingEntityV2>>(new ErpResponse<ProcessPackingEntityV2>(entity, false,"success", "200"), HttpStatus.OK);
	}
	
	 @RequestMapping(value="/data/{serialNo}", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<ProcessPackingEntityV2>> getProcessPackingSerialNo(@PathVariable("serialNo") String serialNo) throws NoSuchAlgorithmException, ApplicationException {
			if(org.springframework.util.StringUtils.isEmpty(serialNo)) {
				throw new IllegalArgumentException("Serial number is required");
			}
			ProcessPackingEntityV2 entity = this.genericService.getByField(new ProcessPackingEntityV2(), "serialNo", serialNo);
			
			if(entity == null)
				throw new IllegalArgumentException("No record found for: "+serialNo);
			
			return new ResponseEntity<ErpResponse<ProcessPackingEntityV2>>(new ErpResponse<ProcessPackingEntityV2>(entity, false,"success", "200"), HttpStatus.OK);
		}
	
	 @RequestMapping(value="/nextserialNo", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<String>> getSerialNumber() {
	    	
	    	//Helper helper = new Helper();
	        //String yearFormat = helper.getCurrentYearFormat();
	    	//String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "GREY_INWARD", 3); 
	    	String nexVal = getSerialNo();
	    	
			return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>(nexVal, false,"success", "200"), HttpStatus.OK);
		}
	 
	 @RequestMapping(value="/baleNo", method=RequestMethod.GET, produces="application/json")
		public ResponseEntity<ErpResponse<Long>> getBaleNo() {
	    	
	    	//Helper helper = new Helper();
	        //String yearFormat = helper.getCurrentYearFormat();
	    	//String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "GREY_INWARD", 3); 
	    	Long nexVal = this.ppService.getNextBaleNo() + 1;
	    	
			return new ResponseEntity<ErpResponse<Long>>(new ErpResponse<Long>(nexVal, false,"success", "200"), HttpStatus.OK);
		}
	 
	  @RequestMapping(value="", method=RequestMethod.GET)
	  public ResponseEntity<Grid<List<ProcessPackingEntityV2>>> getPackingList(
									@RequestParam("from") int from,
									@RequestParam("rows") int rows,
									@RequestParam("sortBy") String sortBy,
									@RequestParam("sortOrder") int sortOrder,
									@RequestParam("filter") String filter,
									@RequestParam("globalFilter") String globalFilter
							){
				Grid<List<ProcessPackingEntityV2>> response = new Grid<>();
				String sortDir = sortOrder == 1 ? "asc": "desc";
				
				JSONArray likeCondition = new JSONArray();
				likeCondition.put("pp.serialNo").put("pp.lotNo")
				.put("pp.deliveryDate-dd")
				.put("party.name")
				.put("pp.baleNo-i")
				.put("pp.created_by").put("pp.updated_by").put("pp.created_at-dd");
				
				JSONArray entitySubAlias = new JSONArray();
				entitySubAlias.put("party");
				
				if(StringUtils.isNullOrEmpty(sortBy)) {
					sortBy = "created_at";
					sortDir = "desc";
				}
				
				List<ProcessPackingEntityV2> data = genericService.filterResult(new ProcessPackingEntityV2(), globalFilter, rows, from, sortBy, sortDir, 
						"pp", entitySubAlias, likeCondition);
				Integer count = genericService.getFilteredRecords(new ProcessPackingEntityV2(), globalFilter, rows, from+1, 
						"pp", entitySubAlias, likeCondition);
				
				data = data.stream().map(master -> getIoLazy(master)).collect(Collectors.toList());
				
				response.setData(data);
				response.setCount(count);
				
				return new ResponseEntity<Grid<List<ProcessPackingEntityV2>>>(response, HttpStatus.OK);
			}
	  
	  
	  
	  private ProcessPackingEntityV2 getIoLazy(ProcessPackingEntityV2 entity)  {
	    	entity.setDetails(null);
	    	return entity;
     }
	 
	private void removeDeletedLots(ProcessPackingEntityV2 entity) throws ApplicationException {
		if(entity.getDetails()!=null && entity.getDetails().size() > 0) {
			ProcessPackingEntityV2 dbEntity = this.ppService.get(entity.getId());
			
			for(ProcessPackingDetailEntityV2 detail: dbEntity.getDetails()) {
				ProcessPackingDetailEntityV2 matched = entity.getDetails().stream().filter(x -> x.getId() == detail.getId()).findFirst().orElse(null);
				if(matched == null) {
					detail.getDetail().setPackingDone(false);
					this.genericService.update(detail.getDetail());
	    			this.genericService.delete(detail);
	    		}
			}
		}
	}
	
	private void updatePackingStatus(ProcessPackingEntityV2 entity) {
		if(entity.getDetails()!=null && entity.getDetails().size() > 0) {
        	entity.getDetails().forEach(x -> {
	        		DyeingProgmDetailEntityV2 detailEntity = this.genericService.findLongOne(new DyeingProgmDetailEntityV2(), x.getDetail().getId());
			        detailEntity.setPackingDone(true);
			        genericService.update(detailEntity);
        	});
        }
	}
	
	private void updatePentity(ProcessPackingEntityV2 entity, UserIO user) {
		if(entity.getDetails()==null || entity.getDetails().size() == 0) {
			entity.setLotNo(null);
			return;
		}
		
		List<ProcessPackingDetailEntityV2> details = entity.getDetails();
		
		StringBuilder lotNo = new StringBuilder();
		
		for(ProcessPackingDetailEntityV2 detail: details) {
			if(detail.getDetail()!=null) {
				lotNo.append(detail.getDetail().getLot().getGrey().getLotNo()).append(", ");
			}
			
			if(detail.getId()==null) {
				detail.setCreated_at(new Date());
				detail.setCreated_by(user.getUserName());
			}else {
				detail.setUpdated_at(new Date());
				detail.setUpdated_by(user.getUserName());
			}
			
			detail.setDeleted('0');
			detail.setProcessEntity(entity);
		}
		entity.setLotNo(lotNo.toString().substring(0, lotNo.toString().length()-2));
	}
	
	private String getSerialNo() {
    	Helper helper = new Helper();
        String yearFormat = helper.getCurrentYearFormat();
    	String lastReq = genericService.getNextNumber("SERIAL_NO", yearFormat, "PROCESS_PACKING_V2", 3); 
    	String nexVal = helper.getNextVal("PP", lastReq, yearFormat);
    	
    	return nexVal;
    }
	
	private void validateRequest(ProcessPackingEntityV2 model) {
    	Helper.assertMsg(Helper.isEmpty(model.getSerialNo()), "Serial number is required");
    	Helper.assertMsg(Helper.isNull(model.getDeliveryDate()), "Delivery date is required");
    	
    	if(model.getDetails()!=null && model.getDetails().size() > 0) {
    		model.getDetails().forEach(x -> {
    			Helper.assertMsg(Helper.isNull(x.getDetail().getLot().getGrey().getLength()), "Length is required");
    	    	Boolean packingDone = x.getDetail().getPackingDone();
    	    	if(packingDone!=null && packingDone==true && x.getId()==null) {
    	    		Helper.assertMsg(packingDone, "Packing already done for this lot: "+x.getDetail().getLot().getGrey().getLotNo());
    	    	}
    		});
    	}
    	
    	
    }
	
}
