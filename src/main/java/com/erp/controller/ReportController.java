package com.erp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.LotEntryEntity;
import com.erp.entity.ReportUserDataEntity;
import com.erp.entity.ReportUserDataEntityPK;
import com.erp.exception.ApplicationException;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.UserIO;
import com.erp.rest.model.report.PartyLotWiseModel;
import com.erp.rest.model.report.PartyLotWiseResponseModel;
import com.erp.rest.service.ReportsService;
import com.erp.service.GenericService;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/report")
public class ReportController {
	
	@Resource
	private ReportsService reportsService;
	
	@Resource
	private GenericService genericService;
	
	@RequestMapping(value="/partywise-lot", method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<ErpResponse<List<PartyLotWiseResponseModel>>> partyWiseLotWise(@RequestBody PartyLotWiseModel model) {
	
		List<LotEntryEntity> result = reportsService.getPartyLotWise(model);
		List<PartyLotWiseResponseModel> response = getPartyWiseLotResponse(result);
		
		return new ResponseEntity<ErpResponse<List<PartyLotWiseResponseModel>>>(new ErpResponse<List<PartyLotWiseResponseModel>>(response, false,"success", "200"), HttpStatus.OK);
	}
	
	private List<PartyLotWiseResponseModel> getPartyWiseLotResponse(List<LotEntryEntity> results){
		List<PartyLotWiseResponseModel> response = new ArrayList<>();
		for(LotEntryEntity result: results) {
			Long partyId = result.getGrey().getParty().getId();
			
			PartyLotWiseResponseModel partyDetail = response.stream().filter(x -> x.getPartyId() == partyId).findFirst().orElse(null);
			
			if(partyDetail == null) {
				PartyLotWiseResponseModel partyModel = new PartyLotWiseResponseModel();
				partyModel.addDetail(result);
				partyModel.setPartyId(partyId);
				partyModel.setPartyName(result.getGrey().getParty().getName());
				partyModel.setThan(partyModel.getThan() == null ? result.getReceived() : partyModel.getThan() + result.getReceived());
				partyModel.setBalance(partyModel.getBalance() == null ? result.getBalance() : partyModel.getBalance() + result.getBalance());
				partyModel.setSent(partyModel.getSent() == null ? (result.getReceived() - result.getBalance()) : partyModel.getSent() + (result.getReceived() - result.getBalance()));
				
				response.add(partyModel);
			}else {
				partyDetail.addDetail(result);
				
				partyDetail.setThan(partyDetail.getThan() == null ? result.getReceived() : partyDetail.getThan() + result.getReceived());
				partyDetail.setBalance(partyDetail.getBalance() == null ? result.getBalance() : partyDetail.getBalance() + result.getBalance());
				partyDetail.setSent(partyDetail.getSent() == null ? (result.getReceived() - result.getBalance()) : partyDetail.getSent() + (result.getReceived() - result.getBalance()));
				
			}
		}
		return response;
	}
	
	/**
	 * rtype -> reportType -> 1 - Party Lot Wise Fabric, 2 -> party wise cloth stock summary..
	 * typeId -> 1 -> party, 2 -> quality
	 * data -> data id
	 * @param data
	 * @return
	 * @throws ApplicationException 
	 */
	@RequestMapping(value="/userdata/{rtype}/{typeId}", method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<ErpResponse<Long>> updateReportUserData(@RequestBody List<Long> data, 
																		@PathVariable("rtype") Long rtype,
																		@PathVariable("typeId") Long typeId,
																		HttpServletRequest req) throws ApplicationException{
		UserIO user = (UserIO) req.getAttribute("user");
		this.reportsService.deleteUserReportData(rtype, typeId, user.getId());
		
		if(typeId == 1) {
			updatePartyData(rtype, typeId, user.getId(), data);
		}else if(typeId == 2) {
			updateQualityData(rtype, typeId, user.getId(), data);
		}
		
		return new ResponseEntity<ErpResponse<Long>>(new ErpResponse<Long>(user.getId(), false,"success", "200"), HttpStatus.OK);
	}

	private void updatePartyData(Long rtype, Long typeId, Long userId, List<Long> data) throws ApplicationException {
		if(data == null || data.size() == 0) {
			return;
		}else {
			for(Long x: data) {
				update(x, rtype, typeId, userId);
			}
			//data.forEach(x -> );
		}
	}
	private void updateQualityData(Long rtype, Long typeId, Long userId, List<Long> data) throws ApplicationException {
		if(data == null || data.size() == 0) {
			return;
		}else {
			for(Long x: data) {
				update(x, rtype, typeId, userId);
			}
		}
	}
	
	private void update(Long x, Long rtype, Long typeId, Long userId) throws ApplicationException {
		ReportUserDataEntityPK pk = new ReportUserDataEntityPK(rtype, typeId, userId, x);
		ReportUserDataEntity e = new ReportUserDataEntity();
		e.setPk(pk);
		this.genericService.add(e);
	}
}
