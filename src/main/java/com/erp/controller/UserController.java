package com.erp.controller;


import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.erp.entity.UserEntity;
import com.erp.exception.ApplicationException;
import com.erp.helper.Constants;
import com.erp.helper.DateUtil;
import com.erp.helper.EmailUtil;
import com.erp.helper.EncrptionUtil;
import com.erp.helper.Helper;
import com.erp.helper.UserHelper;
import com.erp.rest.model.ChangePasswordModel;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.Grid;
import com.erp.rest.model.UserIO;
import com.erp.rest.model.UserModel;
import com.erp.rest.service.UserService;
import com.erp.service.GenericService;
import com.mysql.jdbc.StringUtils;

@CrossOrigin(origins = "*", maxAge=-1, allowedHeaders = "*")
@RestController
@RequestMapping(value="/api/user")
public class UserController {

	@Resource
	private UserService userService;
	
	@Resource
	private GenericService genericService;
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ResponseEntity<ErpResponse<String>> login(@RequestBody UserModel userInput){
        return UserHelper.loginUser(userInput, userService);
    }
	
	@RequestMapping(value="", method=RequestMethod.POST, consumes="application/json", produces="application/json")
	public ResponseEntity<ErpResponse<String>> saveUser(@RequestBody UserModel user, HttpServletRequest req) throws NoSuchAlgorithmException, ApplicationException {
		
		UserHelper.validateUser(user, genericService, userService);
		UserIO userSession = (UserIO) req.getAttribute("user");
		
		UserEntity userDb = UserHelper.getUserEntity(user);
		
		if(user.getId() == null) {
			userDb.setCreated_by(userSession.getUserName());
			this.genericService.add(userDb);
		}else {
			userDb.setUpdated_by(userSession.getUserName());
			this.genericService.update(userDb);
		}
		return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"success", "201"), HttpStatus.CREATED);
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<ErpResponse<UserModel>> getUser(@PathVariable("id") Long id, HttpServletRequest req) throws NoSuchAlgorithmException, ApplicationException {
		
		if(id == -1)
		{
			UserIO user = (UserIO) req.getAttribute("user");
			id = user.getId();
		}
		
		UserEntity user = this.genericService.findLongOne(new UserEntity(), id);
		UserModel userModel = UserHelper.getUserModel(user);
		return new ResponseEntity<ErpResponse<UserModel>>(new ErpResponse<UserModel>(userModel, false,"success", "200"), HttpStatus.OK);
	}
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public ResponseEntity<Grid<List<UserModel>>> getUsers(
							@RequestParam("from") int from,
							@RequestParam("rows") int rows,
							@RequestParam("sortBy") String sortBy,
							@RequestParam("sortOrder") int sortOrder,
							@RequestParam("filter") String filter,
							@RequestParam("globalFilter") String globalFilter
					){
		Grid<List<UserModel>> response = new Grid<>();

		List<UserModel> dataIo = new ArrayList<>();
		
		String sortDir = sortOrder == 1 ? "asc": "desc";
		
		JSONArray likeCondition = new JSONArray();
		likeCondition.put("name").put("username").put("email");
		
		if(sortBy!=null && "active".equals(sortBy)) {
			sortBy = "deleted";
		}
		
		if(StringUtils.isNullOrEmpty(sortBy)) {
			sortBy = "created_at";
			sortDir = "desc";
		}
		
		List<UserEntity> data = genericService.filterResult(new UserEntity(), globalFilter, rows, from, sortBy, sortDir, "user", new JSONArray(), likeCondition);
		Integer count = genericService.getFilteredRecords(new UserEntity(), globalFilter, rows, from+1, "user", new JSONArray(), likeCondition);
		
		dataIo = data.stream().map(user -> getUserIo(user)).collect(Collectors.toList());
		
		response.setData(dataIo);
		response.setCount(count);
		
		return new ResponseEntity<Grid<List<UserModel>>>(response, HttpStatus.OK);
	}
	
    @RequestMapping(value="/change-password", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<ErpResponse<String>> updatePassword(@RequestBody ChangePasswordModel passwordInput, HttpServletRequest request) throws ApplicationException, NoSuchAlgorithmException{
        
        if(passwordInput == null || Helper.isEmpty(passwordInput.getNewPass()) || Helper.isEmpty(passwordInput.getOldPass())){
            throw new IllegalArgumentException("Invalid or missing password information");
        }
        
        UserIO data = (UserIO) request.getAttribute("user");
        UserEntity userDb = this.genericService.findOne(UserEntity.class, data.getId());
        
        String encPassword = EncrptionUtil.getSecurePassword(passwordInput.getOldPass());
        
        if(!userDb.getPassword().equals(encPassword)){
        	throw new IllegalArgumentException("Current password is incorrect");
        }
        
        String newPassEnc = EncrptionUtil.getSecurePassword(passwordInput.getNewPass());
        userDb.setPassword(newPassEnc);
        this.genericService.update(userDb);
        
        return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"Password updated successfully", "200"), HttpStatus.OK);
        
    }
    
    @RequestMapping(value="/forgotpassword", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<ErpResponse<String>> sendForgotPasswordEmail(@RequestBody UserModel model) throws NoSuchAlgorithmException{
        
        String type = null;
        String value = null;
        
        if(Helper.isNotEmpty(model.getUsername())) {
        	type = "username";
        	value = model.getUsername();
        }else if(Helper.isNotEmpty(model.getEmail())) {
        	if(!Helper.isValid(model.getEmail())) {
        		throw new IllegalArgumentException("Please enter a valid email id");
        	}
        	type = "email";
        	value = model.getEmail();
        }else {
        	throw new IllegalArgumentException("Username or Email is required");
        }
        
        String randomPassword = UserHelper.randomString(6);
        String encPassword = EncrptionUtil.getSecurePassword(randomPassword);
        
        UserEntity userDb = this.genericService.getByField(new UserEntity(), type, value);
        
        if(userDb == null)
        	throw new IllegalArgumentException("No user found for given "+type);
        
        userDb.setPassword(encPassword);
        this.genericService.update(userDb);
        
        
        String emailContent = "Dear "+userDb.getName()+", <BR /><BR />Please use below password to login and update your password. <BR /><BR /><span>Password: </span><span style='font-weight:bold'>"+randomPassword+"</span><BR /><BR />Best Refards,<BR />Admin";
        boolean mailSentError = EmailUtil.sendHtmlEmail("ERP Forgot password", Constants.ADMIN_EMAIL, new String[]{userDb.getEmail()}, emailContent);
        
        if(!mailSentError){
            System.out.println("New password: "+randomPassword);
            System.out.println("New enc password: "+encPassword);
            
            return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("success", false,"Temoprary password has been sent to email", "200"), HttpStatus.OK);
        }else{
        	return new ResponseEntity<ErpResponse<String>>(new ErpResponse<String>("error", true,"Something went wrong while sending email, please try later", "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	private UserModel getUserIo(UserEntity user) {
		UserModel model = new UserModel();
		
		model.setCreated_at(user.getCreated_at() == null ? "" : DateUtil.formateDateWithTime(user.getCreated_at()));
		model.setActive(user.isActive());
		model.setEmail(user.getEmail());
		model.setId(user.getId());
		model.setName(user.getName());
		model.setUsername(user.getUsername());
		
		return model;
	}
}
