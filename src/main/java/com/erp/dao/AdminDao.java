package com.erp.dao;

import java.util.Collection;

import com.erp.entity.DepartmentEntity;

public interface AdminDao {

    public Collection<DepartmentEntity> searchDept(DepartmentEntity obj);
    
}
