package com.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ChartsDao {

	public <E> List<Object[]> getDayWiseCount(E e, Date tillDate, String table);
	public Map<String, Integer> getTaskChart(Date tillDate);
	
}
