package com.erp.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;

import com.erp.entity.MasterEntity;
import com.erp.entity.ShadeEntity;
import com.erp.exception.ApplicationException;
import com.erp.helper.QueryObject;

/*import com.terp.entity.factory.StoreEmailEntity;
import com.terp.entity.masters.yarn.YarnMaster;*/

public interface GenericDao {

    public <E> void add(E e) throws ApplicationException;
    public <E> void saveOrUpdate(E e) throws ApplicationException;
    public <E> void delete(final Object object) throws ApplicationException;
    public <E> void delete(final Class<E> type,Long id) throws ApplicationException;
    public <E> E findOne(final Class<E> type, final Long id) throws ApplicationException;
    public <E> E findByCode(final Class<E> type, String code) throws ApplicationException;
    public <E> E findLongOne(E e, Long id);
    
    public <E> void update(E e);
    public <E> List<E> getAll(final Class<E> type);
    public <E> List<E> getAll(final Class<E> type, String colName, Character value);
    public <E> List<Object[]> searchAndRestrict(final Class<E> type, List<String> restriction, String value, List<String> projection);
    public List<MasterEntity> searchMaster(String qry, Character type);
    public <E> String getNextNumber(String yearFormat, String tableName, int length);
    public <E> String getNextNumber(String yearFormat, String tableName, int length, String type);
    
    public Long recordsTotal(String entity);
    public <E> Integer getFilteredRecords(E type, String qry, int length, int start, String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition) throws HibernateException, JSONException;
    public <E> List<E> filterResult(E type, String qry, int length, int start, String sortCol, String sortDir,String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition) throws HibernateException, JSONException;
    
    /*public List<YarnMaster> getMasterDetails(Integer[] types);*/

    public <E> List<String> getOneColumn(E type, String colName);
    public <E> List<String> getOneColumn(E type, String colName, Integer master_type);
    public <E> List<Double> getOneColumn(E type, Double inp, String property);
    public <E> List<Object[]> getDateOneRow(E type, Long id, List<String> fields);
    public <E> E getPrevEntry(E type, String docNo);

    /*public StoreEmailEntity getStoreEmailByType(String type);*/
    public void setBeamStatus(Integer jobCardId, String prop);

    public <E> List<String> getOneColumn(E type, String inp, String property);
    public <E> E addReturnId(E e);
    public <E> E getByName(E type, String name);

    public <E> E getByField(E type, String field, String value);
    public <E> E getByField(E type, String field, Integer value);
    public int deleteMultiple(String entity, Set<Integer> ids);
   // public <E, T> boolean deleteva(String entity, String id, List<T> parents2, List<String> aliasName);
    public <E, T> boolean deleteva(E entity, String id, List<T> listToCheck,  List<String> aliasName);

    
    public Long recordsTotal(String entity, String type, String value);
    public <E> Integer getFilteredRecords(E type, String qry1, int length, int start, String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition, String colName, String value) throws HibernateException, JSONException;
    public <E> List<E> filterResult(E type, String qry1, int length, int start, String sortCol, String sortDir,String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition, String colName, String value) throws HibernateException, JSONException;

    public <E> String getNextNumber(String colName, String yearFormat, String tableName, int length);
    
    public <E> List<E> searchGeneric(E type) throws IllegalArgumentException, IllegalAccessException;
    public <E> List<E> findByFields(final Class<E> type, List<QueryObject> query, String alias, List<String> subAliasList) throws ApplicationException;
    
    public ShadeEntity getShade(Long id);
}
