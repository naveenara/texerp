package com.erp.dao;

import java.util.List;

import com.erp.entity.InvoiceDetailEntity;
import com.erp.entity.InvoiceEntity;

public interface InvoiceDao {

	public InvoiceEntity get(Long id);
	public List<InvoiceDetailEntity> getDetails(Long dpId);
	
}
