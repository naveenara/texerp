package com.erp.dao;

import java.util.List;

import com.erp.entity.ProcessPackingEntityV2;

public interface ProcessPackingDao {

	public ProcessPackingEntityV2 get(Long id);
	public List<Object[]> searchDyeingDetails(String qry);
	public Long getNextBaleNo();
	public List<Object[]> searchProcessPacking(String qry, Long partyId);
	
}
