package com.erp.dao;

import java.text.ParseException;
import java.util.List;

import com.erp.entity.DyeingProgmDetailEntity;
import com.erp.entity.GreyInwardEntity;
import com.erp.entity.LotEntryEntity;
import com.erp.rest.model.FabricStockReportModel;
import com.erp.rest.model.GreyInwardReportModel;
import com.erp.rest.model.LotEntryModel;
import com.erp.rest.model.LotEntryReportModel;
import com.erp.rest.model.report.PartyLotWiseModel;

public interface ReportsDao {

	public List<GreyInwardEntity> searchGreyInward(GreyInwardReportModel report) throws ParseException;
	public List<LotEntryEntity> searchLotEntry(LotEntryModel model) throws ParseException;
	public List<DyeingProgmDetailEntity> searchDyeingDetailsReport(FabricStockReportModel report) throws ParseException;
	public List<LotEntryEntity> searchLotEntryReport(LotEntryReportModel report) throws ParseException;
	public List<LotEntryEntity> getPartyLotWise(PartyLotWiseModel model);
	public void deleteUserReportData(Long rtype, Long typeId, Long userId);
	
}
