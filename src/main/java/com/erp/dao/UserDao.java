package com.erp.dao;

import com.erp.entity.UserEntity;

public interface UserDao {

	public UserEntity validateUser(String userName, String password);
	
	public UserEntity getUserEdit(String username, String email, Long id);
	
}
