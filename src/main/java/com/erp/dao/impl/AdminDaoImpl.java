package com.erp.dao.impl;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.dao.AdminDao;
import com.erp.entity.DepartmentEntity;

@Repository("adminDao")
public class AdminDaoImpl implements AdminDao{

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public Collection<DepartmentEntity> searchDept(DepartmentEntity obj) {
        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(DepartmentEntity.class);
        
        if(!isEmpty(obj.getName())){
            cr.add(Restrictions.like("name", obj.getName(), MatchMode.ANYWHERE));
        }
        if(!isEmpty(obj.getCode())){
            cr.add(Restrictions.like("code", obj.getCode(), MatchMode.ANYWHERE));
        }
        
        Collection<DepartmentEntity> result = cr.list();
        return result;
    }
    
    private boolean isEmpty(String s){
        if(s != null && !s.isEmpty()){
               return false;
        }else{
            return true;
        }
    }

}
