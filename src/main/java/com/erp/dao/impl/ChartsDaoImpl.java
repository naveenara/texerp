package com.erp.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
//import org.testng.collections.Maps;

import com.erp.dao.ChartsDao;
import com.erp.entity.DyeingProgmDetailEntityV2;
import com.erp.entity.LotEntryEntity;
import com.erp.entity.ProcessPackingEntityV2;

import jersey.repackaged.com.google.common.collect.Maps;

@Repository("chartsDao")
public class ChartsDaoImpl implements ChartsDao{

	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
	public <E> List<Object[]> getDayWiseCount(E e, Date tillDate, String table){
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(e.getClass());
		String sql = "select date(created_at) as ndate, count(*) from "+table;
		if(tillDate!=null) {
			sql += " where deleted !='1' and created_at >= date(:inDate)";
			sql += " group by ndate";
			return this.sessionFactory.getCurrentSession().createSQLQuery(sql).setParameter("inDate", tillDate).list();
		}else {
			sql += " group by ndate";
			return this.sessionFactory.getCurrentSession().createSQLQuery(sql).list();
		}
			 
		
	}
	
	@Override
	public Map<String, Integer> getTaskChart(Date tillDate) {
		
		Map<String, Integer> response = Maps.newHashMap();
		
		/**
		 * How many process were completed in time
		 */
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(ProcessPackingEntityV2.class, "process");
		cr.setProjection(Projections.projectionList().add(Projections.rowCount(), "processDone"));
		if(tillDate!=null) {
			cr.add(Restrictions.ge("process.created_at", tillDate));
		}
		Integer processDone = ((Long)cr.uniqueResult()).intValue();
		response.put("Process Completed", processDone);
		
		/**
		 * How many shades were created from lot for which packing is not done in given time
		 */
		Criteria cr2 = this.sessionFactory.getCurrentSession().createCriteria(DyeingProgmDetailEntityV2.class, "dd");
		cr2.setProjection(Projections.projectionList().add(Projections.rowCount(), "processPending"));
		cr2.add(Restrictions.eq("dd.packingDone", false));
		if(tillDate!=null) {
			cr2.add(Restrictions.ge("dd.created_at", tillDate));
		}
		Integer processPending = ((Long)cr2.uniqueResult()).intValue();
		response.put("Process Pending", processPending);
		
		/**
		 * How many lots were created and completed in given time
		 */
		Criteria cr3 = this.sessionFactory.getCurrentSession().createCriteria(LotEntryEntity.class, "lot");
		cr3.setProjection(Projections.projectionList().add(Projections.groupProperty("completed"), "completed").add(Projections.rowCount(), "count"));
		if(tillDate!=null) {
			cr3.add(Restrictions.ge("lot.created_at", tillDate));
		}
		List<Object[]> lots = cr3.list();
		
		if(lots.size() == 0) {
			response.put("Lot Completed", 0);
			response.put("Lot In progress", 0);
		}else if(lots.size() == 1) {
			Object[] one = lots.get(0);
			if((Boolean) one[0] == false) {
				response.put("Lot Completed", 0);
				response.put("Lot In progress", ((Long) one[1]).intValue());
			}else if((Boolean) one[0] == true) {
				response.put("Lot Completed", ((Long) one[1]).intValue());
				response.put("Lot In progress", 0);
			}
		}else if(lots.size() == 2) {
			Object[] one = lots.get(0);
			Object[] two = lots.get(1);
			
			if((Boolean) one[0] == false) {
				response.put("Lot Completed", ((Long) two[1]).intValue());
				response.put("Lot In progress", ((Long) one[1]).intValue());
			}else if((Boolean) one[1] == true) {
				response.put("Lot Completed", ((Long) one[1]).intValue());
				response.put("Lot In progress", ((Long) two[1]).intValue());
			}
		}
		
		/*********************************/
		String dyeingDone = "select count(*), DEYING_PROGRAM_ID from\r\n" + 
				"\r\n" + 
				"	DEYING_PROGRAM_DETAIL_V2\r\n" + 
				"\r\n" + 
				"where deleted !='1' and packing_done = '1' and ";
		
		if(tillDate!=null) {
			dyeingDone += " created_at >= date(:inDate) and ";
		}
		
		dyeingDone += "\r\n" + 
				"\r\n" + 
				"DEYING_PROGRAM_ID not in (\r\n" + 
				"		select DEYING_PROGRAM_ID from DEYING_PROGRAM_DETAIL_V2\r\n" + 
				"		where deleted != '1' and packing_done = '0'\r\n" + 
				"		group by 	DEYING_PROGRAM_ID\r\n" + 
				")\r\n" + 
				"\r\n" + 
				"group by 	DEYING_PROGRAM_ID\r\n" + 
				"having count(*) > 0";
		List dyeingDoneList = this.sessionFactory.getCurrentSession().createSQLQuery(dyeingDone).setParameter("inDate", tillDate).list();
		response.put("Dyeing Completed", dyeingDoneList.size());
		
		String dyeingPending = "select DEYING_PROGRAM_ID from DEYING_PROGRAM_DETAIL_V2\r\n" + 
				"		where created_at >= date(:inDate) and deleted != '1' and packing_done = '0'\r\n" + 
				"		group by 	DEYING_PROGRAM_ID";
		List dyeingPendingList = this.sessionFactory.getCurrentSession().createSQLQuery(dyeingPending).setParameter("inDate", tillDate).list();
		response.put("Dyeing In Progress", dyeingPendingList.size());
		
		return response;
	}
	
}
