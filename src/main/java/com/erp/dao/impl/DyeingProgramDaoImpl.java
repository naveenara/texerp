package com.erp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.dao.DyeingProgramDao;
import com.erp.entity.DyeingProgmDetailEntity;
import com.erp.entity.DyeingProgmDetailEntityV2;
import com.erp.entity.DyeingProgmEntity;
import com.erp.entity.DyeingProgmEntityV2;
import com.erp.helper.Helper;

@Repository("dpDao")
public class DyeingProgramDaoImpl implements DyeingProgramDao {

	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
	public DyeingProgmEntity getDyeingProgram(Long id) {
		DyeingProgmEntity entity = (DyeingProgmEntity) this.sessionFactory.getCurrentSession().get(DyeingProgmEntity.class, id);
		Hibernate.initialize(entity.getDetails());
		return entity;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DyeingProgmDetailEntity> getDetails(Long dpId){
		
		@SuppressWarnings("deprecation")
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(DyeingProgmDetailEntity.class, "details").
						createAlias("details.program", "program", CriteriaSpecification.INNER_JOIN);
		
		cr.setProjection(Projections.projectionList().add(Projections.property("details.addedThan"), "addedThan").add(Projections.property("details.id"), "id")
				.add(Projections.property("details.lot"), "lot"));
		
		cr.add(Restrictions.eq("program.id", dpId));
		cr.setResultTransformer(Transformers.aliasToBean(DyeingProgmDetailEntity.class));
		
		return cr.list();
	}
	
	
	@Override
	public List<DyeingProgmDetailEntityV2> searchProgramForPacking(Long partyId, String qry){
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(DyeingProgmDetailEntityV2.class, "details")
				.createAlias("details.lot", "lot", CriteriaSpecification.INNER_JOIN)
				.createAlias("lot.grey", "grey", CriteriaSpecification.INNER_JOIN)
				.createAlias("grey.party", "party", CriteriaSpecification.INNER_JOIN);
		
		cr.add(Restrictions.eq("party.id", partyId));
		
		if(Helper.isNotEmpty(qry)) {
			cr.add(Restrictions.ilike("grey.lotNo", qry, MatchMode.ANYWHERE));
		}		
		
		cr.add(Restrictions.eq("packingDone", false));
		
		//cr.setResultTransformer(Transformers.aliasToBean(DyeingProgmDetailEntityV2.class));
		
		return cr.list();		
	}
	
	@Override
	public DyeingProgmEntityV2 getDyeingProgramV2(Long id) {
		DyeingProgmEntityV2 entity = (DyeingProgmEntityV2) this.sessionFactory.getCurrentSession().get(DyeingProgmEntityV2.class, id);
		Hibernate.initialize(entity.getDetails());
		return entity;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DyeingProgmDetailEntityV2> getDetailsV2(Long dpId){
		
		@SuppressWarnings("deprecation")
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(DyeingProgmDetailEntityV2.class, "details").
						createAlias("details.program", "program", CriteriaSpecification.INNER_JOIN);
		
		cr.setProjection(Projections.projectionList().add(Projections.property("details.addedThan"), "addedThan").add(Projections.property("details.id"), "id")
				.add(Projections.property("details.lot"), "lot"));
		
		cr.add(Restrictions.eq("program.id", dpId));
		cr.setResultTransformer(Transformers.aliasToBean(DyeingProgmDetailEntityV2.class));
		
		return cr.list();
	}
	
}
