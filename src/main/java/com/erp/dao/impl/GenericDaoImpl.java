package com.erp.dao.impl;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.erp.dao.GenericDao;
import com.erp.entity.MasterEntity;
import com.erp.entity.ShadeEntity;
import com.erp.exception.ApplicationException;
import com.erp.helper.Helper;
import com.erp.helper.QueryObject;
import com.mysql.jdbc.StringUtils;

@Repository("gdao")
@Transactional
public class GenericDaoImpl implements GenericDao{

    @Autowired
    private SessionFactory sessionFactory;
    private Session session=null;
    
    @Override
    public <E> void add(E e) throws ApplicationException{
        try{
            this.sessionFactory.getCurrentSession().persist(e);
        }catch(Exception ex){
            throw new ApplicationException("Error adding new entity.", ex);
        }
    }
    
    @Override
    public <E> void saveOrUpdate(E e) throws ApplicationException{
        try{
            this.sessionFactory.getCurrentSession().saveOrUpdate(e);
        }catch(Exception ex){
            throw new ApplicationException("Error adding new entity.", ex);
        }
    }
    
    @Override
    public <E> E addReturnId(E e){
        this.sessionFactory.getCurrentSession().persist(e);
        return e;
    }
    
    @Override
    public <E> void delete(final Object object) throws ApplicationException{
        try{
            this.sessionFactory.getCurrentSession().delete(object);
        }catch(Exception e){
            throw new ApplicationException("Error deleting entity.", e);
        }
    }
    
    @Override
    public <E> void delete(final Class<E> type,Long id) throws ApplicationException{
        E e = findOne(type, id);
        if(e != null)
            this.sessionFactory.getCurrentSession().delete(e);
    }
    
    @Override
    public int deleteMultiple(String entity, Set<Integer> ids){
        String qry = "delete from "+entity+" where id in (:ids)";
        Query hql = this.sessionFactory.getCurrentSession().createQuery(qry);
        hql.setParameterList("ids", ids);
        
        int i = hql.executeUpdate();     
        return i;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <E> E findOne(final Class<E> type, Long id) throws ApplicationException{
        
        try{
            E entity = (E) this.sessionFactory.getCurrentSession().get(type, id);
            return entity;
        }catch(Exception e){
            throw new ApplicationException("Error finding entity.", e);
        }
        //return (E) this.sessionFactory.getCurrentSession().createCriteria(e.getClass()).add(Restrictions.idEq(id)).uniqueResult();
    }
    
    @Override
    public <E> E findByCode(final Class<E> type, String code) throws ApplicationException{
        try{
            Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type);
            cr.add(Restrictions.eq("code", code));
            List result = cr.list();
            if(result!=null && result.size() > 0){
                return (E) result.get(0);
            }
            return null;
        }catch(Exception e){
            throw new ApplicationException("Error finding entity by code.", e);
        }
    }
    
    @Override
    public <E> void update(E e){
       this.sessionFactory.getCurrentSession().update(e);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <E> List<E> getAll(final Class<E> type){
        Criteria cr =  this.sessionFactory.getCurrentSession().createCriteria(type).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return cr.list();
    }
    
    @Override
    public <E> List<E> getAll(final Class<E> type, String colName, Character value){
        Criteria cr =  this.sessionFactory.getCurrentSession().createCriteria(type).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        cr.add(Restrictions.eq(colName, value));
        return cr.list();
    }
    
    @Override
    public <E> List<Object[]> searchAndRestrict(final Class<E> type, List<String> restriction, String value, List<String> projection){
        Criteria cr =  this.sessionFactory.getCurrentSession().createCriteria(type);
        
        ProjectionList plist = Projections.projectionList();
        
        if(projection!=null && projection.size() > 0) {
        	projection.forEach(x -> {
        		plist.add(Projections.property(x));
        	});
        	cr.setProjection(plist);
        }
        
        
        if(restriction != null && restriction.size() > 0 && Helper.isNotEmpty(value)) {
        	restriction.forEach(x -> cr.add(Restrictions.ilike(x, value, MatchMode.ANYWHERE)));
        }
        

		//cr.setResultTransformer(Transformers.aliasToBean(type));
        return cr.list();
    }
    
    
    @Override
    public List<MasterEntity> searchMaster(String qry, Character type){
        Criteria cr =  this.sessionFactory.getCurrentSession().createCriteria(MasterEntity.class);
        cr.add(Restrictions.ilike("name", qry, MatchMode.ANYWHERE));
        cr.add(Restrictions.eq("type", type));
		//cr.setResultTransformer(Transformers.aliasToBean(type));
        return cr.list();
    }
    
    @Override
    public <E> String getNextNumber(String yearFormat, String tableName, int length) {

        Session session = this.sessionFactory.getCurrentSession();
        String qry1 = "select concat(max(CONVERT(SUBSTRING(d.docNo, "+length+"), SIGNED INTEGER)), '') from "+tableName+" as d where deleted = '0' and d.yearFormat = "+yearFormat;
        //System.out.println(qry1);
        Query qry = session.createSQLQuery(qry1);

        try{
            byte[] result = (byte[]) qry.list().get(0);
            
            if(result == null)
                return null;
            else
                return new String(result);
            
            }catch(Exception e){
                String result = (String) qry.list().get(0);
                
                if(result == null)
                    return null;
                else
                    return new String(result);
            }
    }
    
    
    
    
    @Override
    public <E> String getNextNumber(String yearFormat, String tableName, int length, String type) {

        Session session = this.sessionFactory.getCurrentSession();
        String qry1 = "select concat(max(CONVERT(SUBSTRING(d.docNo, "+length+"), SIGNED INTEGER)), '') from "+tableName+" as d where deleted = '0' and type = '"+type+"' and d.yearFormat = "+yearFormat;
        //System.out.println(qry1);
        Query qry = session.createSQLQuery(qry1);

        try{
            byte[] result = (byte[]) qry.list().get(0);
            
            if(result == null)
                return null;
            else
                return new String(result);
            
            }catch(Exception e){
                String result = (String) qry.list().get(0);
                
                if(result == null)
                    return null;
                else
                    return new String(result);
            }
    }
    
    
    /**
     * next number for grey lump piece received after finish received
     * 13-June-2016
     */
    
    @Override
    public <E> String getNextNumber(String colName, String yearFormat, String tableName, int length) {

        Session session = this.sessionFactory.getCurrentSession();
        String qry1 = "select concat(max(CONVERT(SUBSTRING(d."+colName+", "+length+"), SIGNED INTEGER)), '') from "+tableName+" as d where deleted = '0' and d.yearFormat = "+yearFormat;
        //System.out.println(qry1);
        Query qry = session.createSQLQuery(qry1);

        try{
            byte[] result = (byte[]) qry.list().get(0);
            
            if(result == null)
                return null;
            else
                return new String(result);
            
            }catch(Exception e){
                String result = (String) qry.list().get(0);
                
                if(result == null)
                    return null;
                else
                    return new String(result);
            }
    }
    
    
    
    
    /**
     * get previous entry -- yarn factory
     */
    @SuppressWarnings("unchecked")
    @Override
    public <E> E getPrevEntry(E type, String docNo){
        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass());
        cr.add(Restrictions.like("docNo", docNo, MatchMode.END)).uniqueResult();
        
        if(cr.list().size() > 0)
            return (E) cr.list().get(0);
        else
            return null;
    }
    
    
    /**
     * Server side pagination
     */
    @Override
    public Long recordsTotal(String entity) {
        /*
         * Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(
         * PurchaseOrderEntity.class); CriteriaQuery<PurchaseOrderEntity> cq1 =
         * cr.createQuery(PurchaseOrderEntity.class);
         */

        Session session = this.sessionFactory.getCurrentSession();
        
        if(entity.equals("ProgramCardEntity")){
            Query qry = session.createQuery("select count(id) from "+entity+" where deletionStatus is null");
            return (Long) qry.list().get(0);
        }else{
            Query qry = session.createQuery("select count(id) from "+entity);
            return (Long) qry.list().get(0);
        }
        
        

        
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public <E> Integer getFilteredRecords(E type, String qry1, int length, int start, String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition) throws HibernateException, JSONException {
        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass() , entityAlias);
        //Criteria crCount = this.sessionFactory.getCurrentSession().createCriteria(type.getClass(), entityAlias);
        
        for(int i=0; i<entitySubAlias.length(); i++){
            
            if(!entitySubAlias.getString(i).contains("."))
                cr.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i), CriteriaSpecification.LEFT_JOIN);
            else
                cr.createAlias(entitySubAlias.getString(i), entitySubAlias.getString(i).split("\\.")[1], CriteriaSpecification.LEFT_JOIN);
            //crCount.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i));
        }
        
        //.createAlias("indent.merchant", "merchant").createAlias("indent.product", "product").createAlias("indent.currency", "currency");

        if (!qry1.isEmpty()) {
            
            String[] queryArr;
            
            if(qry1.contains(" ")){
                queryArr = qry1.split(" ");
            }else{
                queryArr = new String[1];
                queryArr[0] = qry1;
            }
            
            for(int j=0; j<queryArr.length; j++)
            {
                
                Disjunction res = Restrictions.disjunction();
                String qry = queryArr[j];
                
                for(int i=0; i<likeCondition.length(); i++){
                    if(!likeCondition.getString(i).contains("-"))
                      res.add(Restrictions.like(likeCondition.getString(i), qry, MatchMode.ANYWHERE));
                    else{
                      
                      String[] dataType = likeCondition.getString(i).split("-");
                      
                          if(dataType[1].equals("c"))
                             res.add(Restrictions.eq(dataType[0], qry.charAt(0)));
                          else if(dataType[1].equals("f") && isNumeric(qry))
                              res.add(Restrictions.eq(dataType[0], Float.parseFloat(qry)));
                          else if(dataType[1].equals("i") && isNumeric(qry))
                              res.add(Restrictions.eq(dataType[0], Integer.parseInt(qry)));
                          else if(dataType[1].equals("d") && isNumeric(qry))
                              res.add(Restrictions.eq(dataType[0], Double.parseDouble(qry)));
                          else if(dataType[1].equals("l") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Long.parseLong(qry)));
                          }
                          else if(dataType[1].equals("dd")){
                              DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                              
                              Date startDate = null;
                              try {
                                  startDate = sdf.parse(qry);
                              } catch (ParseException e) {
                                  // TODO Auto-generated catch block
                                  //e.printStackTrace();
                              }
                              
                              if(startDate != null)
                                  res.add(Restrictions.eq(dataType[0], startDate));
                          }
                    }
                }// like condition for loop.
                
                cr.add(res);
                
            }// query space loop.
            
            
            //crCount.add(res);
            //cr.add(Restrictions.disjunction().add(Restrictions.like("indent.indent_number", "%" + qry + "%")).add(Restrictions.like("merchant.name", "%" + qry + "%")).add(Restrictions.like("product.name", "%" + qry + "%")).add(Restrictions.like("currency.name", "%" + qry + "%")));

        }
        
        //crCount.setProjection(Projections.rowCount());

       
        cr.setProjection(Projections.rowCount());
        //Integer count = ((Long) crCount.uniqueResult()).intValue();

        System.out.println(" --------------- > "+type.getClass().getSimpleName());
        
    
        if(type.getClass().getSimpleName().equals("ProgramCardEntity")){
            
            cr.add(Restrictions.isNull("deletionStatus"));
            
        }
        
        
        return ((Long) cr.uniqueResult()).intValue();
    }

    @SuppressWarnings({ "unchecked", "deprecation" })
    @Override
    public <E> List<E> filterResult(E type, String qry1, int length, int start, String sortCol, String sortDir,String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition) throws HibernateException, JSONException {

        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass() , entityAlias);
        //cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        //Criteria crCount = this.sessionFactory.getCurrentSession().createCriteria(type.getClass(), entityAlias);
        
        for(int i=0; i<entitySubAlias.length(); i++){
            //cr.setFetchMode(entityAlias+"."+entitySubAlias.getString(i), FetchMode.JOIN);
            
            if(!entitySubAlias.getString(i).contains("."))
                cr.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i), CriteriaSpecification.LEFT_JOIN);
            else
                cr.createAlias(entitySubAlias.getString(i), entitySubAlias.getString(i).split("\\.")[1], CriteriaSpecification.LEFT_JOIN);
            //crCount.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i));
        }
        
        //.createAlias("indent.merchant", "merchant").createAlias("indent.product", "product").createAlias("indent.currency", "currency");

        if (!qry1.isEmpty()) {
            
            
            
            String[] queryArr;
            
            if(qry1.contains(" ")){
                queryArr = qry1.split(" ");
            }else{
                queryArr = new String[1];
                queryArr[0] = qry1;
            }
            
            for(int j=0; j<queryArr.length; j++)
            {                
                    Disjunction res = Restrictions.disjunction();
                    String qry = queryArr[j];
        
                    for(int i=0; i<likeCondition.length(); i++){
                          if(!likeCondition.getString(i).contains("-"))
                              res.add(Restrictions.like(likeCondition.getString(i), qry, MatchMode.ANYWHERE));
                          else{
                          
                          String[] dataType = likeCondition.getString(i).split("-");
                          
                          if(dataType[1].equals("c"))
                              res.add(Restrictions.eq(dataType[0], qry.charAt(0)));
                          else if(dataType[1].equals("f") && isNumeric(qry)){
                             
                              res.add(Restrictions.eq(dataType[0], Float.parseFloat(qry)));
                          }
                          else if(dataType[1].equals("i") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Integer.parseInt(qry)));
                          }
                          else if(dataType[1].equals("d") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Double.parseDouble(qry)));
                          }
                          else if(dataType[1].equals("l") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Long.parseLong(qry)));
                          }
                          else if(dataType[1].equals("dd")){
                              DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                              
                              Date startDate = null;
                              try {
                                  startDate = sdf.parse(qry);
                              } catch (ParseException e) {
                                  // TODO Auto-generated catch block
                                  //e.printStackTrace();
                              }
                              
                              if(startDate != null)
                                  res.add(Restrictions.eq(dataType[0], startDate));
                          }
                      }
                        
                    }// like condition loop
                    
                        cr.add(res);
            }
            
            //crCount.add(res);
            //cr.add(Restrictions.disjunction().add(Restrictions.like("indent.indent_number", "%" + qry + "%")).add(Restrictions.like("merchant.name", "%" + qry + "%")).add(Restrictions.like("product.name", "%" + qry + "%")).add(Restrictions.like("currency.name", "%" + qry + "%")));

        }
        
        //crCount.setProjection(Projections.rowCount());

        if(!StringUtils.isNullOrEmpty(sortCol)) {
	        if (sortDir.equals("desc")) {
	            cr.addOrder(Order.desc(sortCol));
	        } else {
	            cr.addOrder(Order.asc(sortCol));
	        }
	        
	        if(type.getClass().getSimpleName().equals("ProgramCardEntity")){
	            
	            cr.add(Restrictions.isNull("deletionStatus"));
	            
	        }
        }

        //Integer count = ((Long) crCount.uniqueResult()).intValue();
        //Integer count1 = cr.list().size();
        
        cr.setFirstResult(start);
        cr.setMaxResults(length);
        
        //added by naveen 28-june-2016
        cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        
        List<E> op = cr.list();
        /*IndentEntity ie = new IndentEntity();
        ie.setId(count);
        op.add(ie);*/
        
        return op;
    }
    
    
    @SuppressWarnings({ "unchecked", "deprecation" })
    //@Override
    public <E> List<E> filterResult(E type, String qry1, int length, int start, String sortCol, String sortDir,
    		String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition, JSONObject filters) throws HibernateException, JSONException {

        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass() , entityAlias);
        //cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        //Criteria crCount = this.sessionFactory.getCurrentSession().createCriteria(type.getClass(), entityAlias);
        
        for(int i=0; i<entitySubAlias.length(); i++){
            //cr.setFetchMode(entityAlias+"."+entitySubAlias.getString(i), FetchMode.JOIN);
            
            if(!entitySubAlias.getString(i).contains("."))
                cr.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i), CriteriaSpecification.LEFT_JOIN);
            else
                cr.createAlias(entitySubAlias.getString(i), entitySubAlias.getString(i).split("\\.")[1], CriteriaSpecification.LEFT_JOIN);
            //crCount.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i));
        }
        
        //.createAlias("indent.merchant", "merchant").createAlias("indent.product", "product").createAlias("indent.currency", "currency");

        if (!qry1.isEmpty()) {
            
            
            
            String[] queryArr;
            
            if(qry1.contains(" ")){
                queryArr = qry1.split(" ");
            }else{
                queryArr = new String[1];
                queryArr[0] = qry1;
            }
            
            for(int j=0; j<queryArr.length; j++)
            {                
                    Disjunction res = Restrictions.disjunction();
                    String qry = queryArr[j];
        
                    for(int i=0; i<likeCondition.length(); i++){
                          if(!likeCondition.getString(i).contains("-"))
                              res.add(Restrictions.like(likeCondition.getString(i), qry, MatchMode.ANYWHERE));
                          else{
                          
                          String[] dataType = likeCondition.getString(i).split("-");
                          
                          if(dataType[1].equals("c"))
                              res.add(Restrictions.eq(dataType[0], qry.charAt(0)));
                          else if(dataType[1].equals("f") && isNumeric(qry)){
                             
                              res.add(Restrictions.eq(dataType[0], Float.parseFloat(qry)));
                          }
                          else if(dataType[1].equals("i") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Integer.parseInt(qry)));
                          }
                          else if(dataType[1].equals("d") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Double.parseDouble(qry)));
                          }
                          else if(dataType[1].equals("l") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Long.parseLong(qry)));
                          }
                          else if(dataType[1].equals("dd")){
                              DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                              
                              Date startDate = null;
                              try {
                                  startDate = sdf.parse(qry);
                              } catch (ParseException e) {
                                  // TODO Auto-generated catch block
                                  //e.printStackTrace();
                              }
                              
                              if(startDate != null)
                                  res.add(Restrictions.eq(dataType[0], startDate));
                          }
                      }
                        
                    }// like condition loop
                    
                        cr.add(res);
            }
            
            //crCount.add(res);
            //cr.add(Restrictions.disjunction().add(Restrictions.like("indent.indent_number", "%" + qry + "%")).add(Restrictions.like("merchant.name", "%" + qry + "%")).add(Restrictions.like("product.name", "%" + qry + "%")).add(Restrictions.like("currency.name", "%" + qry + "%")));

        }
        
        
        if(filters!=null && filters.length() > 0) {
        	Iterator<String> keys = filters.keys();
        	
        	while(keys.hasNext()) {
        	    String key = keys.next();
        	    if (filters.get(key) instanceof JSONObject) {
        	          // do something with jsonObject here      
        	    }
        	}
        	
        }
        
        
        //crCount.setProjection(Projections.rowCount());

        if(!StringUtils.isNullOrEmpty(sortCol)) {
	        if (sortDir.equals("desc")) {
	            cr.addOrder(Order.desc(sortCol));
	        } else {
	            cr.addOrder(Order.asc(sortCol));
	        }
	        
	        if(type.getClass().getSimpleName().equals("ProgramCardEntity")){
	            
	            cr.add(Restrictions.isNull("deletionStatus"));
	            
	        }
        }

        //Integer count = ((Long) crCount.uniqueResult()).intValue();
        //Integer count1 = cr.list().size();
        
        cr.setFirstResult(start);
        cr.setMaxResults(length);
        
        //added by naveen 28-june-2016
        cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        
        List<E> op = cr.list();
        /*IndentEntity ie = new IndentEntity();
        ie.setId(count);
        op.add(ie);*/
        
        return op;
    }
    
    
    
    
    
    
    /**
     * server side pagination by type.
     */
    
    @Override
    public Long recordsTotal(String entity, String type, String value) {
        /*
         * Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(
         * PurchaseOrderEntity.class); CriteriaQuery<PurchaseOrderEntity> cq1 =
         * cr.createQuery(PurchaseOrderEntity.class);
         */

        Session session = this.sessionFactory.getCurrentSession();
        Query qry = session.createQuery("select count(id) from "+entity+" where "+type+" = '"+value+"'");

        return (Long) qry.list().get(0);
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public <E> Integer getFilteredRecords(E type, String qry1, int length, int start, String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition, String colName, String value) throws HibernateException, JSONException {
        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass() , entityAlias);
        //Criteria crCount = this.sessionFactory.getCurrentSession().createCriteria(type.getClass(), entityAlias);
        
        for(int i=0; i<entitySubAlias.length(); i++){
            
            if(!entitySubAlias.getString(i).contains("."))
                cr.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i), CriteriaSpecification.LEFT_JOIN);
            else
                cr.createAlias(entitySubAlias.getString(i), entitySubAlias.getString(i).split("\\.")[1], CriteriaSpecification.LEFT_JOIN);
            //crCount.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i));
        }
        
        /**
         * add restriction for type
         */
        cr.add(Restrictions.eq(colName, value.charAt(0)));
        
        //.createAlias("indent.merchant", "merchant").createAlias("indent.product", "product").createAlias("indent.currency", "currency");

        if (!qry1.isEmpty()) {
            
            String[] queryArr;
            
            if(qry1.contains(" ")){
                queryArr = qry1.split(" ");
            }else{
                queryArr = new String[1];
                queryArr[0] = qry1;
            }
            
            for(int j=0; j<queryArr.length; j++)
            {
                
                Disjunction res = Restrictions.disjunction();
                String qry = queryArr[j];
                
                for(int i=0; i<likeCondition.length(); i++){
                    if(!likeCondition.getString(i).contains("-"))
                      res.add(Restrictions.like(likeCondition.getString(i), qry, MatchMode.ANYWHERE));
                    else{
                      
                      String[] dataType = likeCondition.getString(i).split("-");
                      
                          if(dataType[1].equals("c"))
                             res.add(Restrictions.eq(dataType[0], qry.charAt(0)));
                          else if(dataType[1].equals("f") && isNumeric(qry))
                              res.add(Restrictions.eq(dataType[0], Float.parseFloat(qry)));
                          else if(dataType[1].equals("i") && isNumeric(qry))
                              res.add(Restrictions.eq(dataType[0], Integer.parseInt(qry)));
                          else if(dataType[1].equals("d") && isNumeric(qry))
                              res.add(Restrictions.eq(dataType[0], Double.parseDouble(qry)));
                          else if(dataType[1].equals("l") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Long.parseLong(qry)));
                          }
                          else if(dataType[1].equals("dd")){
                              DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                              
                              Date startDate = null;
                              try {
                                  startDate = sdf.parse(qry);
                              } catch (ParseException e) {
                                  // TODO Auto-generated catch block
                                  //e.printStackTrace();
                              }
                              
                              if(startDate != null)
                                  res.add(Restrictions.eq(dataType[0], startDate));
                          }
                    }
                }// like condition for loop.
                
                cr.add(res);
                
            }// query space loop.
            
            
            //crCount.add(res);
            //cr.add(Restrictions.disjunction().add(Restrictions.like("indent.indent_number", "%" + qry + "%")).add(Restrictions.like("merchant.name", "%" + qry + "%")).add(Restrictions.like("product.name", "%" + qry + "%")).add(Restrictions.like("currency.name", "%" + qry + "%")));

        }
        
        //crCount.setProjection(Projections.rowCount());

       
        cr.setProjection(Projections.rowCount());
        //Integer count = ((Long) crCount.uniqueResult()).intValue();

        return ((Long) cr.uniqueResult()).intValue();
    }

    @SuppressWarnings({ "unchecked", "deprecation" })
    @Override
    public <E> List<E> filterResult(E type, String qry1, int length, int start, String sortCol, String sortDir,String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition, String colName, String value) throws HibernateException, JSONException {

        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass() , entityAlias);
        //cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        //Criteria crCount = this.sessionFactory.getCurrentSession().createCriteria(type.getClass(), entityAlias);
        
        /**
         * add restriction for type
         */
        cr.add(Restrictions.eq(colName, value.charAt(0)));
        
        for(int i=0; i<entitySubAlias.length(); i++){
            //cr.setFetchMode(entityAlias+"."+entitySubAlias.getString(i), FetchMode.JOIN);
            
            if(!entitySubAlias.getString(i).contains("."))
                cr.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i), CriteriaSpecification.LEFT_JOIN);
            else
                cr.createAlias(entitySubAlias.getString(i), entitySubAlias.getString(i).split("\\.")[1], CriteriaSpecification.LEFT_JOIN);
            //crCount.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i));
        }
        
        //.createAlias("indent.merchant", "merchant").createAlias("indent.product", "product").createAlias("indent.currency", "currency");

        if (!qry1.isEmpty()) {
            
            
            
            String[] queryArr;
            
            if(qry1.contains(" ")){
                queryArr = qry1.split(" ");
            }else{
                queryArr = new String[1];
                queryArr[0] = qry1;
            }
            
            for(int j=0; j<queryArr.length; j++)
            {                
                    Disjunction res = Restrictions.disjunction();
                    String qry = queryArr[j];
        
                    for(int i=0; i<likeCondition.length(); i++){
                          if(!likeCondition.getString(i).contains("-"))
                              res.add(Restrictions.like(likeCondition.getString(i), qry, MatchMode.ANYWHERE));
                          else{
                          
                          String[] dataType = likeCondition.getString(i).split("-");
                          
                          if(dataType[1].equals("c"))
                              res.add(Restrictions.eq(dataType[0], qry.charAt(0)));
                          else if(dataType[1].equals("f") && isNumeric(qry)){
                             
                              res.add(Restrictions.eq(dataType[0], Float.parseFloat(qry)));
                          }
                          else if(dataType[1].equals("i") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Integer.parseInt(qry)));
                          }
                          else if(dataType[1].equals("d") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Double.parseDouble(qry)));
                          }
                          else if(dataType[1].equals("l") && isNumeric(qry)){
                              res.add(Restrictions.eq(dataType[0], Long.parseLong(qry)));
                          }
                          else if(dataType[1].equals("dd")){
                              DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                              
                              Date startDate = null;
                              try {
                                  startDate = sdf.parse(qry);
                              } catch (ParseException e) {
                                  // TODO Auto-generated catch block
                                  //e.printStackTrace();
                              }
                              
                              if(startDate != null)
                                  res.add(Restrictions.eq(dataType[0], startDate));
                          }
                      }
                        
                    }// like condition loop
                    
                        cr.add(res);
            }
            
            //crCount.add(res);
            //cr.add(Restrictions.disjunction().add(Restrictions.like("indent.indent_number", "%" + qry + "%")).add(Restrictions.like("merchant.name", "%" + qry + "%")).add(Restrictions.like("product.name", "%" + qry + "%")).add(Restrictions.like("currency.name", "%" + qry + "%")));

        }
        
        //crCount.setProjection(Projections.rowCount());

        if (sortDir.equals("desc")) {
            cr.addOrder(Order.desc(sortCol));
        } else {
            cr.addOrder(Order.asc(sortCol));
        }

        //Integer count = ((Long) crCount.uniqueResult()).intValue();
        //Integer count1 = cr.list().size();
        
        cr.setFirstResult(start);
        cr.setMaxResults(length);
        
        List<E> op = cr.list();
        /*IndentEntity ie = new IndentEntity();
        ie.setId(count);
        op.add(ie);*/
        
        return op;
    }
    
    
    @Override
    public <E> List<E> searchGeneric(E type) throws IllegalArgumentException, IllegalAccessException{
    	
    	Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass());
    	
    	Field[] fields = type.getClass().getDeclaredFields();
    	for(Field field: fields) {
    		field.setAccessible(true);
    		Class<?> fclass = field.getClass();
    		Object data = field.get(type);
    		
    		if(data != null) {
    			if(field.getType().isAssignableFrom(String.class)) {
    				String sdata = (String) data;
    				if(Helper.isNotEmpty(sdata))
    					cr.add(Restrictions.ilike(field.getName(), sdata, MatchMode.ANYWHERE));
    			}
    		}
    	}
    	
    	return cr.list();
    }
    
    
    @Override
    public <E> List<E> findByFields(final Class<E> type, List<QueryObject> query, String alias, List<String> subAliasList) throws ApplicationException{
        try{
            //Transaction tx = this.session.beginTransaction();
            
            Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type, alias);
            
           /* for(int i=0; i<entitySubAlias.length(); i++){
                
                if(!entitySubAlias.getString(i).contains("."))
                    cr.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i), CriteriaSpecification.LEFT_JOIN);
                else
                    cr.createAlias(entitySubAlias.getString(i), entitySubAlias.getString(i).split("\\.")[1], CriteriaSpecification.LEFT_JOIN);
                //crCount.createAlias(entityAlias+"."+entitySubAlias.getString(i), entitySubAlias.getString(i));
            }*/
            
            for(String subAlias: subAliasList) {
            	if(!subAlias.contains(".")) {
            		cr.createAlias(alias+"."+subAlias, subAlias, CriteriaSpecification.LEFT_JOIN);
            	}else {
            		cr.createAlias(subAlias, subAlias.split("\\.")[1], CriteriaSpecification.LEFT_JOIN);
            	}
            }
            /*fields.forEach((k,v)->{
                cr.add(Restrictions.eq(k, v));
            });*/
            
            if(query!=null){
                //query.forEach((obj) -> {
                for(QueryObject obj: query){
                    if("i".equals(obj.getDatatype())){
                        cr.add(Restrictions.eq(obj.getColName(), (Integer) obj.getColVal()));
                    }else if("l".equals(obj.getDatatype())){
                        //cr.add(Restrictions.eq(obj.getColName(), (Long) obj.getColVal()));
                        
                        if(Helper.isEmpty(obj.getQuerytype())){
                            cr.add(Restrictions.eq(obj.getColName(), (Long) obj.getColVal()));
                        }
                        else{
                            if("neq".equals(obj.getQuerytype())){
                                //cr.add(Restrictions.ne(obj.getColName(), (String) obj.getColVal()));
                                cr.add(Restrictions.ne(obj.getColName(), (Long) obj.getColVal()));
                            }else {
                                cr.add(Restrictions.eq(obj.getColName(), (Long) obj.getColVal()));
                            }
                        }
                        
                    }else if("c".equals(obj.getDatatype())){
                        cr.add(Restrictions.eq(obj.getColName(), (Character) obj.getColVal()));
                    }else if("s".equals(obj.getDatatype())){
                        
                        if(Helper.isEmpty(obj.getQuerytype())){
                            cr.add(Restrictions.eq(obj.getColName(), (String) obj.getColVal()));
                        }
                        else{
                            if("neq".equals(obj.getQuerytype())){
                                cr.add(Restrictions.ne(obj.getColName(), (String) obj.getColVal()));
                            }else if("like".equals(obj.getQuerytype())){
                                cr.add(Restrictions.ilike(obj.getColName(), (String) obj.getColVal(), MatchMode.ANYWHERE));
                            }else {
                                cr.add(Restrictions.eq(obj.getColName(), (String) obj.getColVal()));
                            }
                        }
                        
                    }
                }
            }
            
            cr.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            List<E> data = cr.list();
            
            //tx.commit();
            //this.session.close();
            
            return data;
        }catch(Exception e){
            throw new ApplicationException("Error searching for data.", e);
        }
    }
    
    
    /************************ Utility methods *****************************/
    
    public static boolean isNumeric(String s)
    {
        char[] input = s.toCharArray();
        
        for(char c : input){
            if(!Character.isDigit(c) && c != '.')
                return false;
            
        }
        return true;
    }
    
    
   
    
    @Override
    public <E> List<String> getOneColumn(E type, String colName){
        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass());
        cr.setProjection(Projections.property(colName));
        cr.addOrder(Order.asc(colName));
        cr.add(Restrictions.eq("status",1));
        List<String> op = cr.list();
        
        return op;
    }
    
    @Override
    public <E> List<String> getOneColumn(E type, String colName, Integer master_type){
        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass());
        cr.setProjection(Projections.property("name"));
        cr.add(Restrictions.eq("masterType", master_type));
        cr.add(Restrictions.like("name", colName, MatchMode.ANYWHERE));
        cr.add(Restrictions.eq("status",1));
        cr.addOrder(Order.asc("name"));
        
        List<String> op = cr.list();
        
        return op;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <E> List<String> getOneColumn(E type, String inp, String property) {
        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass());
        cr.setProjection(Projections.projectionList().add(Projections.property(property)));
        cr.add(Restrictions.like(property, inp, MatchMode.ANYWHERE));
        
                
        //Query qry = this.sessionFactory.getCurrentSession().createSQLQuery(query).setParameter("inp", "%" + inp.toUpperCase() + "%");
        return cr.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <E> List<Double> getOneColumn(E type, Double inp, String property) {
        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass());
        cr.setProjection(Projections.projectionList().add(Projections.property(property)));
        cr.add(Restrictions.eq(property, inp));
        
                
        //Query qry = this.sessionFactory.getCurrentSession().createSQLQuery(query).setParameter("inp", "%" + inp.toUpperCase() + "%");
        return cr.list();
    }
    
    
    @SuppressWarnings("unchecked")
    @Override
    public <E> List<Object[]> getDateOneRow(E type, Long id, List<String> fields) {
        Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(type.getClass());
        ProjectionList plist = Projections.projectionList();
        
        if(fields!=null && fields.size() > 0) {
        	fields.forEach(x -> {
        		plist.add(Projections.property(x));
        	});
        	cr.setProjection(plist);
        }
        
        cr.add(Restrictions.eq("id", id));
        //Query qry = this.sessionFactory.getCurrentSession().createSQLQuery(query).setParameter("inp", "%" + inp.toUpperCase() + "%");
        return cr.list();
    }
    
    
    @Override
    public void setBeamStatus(Integer jobCardId, String prop){
        Query qry = this.sessionFactory.getCurrentSession().createQuery("UPDATE JobCardEntity set "+prop +" = 'Y' where id = :id").setParameter("id", jobCardId);
        qry.executeUpdate();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <E> E getByName(E type, String name){
        Session session = this.sessionFactory.getCurrentSession();
        
        Criteria cr = session.createCriteria(type.getClass());
        
        cr.add(Restrictions.eq("name", name));
        
        cr.setMaxResults(1);
        try{
            return (E) cr.list().get(0);
        }catch(Exception e){
            return null;
        }
        
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <E> E getByField(E type, String field, String value){
        Session session = this.sessionFactory.getCurrentSession();
        
        Criteria cr = session.createCriteria(type.getClass());
        
        cr.add(Restrictions.eq(field, value));
        
        cr.setMaxResults(1);
        try{
            return (E) cr.list().get(0);
        }catch(Exception e){
            return null;
        }
        
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <E> E getByField(E type, String field, Integer value){
        Session session = this.sessionFactory.getCurrentSession();
        
        Criteria cr = session.createCriteria(type.getClass());
        
        cr.add(Restrictions.eq(field, value));
        
        cr.setMaxResults(1);
        
        List<E> result = cr.list();
        
        try{
            return (E) result.get(0);
        }catch(Exception e){
            return null;
        }
        
    }
    @SuppressWarnings("unchecked")
    public <E, T> boolean deleteva(E entity, String id, List<T> listToCheck,  List<String> aliasName) {

         
          /* List<T> parents = new ArrayList<T>();
            List<String> alias = new ArrayList<String>();
            
            
            parents.add((T) new CardEntity());
           alias.add("division");
            alias.add("priority");*/
        
            Session session = this.sessionFactory.getCurrentSession();
            
            boolean found = false;
            int i=0;
            for(T pentity : listToCheck ){
                
                Criteria cr = session.createCriteria(pentity.getClass(), "pentity");
                
                cr.createAlias("pentity."+aliasName.get(i++), "centity");
                
                cr.add(Restrictions.eq("centity.id", Integer.parseInt(id)));
                
                cr.setProjection(Projections.rowCount());
                
                int result = ((Long) cr.uniqueResult()).intValue();
                
                System.out.println("result" + result);
                
                if(result > 0){
                    found = true;
                    break;
                }
                
            }
            
            if(found){
                
            }else{
                session.createQuery("delete from "+entity+" where id="+Integer.parseInt(id)+"").executeUpdate();
            }
                    
        
        return found;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E> E findLongOne(E e, Long id) {
        
         E entity = (E) this.sessionFactory.getCurrentSession().get(e.getClass(), id);
         
         return entity;
         
    }
    
    
    /********************** Custom Method *************************/
    @Override
    public ShadeEntity getShade(Long id) {
    	ShadeEntity s = (ShadeEntity) this.sessionFactory.getCurrentSession().get(ShadeEntity.class, id);
    	Hibernate.initialize(s.getDetails());
    	return s;
    }
}
    
