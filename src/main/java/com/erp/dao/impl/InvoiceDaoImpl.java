package com.erp.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.dao.InvoiceDao;
import com.erp.entity.InvoiceDetailEntity;
import com.erp.entity.InvoiceEntity;

@Repository("invoiceDao")
public class InvoiceDaoImpl implements InvoiceDao{

	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public InvoiceEntity get(Long id) {
		InvoiceEntity entity = (InvoiceEntity) this.sessionFactory.getCurrentSession().get(InvoiceEntity.class, id);
		Hibernate.initialize(entity.getDetails());
		return entity;
	}

	@Override
	public List<InvoiceDetailEntity> getDetails(Long dpId) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
