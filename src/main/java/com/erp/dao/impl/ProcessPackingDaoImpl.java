package com.erp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.dao.ProcessPackingDao;
import com.erp.entity.DyeingProgmDetailEntityV2;
import com.erp.entity.ProcessPackingEntity;
import com.erp.entity.ProcessPackingEntityV2;
import com.erp.helper.Helper;

@Repository("ppDao")
public class ProcessPackingDaoImpl implements ProcessPackingDao{

	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
	public ProcessPackingEntityV2 get(Long id) {
		ProcessPackingEntityV2 entity = (ProcessPackingEntityV2) this.sessionFactory.getCurrentSession().get(ProcessPackingEntityV2.class, id);
		Hibernate.initialize(entity.getDetails());
		if(entity.getDetails().size() > 0) {
			entity.getDetails().forEach(x -> {
				Hibernate.initialize(x.getDetail().getShade().getDetails());
			});
		}
		return entity;
	}
	
	@Override
	public List<Object[]> searchDyeingDetails(String qry) {
		
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(DyeingProgmDetailEntityV2.class, "detail");
		cr.createAlias("detail.lot", "lot")
		.createAlias("detail.program", "program")
		.createAlias("detail.shade", "shade", CriteriaSpecification.LEFT_JOIN)
		.createAlias("lot.grey", "grey")
		.createAlias("grey.party", "party")
		.createAlias("grey.quality", "quality", CriteriaSpecification.LEFT_JOIN);
		
		cr.setProjection(Projections.projectionList()
				.add(Projections.property("detail.id"), "id")
				.add(Projections.property("party.name"), "name")
				.add(Projections.property("grey.lotNo"), "lotNo")
				.add(Projections.property("detail.addedThan"), "addedThan")
				.add(Projections.property("shade.name"), "name")
				.add(Projections.property("program.serialNo"), "serialNo")
				.add(Projections.property("program.programDate"), "programDate")
				.add(Projections.property("grey.biltyNo"), "biltyNo")
				.add(Projections.property("quality.name"), "name"));
		
		if(Helper.isNotEmpty(qry)) {
			Disjunction or = Restrictions.disjunction();
			or.add(Restrictions.ilike("grey.lotNo", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("party.name", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("shade.name", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("program.serialNo", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("grey.serialNo", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("grey.biltyNo", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("quality.name", qry, MatchMode.ANYWHERE));
			
			if(Helper.isInteger(qry)) {
				or.add(Restrictions.eq("detail.addedThan", Integer.parseInt(qry)));
			}
			cr.add(or);
		}
		
		cr.add(Restrictions.eq("detail.packingDone", false));
		
		//cr.setResultTransformer(Transformers.aliasToBean(DyeingProgmDetailEntity.class));
		
		return cr.list();
	}
	
	@Override
	public List<Object[]> searchProcessPacking(String qry, Long partyId){
		
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(ProcessPackingEntity.class, "pp");
		cr.createAlias("pp.detail", "detail").createAlias("detail.lot", "lot").createAlias("detail.program", "program")
		.createAlias("lot.grey", "grey").createAlias("grey.party", "party")
		.createAlias("detail.shade", "shade", CriteriaSpecification.LEFT_JOIN)
		.createAlias("grey.quality", "quality", CriteriaSpecification.LEFT_JOIN);
		
		cr.setProjection(Projections.projectionList()
				.add(Projections.property("pp.id"), "ppid")
				.add(Projections.property("pp.serialNo"), "serialNo")
				.add(Projections.property("grey.lotNo"), "lotNo")
				.add(Projections.property("pp.lrNo"), "lrNo")
				.add(Projections.property("detail.addedThan"), "addedThan")
				.add(Projections.property("pp.length"), "length")
				.add(Projections.property("pp.deliveryDate"), "deliveryDate")
				
				.add(Projections.property("pp.baleNo"), "baleNo")
				.add(Projections.property("quality.name"), "quality")
		);
		
		if(Helper.isNotEmpty(qry)) {
			Disjunction or = Restrictions.disjunction();
			
			or.add(Restrictions.ilike("grey.lotNo", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("party.name", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("shade.name", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("program.serialNo", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("grey.serialNo", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("grey.biltyNo", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("quality.name", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("pp.serialNo", qry, MatchMode.ANYWHERE))
			.add(Restrictions.ilike("pp.lrNo", qry, MatchMode.ANYWHERE));
			
			if(Helper.isInteger(qry)) {
				or.add(Restrictions.eq("detail.addedThan", Integer.parseInt(qry)));
				or.add(Restrictions.eq("pp.baleNo", Long.parseLong(qry)));
			}
			
			cr.add(or);
		}
		
		cr.add(Restrictions.eq("party.id", partyId));
		cr.add(Restrictions.eq("pp.invoiceDone", false));
		return cr.list();
	}
	
	@Override
	public Long getNextBaleNo() {
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(ProcessPackingEntityV2.class, "pp");
		cr.setProjection(Projections.projectionList().add(Projections.max("pp.baleNo")));
		
		Long baleNo = (Long) cr.uniqueResult();
		if(baleNo==null) {
			return 1L;
		}
		return baleNo;
	}
}
