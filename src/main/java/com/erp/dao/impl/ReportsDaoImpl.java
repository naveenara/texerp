package com.erp.dao.impl;

import java.text.ParseException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.dao.ReportsDao;
import com.erp.entity.DyeingProgmDetailEntity;
import com.erp.entity.GreyInwardEntity;
import com.erp.entity.LotEntryEntity;
import com.erp.helper.Helper;
import com.erp.rest.model.FabricStockReportModel;
import com.erp.rest.model.GreyInwardReportModel;
import com.erp.rest.model.LotEntryModel;
import com.erp.rest.model.LotEntryReportModel;
import com.erp.rest.model.report.PartyLotWiseModel;

@Repository("reportsDao")
public class ReportsDaoImpl implements ReportsDao{

	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
	public List<GreyInwardEntity> searchGreyInward(GreyInwardReportModel report) throws ParseException{
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(GreyInwardEntity.class, "gi");
		
		if(report.getCriteria().equals("and")) {
			
			Conjunction and = Restrictions.conjunction();
			
			if(report.getBalesFrom() != null && report.getBalesTo() != null) {
				cr.add(Restrictions.ge("gi.bales", report.getBalesFrom()));
				cr.add(Restrictions.le("gi.bales", report.getBalesTo()));
			}else {
				if(report.getBalesFrom() != null) {
					and.add(Restrictions.ge("gi.bales", report.getBalesFrom()));
				}if(report.getBalesTo() != null) {
					and.add(Restrictions.le("gi.bales", report.getBalesTo()));
				}
			}
			
			if(Helper.isNotEmpty(report.getBiltyNo())) {
				and.add(Restrictions.ilike("biltyNo", report.getBiltyNo(), MatchMode.ANYWHERE));
			}
			if(Helper.isNotEmpty(report.getLotNo())) {
				and.add(Restrictions.ilike("lotNo", report.getLotNo(), MatchMode.ANYWHERE));
			}
			
			
			if(Helper.isNotEmpty(report.getParty())) {
				cr.createAlias("gi.party", "party");
				and.add(Restrictions.ilike("party.name", report.getParty(), MatchMode.ANYWHERE));
			}if(Helper.isNotEmpty(report.getSerialNo())) {
				and.add(Restrictions.ilike("serialNo", report.getSerialNo(), MatchMode.ANYWHERE));
			}
			
			
			if(report.getLengthFrom() != null && report.getLengthTo() != null) {
				cr.add(Restrictions.ge("gi.length", report.getLengthFrom()));
				cr.add(Restrictions.le("gi.length", report.getLengthTo()));
			}else {
				if(report.getLengthFrom() != null) {
					and.add(Restrictions.ge("gi.length", report.getLengthFrom()));
				}
				if(report.getLengthTo() != null) {
					and.add(Restrictions.le("gi.length", report.getLengthTo()));
				}
			}
			
			
			if(report.getQuality() != null) {
				cr.createAlias("gi.quality", "quality");
				and.add(Restrictions.eq("quality.id", report.getQuality().getId()));
			}
			if(report.getThan() != null) {
				and.add(Restrictions.eq("gi.than", report.getThan()));
			}
			if(report.getTp() != null) {
				and.add(Restrictions.eq("gi.tp", report.getTp()));
			}
			if(report.getTransport() != null) {
				cr.createAlias("gi.transport", "transport");
				and.add(Restrictions.eq("transport.id", report.getTransport().getId()));
			}
			if(report.getVoucherNo() != null) {
				and.add(Restrictions.eq("gi.voucherNo", report.getVoucherNo()));
			}
			/*if(Helper.isNotEmpty(report.getCreated_from())) {
				or.add(Restrictions.ge("gi.created_at", sdf.parse(report.getCreated_from())));
			}if(Helper.isNotEmpty(report.getCreated_to())) {
				or.add(Restrictions.le("gi.created_at", sdf.parse(report.getCreated_to())));
			}*/
			
			if(report.getCreated_from() != null && report.getCreated_to() != null) {
				cr.add(Restrictions.ge("gi.created_at", report.getCreated_from()));
				cr.add(Restrictions.le("gi.created_at", report.getCreated_to()));
			}else {
				if(report.getCreated_from() != null) {
					and.add(Restrictions.ge("gi.created_at", report.getCreated_from()));
				}if(report.getCreated_to() != null) {
					and.add(Restrictions.le("gi.created_at", report.getCreated_to()));
				}
			}
			
			
			if(Helper.isNotEmpty(report.getBiltyNo())) {
				and.add(Restrictions.ilike("biltyNo", report.getBiltyNo(), MatchMode.ANYWHERE));
			}
			
			if(report.getLotDone() != null) {
				and.add(Restrictions.eq("lotDone", report.getLotDone()));
			}
			
			cr.add(and);
			
		}else {
			
			Disjunction or = Restrictions.disjunction();
			
			if(report.getBalesFrom() != null && report.getBalesTo() != null) {
				cr.add(Restrictions.ge("gi.bales", report.getBalesFrom()));
				cr.add(Restrictions.le("gi.bales", report.getBalesTo()));
			}else {
				if(report.getBalesFrom() != null) {
					or.add(Restrictions.ge("gi.bales", report.getBalesFrom()));
				}if(report.getBalesTo() != null) {
					or.add(Restrictions.le("gi.bales", report.getBalesTo()));
				}
			}
			
			if(Helper.isNotEmpty(report.getBiltyNo())) {
				or.add(Restrictions.ilike("biltyNo", report.getBiltyNo(), MatchMode.ANYWHERE));
			}
			if(Helper.isNotEmpty(report.getLotNo())) {
				or.add(Restrictions.ilike("lotNo", report.getLotNo(), MatchMode.ANYWHERE));
			}
			
			
			if(Helper.isNotEmpty(report.getParty())) {
				cr.createAlias("gi.party", "party");
				or.add(Restrictions.ilike("party.name", report.getParty(), MatchMode.ANYWHERE));
			}if(Helper.isNotEmpty(report.getSerialNo())) {
				or.add(Restrictions.ilike("serialNo", report.getSerialNo(), MatchMode.ANYWHERE));
			}
			
			
			if(report.getLengthFrom() != null && report.getLengthTo() != null) {
				cr.add(Restrictions.ge("gi.length", report.getLengthFrom()));
				cr.add(Restrictions.le("gi.length", report.getLengthTo()));
			}else {
				if(report.getLengthFrom() != null) {
					or.add(Restrictions.ge("gi.length", report.getLengthFrom()));
				}
				if(report.getLengthTo() != null) {
					or.add(Restrictions.le("gi.length", report.getLengthTo()));
				}
			}
			
			
			if(report.getQuality() != null) {
				cr.createAlias("gi.quality", "quality");
				or.add(Restrictions.eq("quality.id", report.getQuality().getId()));
			}
			if(report.getThan() != null) {
				or.add(Restrictions.eq("gi.than", report.getThan()));
			}
			if(report.getTp() != null) {
				or.add(Restrictions.eq("gi.tp", report.getTp()));
			}
			if(report.getTransport() != null) {
				cr.createAlias("gi.transport", "transport");
				or.add(Restrictions.eq("transport.id", report.getTransport().getId()));
			}
			if(report.getVoucherNo() != null) {
				or.add(Restrictions.eq("gi.voucherNo", report.getVoucherNo()));
			}
			/*if(Helper.isNotEmpty(report.getCreated_from())) {
				or.add(Restrictions.ge("gi.created_at", sdf.parse(report.getCreated_from())));
			}if(Helper.isNotEmpty(report.getCreated_to())) {
				or.add(Restrictions.le("gi.created_at", sdf.parse(report.getCreated_to())));
			}*/
			
			if(report.getCreated_from() != null && report.getCreated_to() != null) {
				cr.add(Restrictions.ge("gi.created_at", report.getCreated_from()));
				cr.add(Restrictions.le("gi.created_at", report.getCreated_to()));
			}else {
				if(report.getCreated_from() != null) {
					or.add(Restrictions.ge("gi.created_at", report.getCreated_from()));
				}if(report.getCreated_to() != null) {
					or.add(Restrictions.le("gi.created_at", report.getCreated_to()));
				}
			}
			
			
			if(Helper.isNotEmpty(report.getBiltyNo())) {
				or.add(Restrictions.ilike("biltyNo", report.getBiltyNo(), MatchMode.ANYWHERE));
			}
			
			if(report.getLotDone() != null) {
				or.add(Restrictions.eq("lotDone", report.getLotDone()));
			}
			
			cr.add(or);
		}
		
		
		cr.addOrder(Order.desc("gi.created_at"));
		cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return cr.list();
	}
	
	
	@Override
	public List<DyeingProgmDetailEntity> searchDyeingDetailsReport(FabricStockReportModel report) throws ParseException{
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(DyeingProgmDetailEntity.class, "dyeing");
		cr.createCriteria("dyeing.lot", "lot")
		.createCriteria("dyeing.program", "program")
		.createCriteria("dyeing.shade", "shade")
		.createCriteria("lot.grey", "grey")
		.createCriteria("program.party", "party")
		.createCriteria("grey.quality", "quality");
		
		if(report.getCriteria().equals("and")) {
			
			Conjunction and = Restrictions.conjunction();
			
			if(report.getPackingDone()!=null) {
				if(report.getPackingDone()) {
					and.add(Restrictions.eq("dyeing.packingDone", true));
				}else {
					and.add(Restrictions.eq("dyeing.packingDone", false));
				}
			}
			if(Helper.isNotEmpty(report.getBiltyNo())) {
				and.add(Restrictions.ilike("grey.biltyNo", report.getBiltyNo(), MatchMode.ANYWHERE));
			}
			if(Helper.isNotEmpty(report.getDyeingSerialNo())) {
				and.add(Restrictions.ilike("program.serialNo", report.getDyeingSerialNo(), MatchMode.ANYWHERE));
			}if(Helper.isNotEmpty(report.getLotNo())) {
				and.add(Restrictions.ilike("grey.lotNo", report.getLotNo(), MatchMode.ANYWHERE));
			}if(Helper.isNotEmpty(report.getLotSerialNo())) {
				and.add(Restrictions.ilike("lot.serialNo", report.getLotSerialNo(), MatchMode.ANYWHERE));
			}if(!Helper.isNull(report.getParty()) && !Helper.isNull(report.getParty().getId())) {
				and.add(Restrictions.eq("party.id", report.getParty().getId()));
			}if(!Helper.isNull(report.getQuality()) && !Helper.isNull(report.getQuality().getId())) {
				and.add(Restrictions.eq("quality.id", report.getQuality().getId()));
			}if(!Helper.isNull(report.getShade()) && !Helper.isNull(report.getShade().getId())) {
				and.add(Restrictions.eq("shade.id", report.getShade().getId()));
			}
			
			if(report.getLotReceivedDateFrom() != null && report.getLotReceivedDateTo() != null) {
				cr.add(Restrictions.ge("lot.receivedDate", report.getLotReceivedDateFrom()));
				cr.add(Restrictions.le("lot.receivedDate", report.getLotReceivedDateTo()));
			}else {
				if(report.getLotReceivedDateFrom() != null) {
					and.add(Restrictions.ge("lot.receivedDate", report.getLotReceivedDateFrom()));
				}if(report.getLotReceivedDateTo() != null) {
					and.add(Restrictions.le("lot.receivedDate", report.getLotReceivedDateTo()));
				}
			}
			if(report.getProgramDateFrom() != null && report.getProgramDateTo() != null) {
				cr.add(Restrictions.ge("program.programDate", report.getProgramDateFrom()));
				cr.add(Restrictions.le("program.programDate", report.getProgramDateTo()));
			}else {
				if(report.getProgramDateFrom() != null) {
					and.add(Restrictions.ge("program.programDate", report.getProgramDateFrom()));
				}if(report.getProgramDateTo() != null) {
					and.add(Restrictions.le("program.programDate", report.getProgramDateTo()));
				}
			}
			
			if(report.getThanFrom() != null && report.getThanTo() != null) {
				cr.add(Restrictions.ge("dyeing.addedThan", report.getThanFrom()));
				cr.add(Restrictions.le("dyeing.addedThan", report.getThanTo()));
			}else {
				if(report.getThanFrom() != null) {
					cr.add(Restrictions.ge("dyeing.addedThan", report.getThanFrom()));
				}
				if(report.getThanTo() != null) {
					cr.add(Restrictions.le("dyeing.addedThan", report.getThanTo()));
				}
			}
			
			cr.add(and);
			
		}else {
			
			Disjunction or = Restrictions.disjunction();
			
			if(report.getPackingDone()!=null) {
				if(report.getPackingDone()) {
					or.add(Restrictions.eq("dyeing.packingDone", true));
				}else {
					or.add(Restrictions.eq("dyeing.packingDone", false));
				}
			}
			if(Helper.isNotEmpty(report.getBiltyNo())) {
				or.add(Restrictions.ilike("grey.biltyNo", report.getBiltyNo(), MatchMode.ANYWHERE));
			}
			if(Helper.isNotEmpty(report.getDyeingSerialNo())) {
				or.add(Restrictions.ilike("program.serialNo", report.getDyeingSerialNo(), MatchMode.ANYWHERE));
			}if(Helper.isNotEmpty(report.getLotNo())) {
				or.add(Restrictions.ilike("grey.lotNo", report.getLotNo(), MatchMode.ANYWHERE));
			}if(Helper.isNotEmpty(report.getLotSerialNo())) {
				or.add(Restrictions.ilike("lot.serialNo", report.getLotSerialNo(), MatchMode.ANYWHERE));
			}if(!Helper.isNull(report.getParty()) && !Helper.isNull(report.getParty().getId())) {
				or.add(Restrictions.eq("party.id", report.getParty().getId()));
			}if(!Helper.isNull(report.getQuality()) && !Helper.isNull(report.getQuality().getId())) {
				or.add(Restrictions.eq("quality.id", report.getQuality().getId()));
			}if(!Helper.isNull(report.getShade()) && !Helper.isNull(report.getShade().getId())) {
				or.add(Restrictions.eq("shade.id", report.getShade().getId()));
			}
			
			if(report.getLotReceivedDateFrom() != null && report.getLotReceivedDateTo() != null) {
				cr.add(Restrictions.ge("lot.receivedDate", report.getLotReceivedDateFrom()));
				cr.add(Restrictions.le("lot.receivedDate", report.getLotReceivedDateTo()));
			}else {
				if(report.getLotReceivedDateFrom() != null) {
					or.add(Restrictions.ge("lot.receivedDate", report.getLotReceivedDateFrom()));
				}if(report.getLotReceivedDateTo() != null) {
					or.add(Restrictions.le("lot.receivedDate", report.getLotReceivedDateTo()));
				}
			}
			if(report.getProgramDateFrom() != null && report.getProgramDateTo() != null) {
				cr.add(Restrictions.ge("program.programDate", report.getProgramDateFrom()));
				cr.add(Restrictions.le("program.programDate", report.getProgramDateTo()));
			}else {
				if(report.getProgramDateFrom() != null) {
					or.add(Restrictions.ge("program.programDate", report.getProgramDateFrom()));
				}if(report.getProgramDateTo() != null) {
					or.add(Restrictions.le("program.programDate", report.getProgramDateTo()));
				}
			}
			
			if(report.getThanFrom() != null && report.getThanTo() != null) {
				cr.add(Restrictions.ge("dyeing.addedThan", report.getThanFrom()));
				cr.add(Restrictions.le("dyeing.addedThan", report.getThanTo()));
			}else {
				if(report.getThanFrom() != null) {
					or.add(Restrictions.ge("dyeing.addedThan", report.getThanFrom()));
				}
				if(report.getThanTo() != null) {
					or.add(Restrictions.le("dyeing.addedThan", report.getThanTo()));
				}
			}
			
			cr.add(or);
			
		}
		
		cr.addOrder(Order.desc("program.created_at"));
		cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return cr.list();
	}
	
	
	@Override
	public List<LotEntryEntity> searchLotEntryReport(LotEntryReportModel report) throws ParseException{
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(LotEntryEntity.class, "lot");
		cr.createCriteria("lot.grey", "grey")
		.createCriteria("grey.party", "party")
		.createCriteria("grey.quality", "quality");
		
		if(report.getCriteria().equals("and")) {
			
			Conjunction and = Restrictions.conjunction();
			
			if(report.getLotDone()!=null) {
				if(report.getLotDone()) {
					and.add(Restrictions.eq("lot.completed", true));
				}else {
					and.add(Restrictions.eq("lot.completed", false));
				}
			}
			if(Helper.isNotEmpty(report.getBiltyNo())) {
				and.add(Restrictions.ilike("grey.biltyNo", report.getBiltyNo(), MatchMode.ANYWHERE));
			}
			if(Helper.isNotEmpty(report.getGreySerialNo())) {
				and.add(Restrictions.ilike("grey.serialNo", report.getGreySerialNo(), MatchMode.ANYWHERE));
			}if(Helper.isNotEmpty(report.getLotNo())) {
				and.add(Restrictions.ilike("grey.lotNo", report.getLotNo(), MatchMode.ANYWHERE));
			}if(Helper.isNotEmpty(report.getLotSerialNo())) {
				and.add(Restrictions.ilike("lot.serialNo", report.getLotSerialNo(), MatchMode.ANYWHERE));
			}if(!Helper.isNull(report.getParty()) && !Helper.isNull(report.getParty().getId())) {
				and.add(Restrictions.eq("party.id", report.getParty().getId()));
			}if(!Helper.isNull(report.getQuality()) && !Helper.isNull(report.getQuality().getId())) {
				and.add(Restrictions.eq("quality.id", report.getQuality().getId()));
			}
			
			if(report.getLotReceivedDateFrom() != null && report.getLotReceivedDateTo() != null) {
				cr.add(Restrictions.ge("lot.receivedDate", report.getLotReceivedDateFrom()));
				cr.add(Restrictions.le("lot.receivedDate", report.getLotReceivedDateTo()));
			}else {
				if(report.getLotReceivedDateFrom() != null) {
					and.add(Restrictions.ge("lot.receivedDate", report.getLotReceivedDateFrom()));
				}if(report.getLotReceivedDateTo() != null) {
					and.add(Restrictions.le("lot.receivedDate", report.getLotReceivedDateTo()));
				}
			}
			
			if(report.getThanFrom() != null && report.getThanTo() != null) {
				cr.add(Restrictions.ge("lot.received", report.getThanFrom()));
				cr.add(Restrictions.le("lot.received", report.getThanTo()));
			}else {
				if(report.getThanFrom() != null) {
					cr.add(Restrictions.ge("lot.received", report.getThanFrom()));
				}
				if(report.getThanTo() != null) {
					cr.add(Restrictions.le("lot.received", report.getThanTo()));
				}
			}
			
			cr.add(and);
			
		}else {
			
			Disjunction or = Restrictions.disjunction();
			
			if(report.getLotDone()!=null) {
				if(report.getLotDone()) {
					or.add(Restrictions.eq("lot.completed", true));
				}else {
					or.add(Restrictions.eq("lot.completed", false));
				}
			}
			if(Helper.isNotEmpty(report.getBiltyNo())) {
				or.add(Restrictions.ilike("grey.biltyNo", report.getBiltyNo(), MatchMode.ANYWHERE));
			}
			if(Helper.isNotEmpty(report.getGreySerialNo())) {
				or.add(Restrictions.ilike("grey.serialNo", report.getGreySerialNo(), MatchMode.ANYWHERE));
			}if(Helper.isNotEmpty(report.getLotNo())) {
				or.add(Restrictions.ilike("grey.lotNo", report.getLotNo(), MatchMode.ANYWHERE));
			}if(Helper.isNotEmpty(report.getLotSerialNo())) {
				or.add(Restrictions.ilike("lot.serialNo", report.getLotSerialNo(), MatchMode.ANYWHERE));
			}if(!Helper.isNull(report.getParty()) && !Helper.isNull(report.getParty().getId())) {
				or.add(Restrictions.eq("party.id", report.getParty().getId()));
			}if(!Helper.isNull(report.getQuality()) && !Helper.isNull(report.getQuality().getId())) {
				or.add(Restrictions.eq("quality.id", report.getQuality().getId()));
			}
			
			if(report.getLotReceivedDateFrom() != null && report.getLotReceivedDateTo() != null) {
				cr.add(Restrictions.ge("lot.receivedDate", report.getLotReceivedDateFrom()));
				cr.add(Restrictions.le("lot.receivedDate", report.getLotReceivedDateTo()));
			}else {
				if(report.getLotReceivedDateFrom() != null) {
					or.add(Restrictions.ge("lot.receivedDate", report.getLotReceivedDateFrom()));
				}if(report.getLotReceivedDateTo() != null) {
					or.add(Restrictions.le("lot.receivedDate", report.getLotReceivedDateTo()));
				}
			}
			
			if(report.getThanFrom() != null && report.getThanTo() != null) {
				cr.add(Restrictions.ge("lot.received", report.getThanFrom()));
				cr.add(Restrictions.le("lot.received", report.getThanTo()));
			}else {
				if(report.getThanFrom() != null) {
					or.add(Restrictions.ge("lot.received", report.getThanFrom()));
				}
				if(report.getThanTo() != null) {
					or.add(Restrictions.le("lot.received", report.getThanTo()));
				}
			}
			
			cr.add(or);
			
		}
		
		cr.addOrder(Order.desc("lot.created_at"));
		cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return cr.list();
	}
	
	@Override
	public List<LotEntryEntity> searchLotEntry(LotEntryModel model) throws ParseException{
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(LotEntryEntity.class, "lot")
					.createAlias("lot.grey", "grey");
		
		if(model.getGrey()!=null) {
			if(Helper.isNotEmpty(model.getGrey().getLotNo())) {
				cr.add(Restrictions.ilike("grey.lotNo", model.getGrey().getLotNo(), MatchMode.ANYWHERE));
			}
			
			if(model.getGrey().getParty()!=null && model.getGrey().getParty().getId()!=null) {
				cr.createAlias("grey.party", "party");
				cr.add(Restrictions.eq("party.id", model.getGrey().getParty().getId()));
			}
		}
		
		if(!Helper.isNull(model.getShowAvailable()) && model.getShowAvailable())
		{
			cr.add(Restrictions.eq("lot.completed", false));
		}
		
		return cr.list();
	}
	
	
	/************************************************************************/
	
	@Override
	public List<LotEntryEntity> getPartyLotWise(PartyLotWiseModel model){
		
		Criteria cr = this.sessionFactory.getCurrentSession().createCriteria(LotEntryEntity.class, "le")
				 				.createAlias("le.grey", "grey")
				 				.createAlias("grey.party", "party");
		cr.add(Restrictions.ge("le.receivedDate", model.getFrom()));
		cr.add(Restrictions.le("le.receivedDate", model.getTo()));
		
		if(model.getPartyModel()!=null) {
			cr.add(Restrictions.eq("party.id", model.getPartyModel().getId()));
		}
		
		return cr.list();
	}
	
	@Override
	public synchronized void deleteUserReportData(Long rtype, Long typeId, Long userId) {
		String sqlDelete = "delete from ReportUserDataEntity r where r.pk.reportType =:rtype and r.pk.userId =:userId and r.pk.typeId =:typeId";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sqlDelete);
		
		query.setLong("rtype", rtype);
		query.setLong("typeId", typeId);
		query.setLong("userId", userId);
		
		query.executeUpdate();
	}
	
	
}
