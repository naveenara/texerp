package com.erp.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.erp.dao.UserDao;
import com.erp.entity.UserEntity;

@Repository("userDao")
public class UserDaoImpl implements UserDao {

	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
	public UserEntity validateUser(String userName, String password) {
		Criteria criteria = sessionFactory
	            .getCurrentSession().createCriteria(UserEntity.class);
	    criteria.add(Restrictions.eq("username", userName));
	    criteria.add(Restrictions.eq("password", password));

	    UserEntity user=(UserEntity) criteria.uniqueResult();
	    return user;
	}
	
	@Override
	public UserEntity getUserEdit(String username, String email, Long id) {
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserEntity.class);
		
		Disjunction or = Restrictions.disjunction();
		or.add(Restrictions.eq("username", username));
		or.add(Restrictions.eq("email", email));
		
		criteria.add(or);
		
		Conjunction and = Restrictions.conjunction();
		and.add(Restrictions.ne("id", id));
		
		criteria.add(and);
		
	    UserEntity user=(UserEntity) criteria.uniqueResult();
	    return user;
	}
}
