package com.erp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="DEPARTMENT")
@SQLDelete(sql = "update DEPARTMENT set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class DepartmentEntity extends BaseEntity{

    /**
     * 
     */
    private static final long serialVersionUID = -505809399042094426L;

    @Column(name="NAME")
    private String name;
    
    @Column(name="CODE")
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DepartmentEntity other = (DepartmentEntity) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "DepartmentEntity [name=" + name + ", code=" + code + ", getCreated_at()=" + getCreated_at() + ", getCreated_by()=" + getCreated_by() + ", getUpdated_at()=" + getUpdated_at() + ", getUpdated_by()=" + getUpdated_by() + ", getDeleted()=" + getDeleted() + ", getClass()=" + getClass() + ", toString()=" + super.toString() + "]";
    }
    
    
    
}
