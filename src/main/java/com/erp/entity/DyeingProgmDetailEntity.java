package com.erp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="DEYING_PROGRAM_DETAIL")
@SQLDelete(sql = "update DEYING_PROGRAM_DETAIL set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class DyeingProgmDetailEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2467969836472876424L;

	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SHADE_ID", referencedColumnName = "id")
	private ShadeEntity shade;
	
	@Column(name="BALANCE_THAN")
	private Integer balanceThan;
	
	@Column(name="THAN")
	private Integer addedThan;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "LOT_ID", referencedColumnName = "id")
	private LotEntryEntity lot;
	
	@ManyToOne
    @JoinColumn(name = "DEYING_PROGRAM_ID", referencedColumnName = "id")
    private DyeingProgmEntity program;
	
	@Column(name="PACKING_DONE", columnDefinition = "boolean default false")
	private Boolean packingDone;

	public ShadeEntity getShade() {
		return shade;
	}

	public void setShade(ShadeEntity shade) {
		this.shade = shade;
	}

	public Integer getAddedThan() {
		return addedThan;
	}

	public void setAddedThan(Integer addedThan) {
		this.addedThan = addedThan;
	}

	public LotEntryEntity getLot() {
		return lot;
	}

	public void setLot(LotEntryEntity lot) {
		this.lot = lot;
	}

	public Integer getBalanceThan() {
		return balanceThan;
	}

	public void setBalanceThan(Integer balanceThan) {
		this.balanceThan = balanceThan;
	}

	public DyeingProgmEntity getProgram() {
		return program;
	}

	public void setProgram(DyeingProgmEntity program) {
		this.program = program;
	}

	public Boolean getPackingDone() {
		return packingDone;
	}

	public void setPackingDone(Boolean packingDone) {
		this.packingDone = packingDone;
	}
	
}
