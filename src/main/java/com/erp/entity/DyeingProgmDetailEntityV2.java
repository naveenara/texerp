package com.erp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="DEYING_PROGRAM_DETAIL_V2")
@SQLDelete(sql = "update DEYING_PROGRAM_DETAIL_V2 set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class DyeingProgmDetailEntityV2 extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4623162892774915133L;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SHADE_ID", referencedColumnName = "id")
	private ShadeEntity shade;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "LOT_ID", referencedColumnName = "id")
	private LotEntryEntity lot;
	
	@Column(name="BALANCE_THAN")
	private Integer balanceThan;
	
	@Column(name="THAN")
	private Integer addedThan;
	
	@Column(name="LENGTH")
	private Float length;
	
	@ManyToOne
    @JoinColumn(name = "DEYING_PROGRAM_ID", referencedColumnName = "id")
    private DyeingProgmEntityV2 program;
	
	@Column(name="PACKING_DONE", columnDefinition = "boolean default false")
	private Boolean packingDone;
	
	@Column(name="BATCH_NO")
	private Integer batchNo;

	public ShadeEntity getShade() {
		return shade;
	}

	public void setShade(ShadeEntity shade) {
		this.shade = shade;
	}

	public LotEntryEntity getLot() {
		return lot;
	}

	public void setLot(LotEntryEntity lot) {
		this.lot = lot;
	}

	public Integer getBalanceThan() {
		return balanceThan;
	}

	public void setBalanceThan(Integer balanceThan) {
		this.balanceThan = balanceThan;
	}

	public Integer getAddedThan() {
		return addedThan;
	}

	public void setAddedThan(Integer addedThan) {
		this.addedThan = addedThan;
	}

	public DyeingProgmEntityV2 getProgram() {
		return program;
	}

	public void setProgram(DyeingProgmEntityV2 program) {
		this.program = program;
	}

	public Boolean getPackingDone() {
		return packingDone;
	}

	public void setPackingDone(Boolean packingDone) {
		this.packingDone = packingDone;
	}

	public Float getLength() {
		return length;
	}

	public void setLength(Float length) {
		this.length = length;
	}

	public Integer getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(Integer batchNo) {
		this.batchNo = batchNo;
	}

}
