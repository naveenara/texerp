package com.erp.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="DEYING_PROGRAM")
@SQLDelete(sql = "update DEYING_PROGRAM set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class DyeingProgmEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6544146551622530832L;

	@Column(name="SERIAL_NO", unique = true)
	private String serialNo;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PARTY_ID", referencedColumnName = "id")
	private PartyEntity party;
	
	@Column(name="THAN")
	private Integer than;
	
	@Column(name="RECEIVED_DT")
    @Temporal(TemporalType.TIMESTAMP)
	private Date programDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "program", cascade = CascadeType.ALL, orphanRemoval=true)
	@Where(clause="deleted <> '1'")
    private List<DyeingProgmDetailEntity> details;
	
	@Column(name="SENT", columnDefinition = "boolean default false")
	private Boolean sent;
	
	@Column(name="LOT_NO")
	private String lotNo;
	
	private String yearFormat;
	
	@Column(name="SHADES")
	private Integer shadeCount;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public PartyEntity getParty() {
		return party;
	}

	public void setParty(PartyEntity party) {
		this.party = party;
	}

	public Integer getThan() {
		return than;
	}

	public void setThan(Integer than) {
		this.than = than;
	}

	public Date getProgramDate() {
		return programDate;
	}

	public void setProgramDate(Date programDate) {
		this.programDate = programDate;
	}

	public List<DyeingProgmDetailEntity> getDetails() {
		return details;
	}

	public void setDetails(List<DyeingProgmDetailEntity> details) {
		this.details = details;
	}

	public Boolean getSent() {
		return sent;
	}

	public void setSent(Boolean sent) {
		this.sent = sent;
	}

	public String getYearFormat() {
		return yearFormat;
	}

	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}

	public Integer getShadeCount() {
		return shadeCount;
	}

	public void setShadeCount(Integer shadeCount) {
		this.shadeCount = shadeCount;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

}
