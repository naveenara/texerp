package com.erp.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="DEYING_PROGRAM_V2")
@SQLDelete(sql = "update DEYING_PROGRAM_V2 set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class DyeingProgmEntityV2 extends BaseEntity{
	
	
	public static void main(String[] args) {
		Long l1 = -1L;
		l1 = l1 - 4L;
		
		System.out.println(l1);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8326284620958259903L;

	
	@Column(name="SERIAL_NO", unique = true)
	private String serialNo;
	
	@Column(name="RECEIVED_DT")
    @Temporal(TemporalType.TIMESTAMP)
	private Date programDate;
	
	@Column(name="SENT", columnDefinition = "boolean default false")
	private Boolean sent;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MACHINE_ID", referencedColumnName = "id")
	private MasterEntity machine;
	
	@Column(name="THAN")
	private Integer than;
	
	@Column(name="BASE_NO")
	private Long baseNo;
	
	@Column(name="SHADE")
	private String shade;
	
	@Column(name="LOT_NO")
	private String lotNo;
	
	@Column(name="PARTY")
	private String party;
	
	@Column(name="SHADE_COUNT")
	private Integer shadeCount;
	
	private String yearFormat;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "program", cascade = CascadeType.ALL, orphanRemoval=true)
	@Where(clause="deleted <> '1'")
    private List<DyeingProgmDetailEntityV2> details;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public Date getProgramDate() {
		return programDate;
	}

	public void setProgramDate(Date programDate) {
		this.programDate = programDate;
	}

	public Boolean getSent() {
		return sent;
	}

	public void setSent(Boolean sent) {
		this.sent = sent;
	}

	public MasterEntity getMachine() {
		return machine;
	}

	public void setMachine(MasterEntity machine) {
		this.machine = machine;
	}

	public Long getBaseNo() {
		return baseNo;
	}

	public void setBaseNo(Long baseNo) {
		this.baseNo = baseNo;
	}

	public String getShade() {
		return shade;
	}

	public void setShade(String shade) {
		this.shade = shade;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public String getParty() {
		return party;
	}

	public void setParty(String party) {
		this.party = party;
	}

	public Integer getShadeCount() {
		return shadeCount;
	}

	public void setShadeCount(Integer shadeCount) {
		this.shadeCount = shadeCount;
	}

	public String getYearFormat() {
		return yearFormat;
	}

	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}

	public List<DyeingProgmDetailEntityV2> getDetails() {
		return details;
	}

	public void setDetails(List<DyeingProgmDetailEntityV2> details) {
		this.details = details;
	}

	public Integer getThan() {
		return than;
	}

	public void setThan(Integer than) {
		this.than = than;
	}
	
}
