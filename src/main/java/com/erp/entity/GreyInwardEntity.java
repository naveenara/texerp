package com.erp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="GREY_INWARD")
@SQLDelete(sql = "update GREY_INWARD set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class GreyInwardEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4053760301840394247L;

	@Column(name="VOUCHER_NO")
	private Long voucherNo;
	
	@Column(name="SERIAL_NO", unique = true)
	private String serialNo;
	
	@Column(name="BILTY_NO")
	private String biltyNo;
	
	@Column(name="BALES")
	private Integer bales;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TRANSPORT_ID", referencedColumnName = "id")
	private MasterEntity transport;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PARTY_ID", referencedColumnName = "id")
	private PartyEntity party;
	
	@Column(name="LOT_NO", unique=true)
	private String lotNo;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "QUALITY_ID", referencedColumnName = "id")
	private MasterEntity quality;
	
	@Column(name="THAN")
	private Integer than;
	
	@Column(name="LENGTH")
	private Double length;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "UNIT_ID", referencedColumnName = "id")
	private MasterEntity unit;
	
	@Column(name="TP")
	private Integer tp;
	
	@Column(name="NOTES")
	private String notes;
	
	@Column(name="DATE")
    @Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@Column(name="LR_NO")
	private String lrNo;
	
	private String yearFormat;
	
	@Column(name="LOT_DONE", columnDefinition = "boolean default false")
	private Boolean lotDone;

	public Long getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(Long voucherNo) {
		this.voucherNo = voucherNo;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getBiltyNo() {
		return biltyNo;
	}

	public void setBiltyNo(String biltyNo) {
		this.biltyNo = biltyNo;
	}

	public Integer getBales() {
		return bales;
	}

	public void setBales(Integer bales) {
		this.bales = bales;
	}

	public MasterEntity getTransport() {
		return transport;
	}

	public void setTransport(MasterEntity transport) {
		this.transport = transport;
	}

	public PartyEntity getParty() {
		return party;
	}

	public void setParty(PartyEntity party) {
		this.party = party;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public MasterEntity getQuality() {
		return quality;
	}

	public void setQuality(MasterEntity quality) {
		this.quality = quality;
	}

	public Integer getThan() {
		return than;
	}

	public void setThan(Integer than) {
		this.than = than;
	}

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public MasterEntity getUnit() {
		return unit;
	}

	public void setUnit(MasterEntity unit) {
		this.unit = unit;
	}

	public Integer getTp() {
		return tp;
	}

	public void setTp(Integer tp) {
		this.tp = tp;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getYearFormat() {
		return yearFormat;
	}

	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}

	public Boolean getLotDone() {
		return lotDone;
	}

	public void setLotDone(Boolean lotDone) {
		this.lotDone = lotDone;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getLrNo() {
		return lrNo;
	}

	public void setLrNo(String lrNo) {
		this.lrNo = lrNo;
	}
	
}
