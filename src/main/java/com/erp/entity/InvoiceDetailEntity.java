package com.erp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="INVOICE_DETAIL")
@SQLDelete(sql = "update INVOICE_DETAIL set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class InvoiceDetailEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1567878279339419441L;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PROCESS_PACKING_ID", referencedColumnName = "id")
	private ProcessPackingEntity pp;
	
	@Column(name="RATE")
	private Float rate;
	
	@Column(name="AMOUNT")
	private Float amount;
	
	@ManyToOne
    @JoinColumn(name = "INVOICE_ID", referencedColumnName = "id")
    private InvoiceEntity invoice;

	public ProcessPackingEntity getPp() {
		return pp;
	}

	public void setPp(ProcessPackingEntity pp) {
		this.pp = pp;
	}

	public Float getRate() {
		return rate;
	}

	public void setRate(Float rate) {
		this.rate = rate;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public InvoiceEntity getInvoice() {
		return invoice;
	}

	public void setInvoice(InvoiceEntity invoice) {
		this.invoice = invoice;
	}
}
