package com.erp.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="INVOICE")
@SQLDelete(sql = "update INVOICE set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class InvoiceEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7422759606906582656L;

	@Column(name="SERIAL_NO", unique = true)
	private String serialNo;
	
	@Column(name="INVOICE_DT")
    @Temporal(TemporalType.TIMESTAMP)
	private Date invoiceDate;
	
	@Column(name="GREY_LR_NO")
	private String greyLrNo;
	
	@Column(name="E_WAY_BILL_NO")
	private String ewayBillNo;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TRANSPORT_ID", referencedColumnName = "id")
	private MasterEntity transport;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "BILL_TO", referencedColumnName = "id")
	private PartyEntity billTo;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SHIP_TO", referencedColumnName = "id")
	private PartyEntity shipTo;
	
	@Column(name="SUPPLY_PLACE")
	private String placeOfSupply;
	
	@Column(name="THAN")
	private Integer than;
	
	@Column(name="LENGTH")
	private Integer length;
	
	@Column(name="AMOUNT")
	private Double amount;
	
	@Column(name="PARTY_LOCATION")
	private String partyLocation;
	
	@Column(name="IGST_AMT")
	private Float igstAmt;
	
	@Column(name="CGST_AMT")
	private Float cgstAmt;
	
	@Column(name="SGST_AMT")
	private Float sgstAmt;

	@Column(name="TOTAL_AMT_WORDS")
	private String totalAmtWords;
	
	@Column(name="TOTAL_AMOUNT")
	private Integer totalAmount;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice", cascade = CascadeType.ALL, orphanRemoval=true)
	@Where(clause="deleted <> '1'")
    private List<InvoiceDetailEntity> details;
	
	private String yearFormat;

	public String getGreyLrNo() {
		return greyLrNo;
	}

	public void setGreyLrNo(String greyLrNo) {
		this.greyLrNo = greyLrNo;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getEwayBillNo() {
		return ewayBillNo;
	}

	public void setEwayBillNo(String ewayBillNo) {
		this.ewayBillNo = ewayBillNo;
	}

	public MasterEntity getTransport() {
		return transport;
	}

	public void setTransport(MasterEntity transport) {
		this.transport = transport;
	}

	public PartyEntity getBillTo() {
		return billTo;
	}

	public void setBillTo(PartyEntity billTo) {
		this.billTo = billTo;
	}

	public PartyEntity getShipTo() {
		return shipTo;
	}

	public void setShipTo(PartyEntity shipTo) {
		this.shipTo = shipTo;
	}

	public String getPlaceOfSupply() {
		return placeOfSupply;
	}

	public void setPlaceOfSupply(String placeOfSupply) {
		this.placeOfSupply = placeOfSupply;
	}

	public Integer getThan() {
		return than;
	}

	public void setThan(Integer than) {
		this.than = than;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getYearFormat() {
		return yearFormat;
	}

	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}

	public List<InvoiceDetailEntity> getDetails() {
		return details;
	}

	public void setDetails(List<InvoiceDetailEntity> details) {
		this.details = details;
	}

	public String getPartyLocation() {
		return partyLocation;
	}

	public void setPartyLocation(String partyLocation) {
		this.partyLocation = partyLocation;
	}

	public Float getIgstAmt() {
		return igstAmt;
	}

	public void setIgstAmt(Float igstAmt) {
		this.igstAmt = igstAmt;
	}

	public Float getCgstAmt() {
		return cgstAmt;
	}

	public void setCgstAmt(Float cgstAmt) {
		this.cgstAmt = cgstAmt;
	}

	public Float getSgstAmt() {
		return sgstAmt;
	}

	public void setSgstAmt(Float sgstAmt) {
		this.sgstAmt = sgstAmt;
	}

	public String getTotalAmtWords() {
		return totalAmtWords;
	}

	public void setTotalAmtWords(String totalAmtWords) {
		this.totalAmtWords = totalAmtWords;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	
}
