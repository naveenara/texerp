package com.erp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="LOT_ENTRY")
@SQLDelete(sql = "update LOT_ENTRY set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class LotEntryEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7728338770828426645L;

	@Column(name="SERIAL_NO", unique = true)
	private String serialNo;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GREYINWARD_ID", referencedColumnName = "id")
	private GreyInwardEntity grey;
	
	@Column(name="BALANCE")
	private Integer balance;
	
	@Column(name="RECEIVED_LENGTH")
	private Integer received;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "UNIT_ID", referencedColumnName = "id")
	private MasterEntity unit;
	
	@Column(name="RECEIVED_DT")
    @Temporal(TemporalType.TIMESTAMP)
	private Date receivedDate;
	
	@Column(name="WEIGHT")
	private Float weight;
	
	@Column(name="COMPLETED", columnDefinition = "boolean default false")
	private Boolean completed;
	
	private String yearFormat;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public GreyInwardEntity getGrey() {
		return grey;
	}

	public void setGrey(GreyInwardEntity grey) {
		this.grey = grey;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public Integer getReceived() {
		return received;
	}

	public void setReceived(Integer received) {
		this.received = received;
	}

	public String getYearFormat() {
		return yearFormat;
	}

	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public MasterEntity getUnit() {
		return unit;
	}

	public void setUnit(MasterEntity unit) {
		this.unit = unit;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}
	
	
}
