package com.erp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="PARTIES")
@SQLDelete(sql = "update PARTIES set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class PartyEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2796369187900417844L;

	@Column(name="NAME")
	private String name;
	
	@Column(name="PAN_NO")
	private String panNo;
	
	@Column(name="VAT_NO")
	private String vatNo;
	
	@Column(name="CST_NO")
	private String cstNo;
	
	@Column(name="TAN_NO")
	private String tanNo;
	
	@Column(name="BROKERAGE")
	private String brokerage;
	
	@Column(name="ADDRESS")
	private String address;
	
	@Column(name="AREA")
	private String area;
	
	@Column(name="CITY")
	private String city;
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="COUNTRY")
	private String country;
	
	@Column(name="PIN_cODE")
	private String pinCode;
	
	@Column(name="TEL_NO")
	private String telNo;
	
	@Column(name="MON_NO")
	private String mobNo;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="PAYTERM")
	private String payterm;
	
	@Column(name="CIN_NO")
	private String cinNo;
	
	@Column(name="HSN_CODE")
	private String hsnCode;
	
	@Column(name="GST_NO")
	private String gstNo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getVatNo() {
		return vatNo;
	}

	public void setVatNo(String vatNo) {
		this.vatNo = vatNo;
	}

	public String getCstNo() {
		return cstNo;
	}

	public void setCstNo(String cstNo) {
		this.cstNo = cstNo;
	}

	public String getTanNo() {
		return tanNo;
	}

	public void setTanNo(String tanNo) {
		this.tanNo = tanNo;
	}

	public String getBrokerage() {
		return brokerage;
	}

	public void setBrokerage(String brokerage) {
		this.brokerage = brokerage;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getTelNo() {
		return telNo;
	}

	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	public String getMobNo() {
		return mobNo;
	}

	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPayterm() {
		return payterm;
	}

	public void setPayterm(String payterm) {
		this.payterm = payterm;
	}

	public String getCinNo() {
		return cinNo;
	}

	public void setCinNo(String cinNo) {
		this.cinNo = cinNo;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

}
