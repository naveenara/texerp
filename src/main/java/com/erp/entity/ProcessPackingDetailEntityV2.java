package com.erp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="PROCESS_PACKING_DETAIL_V2")
@SQLDelete(sql = "update PROCESS_PACKING_DETAIL_V2 set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class ProcessPackingDetailEntityV2 extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1088368839494498283L;

	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DYEING_DETAIL_ID", referencedColumnName = "id")
	private DyeingProgmDetailEntityV2 detail;
	
	@Column(name="LENGTH")
	private Double length;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "UNIT_ID", referencedColumnName = "id")
	private MasterEntity unit;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PROCESS_ID", referencedColumnName = "id")
	private MasterEntity process;
	
	@Column(name="LR_NO")
	private String lrNo;
	
	@Column(name="NOTES")
	private String notes;
	
	@ManyToOne
    @JoinColumn(name = "PROCESS_PACKING_ID", referencedColumnName = "id")
    private ProcessPackingEntityV2 processEntity;

	public DyeingProgmDetailEntityV2 getDetail() {
		return detail;
	}

	public void setDetail(DyeingProgmDetailEntityV2 detail) {
		this.detail = detail;
	}

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public MasterEntity getUnit() {
		return unit;
	}

	public void setUnit(MasterEntity unit) {
		this.unit = unit;
	}

	public MasterEntity getProcess() {
		return process;
	}

	public void setProcess(MasterEntity process) {
		this.process = process;
	}

	public String getLrNo() {
		return lrNo;
	}

	public void setLrNo(String lrNo) {
		this.lrNo = lrNo;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public ProcessPackingEntityV2 getProcessEntity() {
		return processEntity;
	}

	public void setProcessEntity(ProcessPackingEntityV2 processEntity) {
		this.processEntity = processEntity;
	}

}
