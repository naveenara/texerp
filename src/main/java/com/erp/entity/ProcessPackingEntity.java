package com.erp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="PROCESS_PACKING")
@SQLDelete(sql = "update PROCESS_PACKING set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class ProcessPackingEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2006434047448591915L;

	@Column(name="SERIAL_NO", unique = true)
	private String serialNo;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DYEING_DETAIL_ID", referencedColumnName = "id")
	private DyeingProgmDetailEntityV2 detail;
	
	@Column(name="LENGTH")
	private Double length;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "UNIT_ID", referencedColumnName = "id")
	private MasterEntity unit;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PROCESS_ID", referencedColumnName = "id")
	private MasterEntity process;
	
	@Column(name="LR_NO")
	private String lrNo;
	
	@Column(name="FOLD")
	private String fold;
	
	@Column(name="NOTES")
	private String notes;
	
	@Column(name="BALE_NO")
	private Long baleNo;
	
	@Column(name="DEL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
	private Date deliveryDate;
	
	@Column(name="INVOICE_DONE", columnDefinition = "boolean default false")
	private Boolean invoiceDone;
	
	private String yearFormat;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public DyeingProgmDetailEntityV2 getDetail() {
		return detail;
	}

	public void setDetail(DyeingProgmDetailEntityV2 detail) {
		this.detail = detail;
	}

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public MasterEntity getUnit() {
		return unit;
	}

	public void setUnit(MasterEntity unit) {
		this.unit = unit;
	}

	public MasterEntity getProcess() {
		return process;
	}

	public void setProcess(MasterEntity process) {
		this.process = process;
	}

	public String getLrNo() {
		return lrNo;
	}

	public void setLrNo(String lrNo) {
		this.lrNo = lrNo;
	}

	public String getFold() {
		return fold;
	}

	public void setFold(String fold) {
		this.fold = fold;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getYearFormat() {
		return yearFormat;
	}

	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}

	public Long getBaleNo() {
		return baleNo;
	}

	public void setBaleNo(Long baleNo) {
		this.baleNo = baleNo;
	}

	public Boolean getInvoiceDone() {
		return invoiceDone;
	}

	public void setInvoiceDone(Boolean invoiceDone) {
		this.invoiceDone = invoiceDone;
	}
	
}
