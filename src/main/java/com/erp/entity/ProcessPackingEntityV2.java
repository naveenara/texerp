package com.erp.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="PROCESS_PACKING_V2")
@SQLDelete(sql = "update PROCESS_PACKING_V2 set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class ProcessPackingEntityV2 extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7190427438866408387L;

	@Column(name="SERIAL_NO", unique = true)
	private String serialNo;
	
	@Column(name="DEL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
	private Date deliveryDate;
	
	@Column(name="BALE_NO")
	private Long baleNo;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PARTY_ID", referencedColumnName = "id")
	private PartyEntity party;
	
	@Column(name="INVOICE_DONE", columnDefinition = "boolean default false")
	private Boolean invoiceDone;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "processEntity", cascade = CascadeType.ALL, orphanRemoval=true)
	@Where(clause="deleted <> '1'")
    private List<ProcessPackingDetailEntityV2> details;
	
	@Column(name="THAN")
	private Integer than;
	
	@Column(name="LENGTH")
	private Float length;
	
	@Column(name="LOT_NO")
	private String lotNo;
	
	private String yearFormat;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Long getBaleNo() {
		return baleNo;
	}

	public void setBaleNo(Long baleNo) {
		this.baleNo = baleNo;
	}

	public PartyEntity getParty() {
		return party;
	}

	public void setParty(PartyEntity party) {
		this.party = party;
	}

	public Boolean getInvoiceDone() {
		return invoiceDone;
	}

	public void setInvoiceDone(Boolean invoiceDone) {
		this.invoiceDone = invoiceDone;
	}

	public String getYearFormat() {
		return yearFormat;
	}

	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}

	public List<ProcessPackingDetailEntityV2> getDetails() {
		return details;
	}

	public void setDetails(List<ProcessPackingDetailEntityV2> details) {
		this.details = details;
	}

	public Integer getThan() {
		return than;
	}

	public void setThan(Integer than) {
		this.than = than;
	}

	

	public Float getLength() {
		return length;
	}

	public void setLength(Float length) {
		this.length = length;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}
}
