package com.erp.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="REPORT_USER_DATA")
public class ReportUserDataEntity {

	@EmbeddedId
	private ReportUserDataEntityPK pk;

	public ReportUserDataEntityPK getPk() {
		return pk;
	}

	public void setPk(ReportUserDataEntityPK pk) {
		this.pk = pk;
	}
	
}
