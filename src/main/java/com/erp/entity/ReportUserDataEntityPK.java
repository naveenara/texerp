package com.erp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ReportUserDataEntityPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8147097213629550817L;

	@Column(name="REPORT_TYPE")
	private Long reportType;
	
	@Column(name="USER_ID")
	private Long userId;
	
	@Column(name="TYPE_ID")
	private Long typeId;
	
	@Column(name="DATA_ID")
	private Long dataId;
	
	public ReportUserDataEntityPK(Long reportType, Long typeId, Long userId, Long dataId) {
		this.reportType = reportType;
		this.typeId = typeId;
		this.userId = userId;
		this.dataId = dataId;
	}
	
	public Long getReportType() {
		return reportType;
	}
	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}


	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public Long getDataId() {
		return dataId;
	}
	public void setDataId(Long dataId) {
		this.dataId = dataId;
	}
	
}
