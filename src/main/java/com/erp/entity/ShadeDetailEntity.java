package com.erp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="SHADE_DETAIL")
@SQLDelete(sql = "update SHADE_DETAIL set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class ShadeDetailEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7963009311985108673L;

	@Column(name="WEIGHT")
	private Float weight;
	
	/*@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLOR_ID", referencedColumnName = "id")
	private MasterEntity color;*/
	
	@Column(name="COLOR")
	private String color;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "SHADE_ID", referencedColumnName = "id")
	private ShadeEntity shade;

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public ShadeEntity getShade() {
		return shade;
	}

	public void setShade(ShadeEntity shade) {
		this.shade = shade;
	}
	
}
