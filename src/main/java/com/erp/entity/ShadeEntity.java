package com.erp.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name="SHADES")
@SQLDelete(sql = "update SHADES set deleted = 1 where id = ?")
@Where(clause="deleted <> '1'")
public class ShadeEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3340161610185860615L;

	@Column(name="NAME")
	private String name;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PARTY_ID", referencedColumnName = "id")
	private PartyEntity party;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COLOR_ID", referencedColumnName = "id")
	private MasterEntity color;
	
	/*@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	//@Where(clause="deleted <> '1'")
    @JoinTable(name = "shade_color", joinColumns = { @JoinColumn(name = "shade_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "color_id", referencedColumnName = "id") })
    private Set<MasterEntity> composition = new HashSet<MasterEntity>();*/
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shade", cascade = CascadeType.ALL, orphanRemoval=true)
	@Where(clause="deleted <> '1'")
    private List<ShadeDetailEntity> details;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PartyEntity getParty() {
		return party;
	}

	public void setParty(PartyEntity party) {
		this.party = party;
	}

	public MasterEntity getColor() {
		return color;
	}

	public void setColor(MasterEntity color) {
		this.color = color;
	}

	public List<ShadeDetailEntity> getDetails() {
		return details;
	}

	public void setDetails(List<ShadeDetailEntity> details) {
		this.details = details;
	}

	/*public Set<MasterEntity> getComposition() {
		return composition;
	}

	public void setComposition(Set<MasterEntity> composition) {
		this.composition = composition;
	}*/

}