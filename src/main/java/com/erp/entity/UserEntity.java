package com.erp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;

@Entity
@Table(name="USERS")
@SQLDelete(sql = "update USERS set deleted = 1 where id = ?")
//@Where(clause="deleted <> '1'")
public class UserEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="NAME")
	private String name;
	
	@Column(name="EMAIL")
    private String email;
    
	@Column(name="USER_NAME")
    private String username;
	
	@Column(name="PASSWORD")
    private String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isActive() {
		if(this.getDeleted() == '0')
			return true;
		else
			return false;
	}
	
}
