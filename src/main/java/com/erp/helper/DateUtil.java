package com.erp.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static Date changeFormat(Date d, String oldFormatStr, String newFormatStr) throws ParseException{
        
        SimpleDateFormat oldFormat = new SimpleDateFormat(oldFormatStr);
        SimpleDateFormat newFormat = new SimpleDateFormat(newFormatStr);
        
        String oldDate = oldFormat.format(d);
        return newFormat.parse(oldDate);
        
    }
    
    public static int getDateDiff(Date fromDate, Date toDate){
        int daysDiff = (int) ((toDate.getTime() - fromDate.getTime()) / (24 * 60 * 60 * 1000));
        return daysDiff;
    }
    
    public static Date getDateWithoutTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    
    public static Date getTomorrowDate(){
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 1);
        
        Date currentDatePlusOne = c.getTime();
        return currentDatePlusOne;
    }
    
    public static String formateDate(Date d) {
    	DateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy");
    	return sdf.format(d);
    }
    
    public static String formateDateWithTime(Date d) {
    	DateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss");
    	return sdf.format(d);
    }
    
    public static Date getDate(String s, String format) throws ParseException {
        Date date=new SimpleDateFormat(format).parse(s);  
        return date;
    }
    
    public static void main(String[] args) throws ParseException{
        String inputString = "17-05-2017";
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date fromDate = dateFormat.parse(inputString);
        
        String inputString1 = "15-05-2017";
        //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date toDate = dateFormat.parse(inputString1);
        
        int diff = getDateDiff(fromDate, toDate);
        
        System.out.println(diff);
        
    }    
    
}
