package com.erp.helper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

//import com.beust.jcommander.internal.Maps;
import com.erp.controller.LotEntryController;
import com.erp.entity.DyeingProgmDetailEntity;
import com.erp.entity.DyeingProgmDetailEntityV2;
import com.erp.entity.DyeingProgmEntity;
import com.erp.entity.DyeingProgmEntityV2;
import com.erp.entity.LotEntryEntity;
import com.erp.exception.ApplicationException;
import com.erp.exception.ApplicationRuntimeException;
import com.erp.rest.model.DyeingProgmDetailModel;
import com.erp.rest.model.DyeingProgmDetailModelV2;
import com.erp.rest.model.DyeingProgmModel;
import com.erp.rest.model.DyeingProgmModelV2;
import com.erp.rest.service.DyeingProgramService;
import com.erp.service.GenericService;

import jersey.repackaged.com.google.common.collect.Maps;

@Component
public class DyeingProgramHelper {
	
	/*@Resource
	private DyeingProgramService dpService;
	
	@Resource
	private GenericService genericService;
	
	DyeingProgramHelper helper = new DyeingProgramHelper();*/
	
	public static synchronized DyeingProgmEntity getDyeingProgramEntity(DyeingProgmModel model, DyeingProgramService dpService, GenericService genericService) throws ParseException, ApplicationException {
		DyeingProgmEntity entity = new DyeingProgmEntity();
    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
    	BeanUtils.copyProperties(model, entity, "created_at", "updated_at", "details");
    	
    	List<DyeingProgmDetailModel> details = model.getDetails();
    	List<DyeingProgmDetailEntity> entityDetails = new ArrayList<>(details.size());
    	
    	//Map from UI
    	Map<Long, Integer> thansUI = getUIMap(details, entity, entityDetails);
    	
    	if(entity.getId()!=null) {
    		List<DyeingProgmDetailEntity> dbDetails = revertThans(entity.getId(), genericService, dpService);
    		checkForRemovedShades(details, dbDetails, genericService);
    	}
    	
    	addThans(thansUI, genericService);
    	
    	model.setThansDebited(thansUI);
    	entity.setDetails(entityDetails);
		return entity;
	}
	
	public static synchronized DyeingProgmEntityV2 getDyeingProgramEntityV2(DyeingProgmModelV2 model, DyeingProgramService dpService, GenericService genericService) throws ParseException, ApplicationException {
		DyeingProgmEntityV2 entity = new DyeingProgmEntityV2();
    	BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
    	BeanUtils.copyProperties(model, entity, "created_at", "updated_at", "details");
    	
    	List<DyeingProgmDetailModelV2> details = model.getDetails();
    	List<DyeingProgmDetailEntityV2> entityDetails = new ArrayList<>(details.size());
    	
    	//Map from UI
    	Map<Long, Integer> thansUI = getUIMapV2(details, entity, entityDetails);
    	
    	if(entity.getId()!=null) {
    		List<DyeingProgmDetailEntityV2> dbDetails = revertThansV2(entity.getId(), genericService, dpService);
    		checkForRemovedShadesV2(details, dbDetails, genericService);
    	}
    	
    	addThans(thansUI, genericService);
    	
    	model.setThansDebited(thansUI);
    	entity.setDetails(entityDetails);
		return entity;
	}
	
	private static Map<Long, Integer> getUIMap(List<DyeingProgmDetailModel> details, DyeingProgmEntity entity, List<DyeingProgmDetailEntity> entityDetails) throws ParseException{
		Map<Long, Integer> thansDebited = Maps.newHashMap();
    	
    	for(DyeingProgmDetailModel detail: details) {
    		DyeingProgmDetailEntity centity = new DyeingProgmDetailEntity();
    		copyDetails(detail, centity, entity);
    		
    		if(thansDebited.containsKey(detail.getLot().getId())) {
    			thansDebited.put(detail.getLot().getId(), thansDebited.get(detail.getLot().getId()) + detail.getAddedThan());
    		}else {
    			thansDebited.put(detail.getLot().getId(), detail.getAddedThan());
    		}
    		centity.setBalanceThan(centity.getLot().getBalance());
    		centity.setDeleted('0');
    		centity.setCreated_at(new Date());
    		entityDetails.add(centity);
    	}
    	return thansDebited;
	}
	
	private static Map<Long, Integer> getUIMapV2(List<DyeingProgmDetailModelV2> details, DyeingProgmEntityV2 entity, List<DyeingProgmDetailEntityV2> entityDetails) throws ParseException{
		Map<Long, Integer> thansDebited = Maps.newHashMap();
    	
    	for(DyeingProgmDetailModelV2 detail: details) {
    		DyeingProgmDetailEntityV2 centity = new DyeingProgmDetailEntityV2();
    		copyDetailsV2(detail, centity, entity);
    		
    		if(thansDebited.containsKey(detail.getLot().getId())) {
    			thansDebited.put(detail.getLot().getId(), thansDebited.get(detail.getLot().getId()) + detail.getAddedThan());
    		}else {
    			thansDebited.put(detail.getLot().getId(), detail.getAddedThan());
    		}
    		centity.setBalanceThan(centity.getLot().getBalance());
    		centity.setDeleted('0');
    		centity.setCreated_at(new Date());
    		entityDetails.add(centity);
    	}
    	return thansDebited;
	}
	
	private static List<DyeingProgmDetailEntity> revertThans(Long id,  GenericService genericService, DyeingProgramService dpService) {
		//Map<Long, Integer> thansDB = getDBMap(id, dpService);
		
		
		Map<Long, Integer> thansDebited = Maps.newHashMap();
		List<DyeingProgmDetailEntity> details = dpService.getDetails(id);
    	
    	for(DyeingProgmDetailEntity detail: details) {
    		if(thansDebited.containsKey(detail.getLot().getId())) {
    			thansDebited.put(detail.getLot().getId(), thansDebited.get(detail.getLot().getId()) + detail.getAddedThan());
    		}else {
    			thansDebited.put(detail.getLot().getId(), detail.getAddedThan());
    		}
    	}
    	//return thansDebited;
		
		Iterator<Entry<Long, Integer>> it = thansDebited.entrySet().iterator();
		while(it.hasNext()) {
			Entry<Long, Integer> entry = it.next();
			
			Long lotId = entry.getKey();
			Integer than = entry.getValue();
			
			LotEntryEntity lotEntity = genericService.findLongOne(new LotEntryEntity(), lotId);
			
			if(lotEntity.getBalance()==null)
				lotEntity.setBalance(0);
			
			if(lotEntity.getBalance()!=null) {
				lotEntity.setBalance(lotEntity.getBalance() + than);
				genericService.update(lotEntity);
			}
		}
		return details;
	}
	
	private static List<DyeingProgmDetailEntityV2> revertThansV2(Long id,  GenericService genericService, DyeingProgramService dpService) {
		//Map<Long, Integer> thansDB = getDBMap(id, dpService);
		
		
		Map<Long, Integer> thansDebited = Maps.newHashMap();
		List<DyeingProgmDetailEntityV2> details = dpService.getDetailsV2(id);
    	
    	for(DyeingProgmDetailEntityV2 detail: details) {
    		if(thansDebited.containsKey(detail.getLot().getId())) {
    			thansDebited.put(detail.getLot().getId(), thansDebited.get(detail.getLot().getId()) + detail.getAddedThan());
    		}else {
    			thansDebited.put(detail.getLot().getId(), detail.getAddedThan());
    		}
    	}
    	//return thansDebited;
		
		Iterator<Entry<Long, Integer>> it = thansDebited.entrySet().iterator();
		while(it.hasNext()) {
			Entry<Long, Integer> entry = it.next();
			
			Long lotId = entry.getKey();
			Integer than = entry.getValue();
			
			LotEntryEntity lotEntity = genericService.findLongOne(new LotEntryEntity(), lotId);
			
			if(lotEntity.getBalance()==null)
				lotEntity.setBalance(0);
			
			if(lotEntity.getBalance()!=null) {
				lotEntity.setBalance(lotEntity.getBalance() + than);
				genericService.update(lotEntity);
			}
		}
		return details;
	}
	
	
	private static void checkForRemovedShades(List<DyeingProgmDetailModel> ui, List<DyeingProgmDetailEntity> db, GenericService genericService) throws ApplicationException {
		
		for(DyeingProgmDetailEntity detail: db) {
			boolean deleted = true;
			for(DyeingProgmDetailModel uiDetail: ui) {
				if(uiDetail.getId() != null && detail.getId() == uiDetail.getId()) {
					deleted = false;
				}
			}
			if(deleted) {
				if(detail.getPackingDone()!= null && detail.getPackingDone() == true) {
					throw new ApplicationRuntimeException("Some shades were deleted for which packing was done, please modify and submit again");
				}
				genericService.delete(detail);
			}
		}
	}
	
	private static void checkForRemovedShadesV2(List<DyeingProgmDetailModelV2> ui, List<DyeingProgmDetailEntityV2> db, GenericService genericService) throws ApplicationException {
			
			for(DyeingProgmDetailEntityV2 detail: db) {
				boolean deleted = true;
				for(DyeingProgmDetailModelV2 uiDetail: ui) {
					if(uiDetail.getId() != null && detail.getId() == uiDetail.getId()) {
						deleted = false;
					}
				}
				if(deleted) {
					if(detail.getPackingDone()!= null && detail.getPackingDone() == true) {
						throw new ApplicationRuntimeException("Some shades were deleted for which packing was done, please modify and submit again");
					}
					genericService.delete(detail);
				}
			}
	}
	
	private static void addThans(Map<Long, Integer> thansUI, GenericService genericService) {
		Iterator<Entry<Long, Integer>> it = thansUI.entrySet().iterator();
		while(it.hasNext()) {
			Entry<Long, Integer> entry = it.next();
			
			Long lotId = entry.getKey();
			Integer than = entry.getValue();
			
			LotEntryEntity lotEntity = genericService.findLongOne(new LotEntryEntity(), lotId);
			
			if(lotEntity.getBalance()!=null) {
				if(lotEntity.getBalance() < than) {
					throw new ApplicationRuntimeException("Given than: "+than+", is more than balance: "+lotEntity.getBalance());
				}
				lotEntity.setBalance(lotEntity.getBalance() - than);
				if(lotEntity.getBalance() == 0) {
					lotEntity.setCompleted(true);
				}
				genericService.update(lotEntity);
			}
		}
	}
	
	private static Map<Long, Integer> getDBMap(Long id,  DyeingProgramService dpService){
		Map<Long, Integer> thansDebited = Maps.newHashMap();
		List<DyeingProgmDetailEntity> details = dpService.getDetails(id);
    	
    	for(DyeingProgmDetailEntity detail: details) {
    		if(thansDebited.containsKey(detail.getLot().getId())) {
    			thansDebited.put(detail.getLot().getId(), thansDebited.get(detail.getLot().getId()) + detail.getAddedThan());
    		}else {
    			thansDebited.put(detail.getLot().getId(), detail.getAddedThan());
    		}
    	}
    	return thansDebited;
	}
	
	private static void copyDetails(DyeingProgmDetailModel model, DyeingProgmDetailEntity entity, DyeingProgmEntity pentity) throws ParseException {
		BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
		BeanUtils.copyProperties(model, entity, "created_at", "updated_at");
		entity.setLot(LotEntryController.getLotEntryEntity(model.getLot()));
		entity.setProgram(pentity);
	}
	
	private static void copyDetailsV2(DyeingProgmDetailModelV2 model, DyeingProgmDetailEntityV2 entity, DyeingProgmEntityV2 pentity) throws ParseException {
		BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
		BeanUtils.copyProperties(model, entity, "created_at", "updated_at");
		entity.setLot(LotEntryController.getLotEntryEntity(model.getLot()));
		entity.setProgram(pentity);
	}
	
}
