package com.erp.helper;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailUtil {

    final static String host = "smtp.gmail.com";
    final static String port = "587";
    final static String username = "dummydummy896@gmail.com";
    final static String password = "dummy@zoomba";
    
    public static boolean sendTxtEmail(String subject, String from, String[] to, String msg){
        
        boolean error = false;
        
        Session session = getEmailSession();
        
        try{  
            MimeMessage message = new MimeMessage(session);  
            message.setFrom(new InternetAddress(from));  
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(to[0]));  
            message.setSubject(subject);  
            message.setText(msg);  
     
            // Send message  
            Transport.send(message);  
            //System.out.println("message sent successfully....");  
     
         }catch (MessagingException mex) {
                 error = true;
                 mex.printStackTrace();
         }  
        
        return error;
    }
    
 public static boolean sendHtmlEmail(String subject, String from, String[] to, String msg){
        
        boolean error = false;
        
        Session session = getEmailSession();
        
        try{  
            MimeMessage message = new MimeMessage(session);  
            message.setFrom(new InternetAddress(from));  
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(to[0]));  
            message.setSubject(subject);  
            message.setText(msg, "utf-8", "html");  
     
            // Send message  
            Transport.send(message);  
            //System.out.println("message sent successfully....");  
     
         }catch (MessagingException mex) {
                 error = true;
                 mex.printStackTrace();
         }  
        
        return error;
    }
    
    private static Session getEmailSession(){
        Properties properties = System.getProperties();  
        properties.put("mail.smtp.host", host);  
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        
        //Session session = Session.getDefaultInstance(properties); 
        
        Session session = Session.getDefaultInstance(properties,  
                new javax.mail.Authenticator() { 
            
                  protected PasswordAuthentication getPasswordAuthentication() {  
                          return new PasswordAuthentication(username,password);  
                  }  
            }); 
        return  session;
    }
    
}
