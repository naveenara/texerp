package com.erp.helper;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.UserIO;
import com.fasterxml.jackson.databind.ObjectMapper;


public class ErpInterceptor extends HandlerInterceptorAdapter implements HandlerInterceptor{

	private static final String AUTHORIZATION_PROPERTY = "Authorization";
	
	ObjectMapper mapper = new ObjectMapper();
	
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //System.out.println("from pre handler : "+handler);
        String path = request.getRequestURI().substring(request.getContextPath().length());
        
        String token = request.getHeader(AUTHORIZATION_PROPERTY);
        
        if("OPTIONS".equals(request.getMethod()) || path.equals("/") || path.contains("/login") || 
        		path.contains("user/forgotpassword") || path.equals("/validateUser") 
        		|| path.equals("/adminLogin")
        		|| path.contains("/images")){
            return true;
        }
        
        if(path.endsWith(".js") || path.endsWith(".ico") || path.endsWith(".css") || path.endsWith(".scss")
        		|| path.endsWith(".ttf") || path.endsWith(".woff")) {
        	return true;
        }
        
        if(path.contains(".woff2") || path.contains(".woff")) {
        	return true;
        }
        
        String responseForInvalidRequest = validateToken(token, request);
        
        if(responseForInvalidRequest!=null) {
        	
        	ErpResponse<String> error = new ErpResponse<>();
        	
        	error.setCode("401");
        	error.setData("");
        	error.setError(true);
        	error.setMsg(responseForInvalidRequest);
             
        	response.setStatus(401);
        	response.setContentType("application/json");
        	response.setCharacterEncoding("UTF-8");
        	response.getWriter().write(mapper.writeValueAsString(error));
        	
        	response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
            response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
            
        	return false;
        }else {
        	return true;
        }
    }
    
    
    private String validateToken(String bearerToken, HttpServletRequest request){
        ErpResponse<String> error = null;
        if(bearerToken == null || Helper.isEmpty(bearerToken)){
        	return "Missing token in request, use Authorization: 'Bearer <token>'";
        }
        
        String[] authTokenArr = bearerToken.split(" ");
        
        if(authTokenArr.length != 2){
            return "Invalid token in request, use Authorization: 'Bearer <token>'";
        }
        
        String token = authTokenArr[1];
        
        if(Helper.isEmpty(token)){
            return "Empty token found in request";
        }
        
        Map<String, Object> result = JwtTokenUtils.validateToken(token);
        
        if((boolean) result.get("error")){
            return (String) result.get("msg");
        }
        
        UserIO data = (UserIO) result.get("data");
        request.setAttribute("user", data);
        
        return null;
    }
    

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //System.out.println("from post handler.");
        //throw new Error();
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //System.out.println("from after completion handler.");
    }

    
    
    
}
