package com.erp.helper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;

public class Helper {

    public static boolean isEmpty(String s){
        if(s == null || "".equals(s)){
            return true;
        }
        if("".equals(s.trim())){
            return true;
        }
        return false;
    }
    
    public static boolean isNotEmpty(String s){
        return !isEmpty(s);
    }
    
    public static boolean isNull(Object s){
        return s == null;
    }
    
    public static boolean isValid(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                            "[a-zA-Z0-9_+&*-]+)*@" +
                            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                            "A-Z]{2,7}$";
                             
        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }
    
    public static String getbase64Img(String picPath){
        File fi = new File(picPath);
        byte[] fileContent = null;
        try{
            fileContent = Files.readAllBytes(fi.toPath());
        }catch(IOException ioe){
            ioe.printStackTrace();
            return null;
        }
        String encodedFile = Base64.getEncoder().encodeToString(fileContent);
        String ext = FilenameUtils.getExtension(picPath);
        
        String imgBase64 = "data:image/"+ext+";base64,"+encodedFile;
        return imgBase64;
    }

    public static boolean isLong(String s){
        boolean error = true;
        try{
            Long.parseLong(s);
        }catch(Exception e){
            error = false;
        }
        return error;
    }
    
    public static boolean isInteger(String s){
        boolean error = true;
        try{
            Integer.parseInt(s);
        }catch(Exception e){
            error = false;
        }
        return error;
    }
    
    public static void assertMsg(boolean condition, String msg) {
    	if(condition) {
    		throw new IllegalArgumentException(msg);
    	}
    }
    
    public String getCurrentYearFormat() {

        Calendar cal = Calendar.getInstance();

        String yearFormat = "";

        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        DateFormat dateFormat = new SimpleDateFormat("yy");

        if (month <= 3) {
            yearFormat = cal.get(Calendar.YEAR) - 1 + "";
            yearFormat += cal.get(Calendar.YEAR);

            Date today = cal.getTime();

            String nextYearint = dateFormat.format(today);

            cal.add(Calendar.YEAR, -1);

            today = cal.getTime();
            String currentYearint = dateFormat.format(today);

            yearFormat = currentYearint + nextYearint;

        } else {
            Date today = cal.getTime();
            String currentYearint = dateFormat.format(today);
            cal.add(Calendar.YEAR, 1);
            today = cal.getTime();
            String nextYearint = dateFormat.format(today);

            yearFormat = currentYearint + nextYearint;
        }

        return yearFormat;
    }
    
    public String getNextVal(String initial, String lastVal, String yearFormat) {
        StringBuilder sb = new StringBuilder();
        sb.append(initial).append(yearFormat);

        if (lastVal == null || "null".equals(lastVal)) {
            sb.append("0001");
        } else {
            String oldVal = lastVal.substring(4);

            int j = 0;
            while (oldVal.charAt(j) == '0')
                j++;

            int increment = Integer.parseInt(oldVal.substring(j, oldVal.length())) + 1;
            sb.append(oldVal.substring(0, j)).append(increment);
        }
        
        String s = sb.toString();
        
        String s1 = s.substring(initial.length() + 4);
        
        if(s1.length() > 4){
            String s2 = s1.substring(1, s1.length());
            s = initial + yearFormat + s2;
        }
        
        return s;
    }
}
