package com.erp.helper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.springframework.beans.BeanUtils;

import com.erp.entity.InvoiceDetailEntity;
import com.erp.entity.InvoiceEntity;
import com.erp.exception.ApplicationException;
import com.erp.rest.model.InvoiceDetailModel;
import com.erp.rest.model.InvoiceModel;
import com.erp.rest.service.InvoiceService;
import com.erp.service.GenericService;

public class InvoiceHelper {
	
	public static InvoiceEntity getInvoice(InvoiceModel model, InvoiceService invoiceService, GenericService gservice) throws ParseException, ApplicationException {
		
		InvoiceEntity entity = new InvoiceEntity();
		if(model.getId()!=null)
			checkForDeletedInvoiceRecord(model, invoiceService, gservice);
		
		BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
    	BeanUtils.copyProperties(model, entity, "created_at", "updated_at", "details");
		
    	List<InvoiceDetailModel> details = model.getDetails();
    	List<InvoiceDetailEntity> entityDetails = new ArrayList<>(details.size());
    	
    	for(InvoiceDetailModel dmodel: details) {
    		InvoiceDetailEntity dentity = new InvoiceDetailEntity();
    		copyDetails(dmodel, dentity, entity);
    		if(dentity.getId() == null) {
    			dentity.setCreated_at(new Date());
    			dentity.setDeleted('0');
    		}else {
    			dentity.setUpdated_at(new Date());
    		}
    		entityDetails.add(dentity);
    	}
    	
    	entity.setDetails(entityDetails);
		return entity;
	}
	
	private static void checkForDeletedInvoiceRecord(InvoiceModel model, InvoiceService invoiceService, GenericService gservice) throws ApplicationException {
		List<InvoiceDetailEntity> details = invoiceService.get(model.getId()).getDetails();
		List<InvoiceDetailModel> modelDetails = model.getDetails();
		
		for(InvoiceDetailEntity de: details) {
			InvoiceDetailModel matchingObject = modelDetails.stream().
					filter(p -> de.getId() == p.getId()).
					findAny().orElse(null);
			if(matchingObject == null) {
				de.getPp().setInvoiceDone(false);
				gservice.update(de.getPp());
				gservice.delete(de);
			}
		}
	}
	
	private static void copyDetails(InvoiceDetailModel model, InvoiceDetailEntity entity, InvoiceEntity invoice) throws ParseException {
		BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
		BeanUtils.copyProperties(model, entity, "created_at", "updated_at");
		entity.setInvoice(invoice);
	}

}
