package com.erp.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.Key;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import com.erp.rest.model.UserIO;


public class JwtTokenUtils <E>{

    
    public static String getJwtToken(String payload){
        
        /*Key key = getprivateKey();
        
        if(key == null)
            return null;*/
        
        if(Helper.isEmpty(payload)){
            return null;
        }
        
        JwtBuilder builder = Jwts.builder()
                .setSubject(Constants.JWT_TOKEN_SUBJECT)
                //.setPayload(payload)
                .claim("payload", payload)
                .setExpiration(DateUtil.getTomorrowDate())
                .setIssuer(Constants.JWT_TOKEN_ISSUER)
                .signWith(SignatureAlgorithm.HS512, Constants.JWT_TOKEN_KEY);
        
        return builder.compact();
    }
    
    public static Map<String, Object> validateToken(String token){
        final ObjectMapper mapper = new ObjectMapper();
        Gson gson = new Gson();
        Map map = new HashMap<>();
        try {
            Claims claims = Jwts.parser().setSigningKey(Constants.JWT_TOKEN_KEY).parseClaimsJws(token).getBody();
            
            //mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
            //UserIO loginUser = mapper.convertValue(claims.get("payload"), UserIO.class);
            
            UserIO loginUser = gson.fromJson(claims.get("payload").toString(), UserIO.class);
            
            map.put("error", false);
            map.put("msg", null);
            map.put("data", loginUser);
            
            System.out.println("Token verified: "+token);
        } catch (SignatureException e) {
            //don't trust the JWT!
            e.printStackTrace();
            map.put("error", true);
            map.put("msg", "Invalid session, please login to continue.");
            
            System.out.println("Token NOT verified (Invalid): "+token);
        }catch(ExpiredJwtException e){
            map.put("error", true);
            map.put("msg", "Session is expired, please login again.");
            e.printStackTrace();
        }
        catch(Exception e){
            map.put("error", true);
            map.put("msg", "Invalid user token found, please login again.");
            e.printStackTrace();
        }
        return map;
    }
    
    public static <E> Map<String, Object> validateTokenGeneric(Class<E> e, String token){
        final ObjectMapper mapper = new ObjectMapper();
        Gson gson = new Gson();
        Map map = new HashMap<>();
        try {
            Claims claims = Jwts.parser().setSigningKey(Constants.JWT_TOKEN_KEY).parseClaimsJws(token).getBody();
            
            //mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
            //UserIO loginUser = mapper.convertValue(claims.get("payload"), UserIO.class);
            
            ObjectMapper objectMapper = new ObjectMapper();

            E data = (E) objectMapper.readValue(claims.get("payload").toString(), e);
            
            //E data = (E) gson.fromJson(claims.get("payload").toString(), e.getClass());
            
            map.put("error", false);
            map.put("msg", null);
            map.put("data", data);
            
            System.out.println("Token verified: "+token);
        } catch (SignatureException exp) {
            //don't trust the JWT!
            exp.printStackTrace();
            map.put("error", true);
            map.put("msg", "Invalid signature in token.");
            
            System.out.println("Token NOT verified (Invalid): "+token);
        }catch(ExpiredJwtException exp){
            map.put("error", true);
            map.put("msg", "Query is expired.");
            exp.printStackTrace();
        }
        catch(Exception exp){
            map.put("error", true);
            map.put("msg", "Invalid user token found.");
            exp.printStackTrace();
        }
        return map;
    }
    
    public static Key getprivateKey(){
        Key key = null;
        ObjectInputStream oin = null;
        
        try {
          oin = new ObjectInputStream(new FileInputStream(new File("private.key")));
          key = (Key) oin.readObject();
        }catch(Exception e){
            e.printStackTrace();
        }
        finally {
          try {
            oin.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }
        return key;
    }
}
