package com.erp.helper;

public class QueryObject<T> {

    private String colName;
    private T colVal;
    private String datatype;
    private String querytype;
    
    public QueryObject(){
        
    }
    public QueryObject(String colName, T colVal, String datatype, String querytype){
        this.colName = colName;
        this.colVal = colVal;
        this.datatype = datatype;
        this.querytype = querytype;
    }
    
    public String getColName() {
        return colName;
    }
    public void setColName(String colName) {
        this.colName = colName;
    }
    public T getColVal() {
        return colVal;
    }
    public void setColVal(T colVal) {
        this.colVal = colVal;
    }
    public String getDatatype() {
        return datatype;
    }
    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }
    public String getQuerytype() {
        return querytype;
    }
    public void setQuerytype(String querytype) {
        this.querytype = querytype;
    }
    
}
