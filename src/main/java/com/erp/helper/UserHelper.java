package com.erp.helper;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;

import com.erp.entity.UserEntity;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.UserIO;
import com.erp.rest.model.UserModel;
import com.erp.rest.service.UserService;
import com.erp.service.GenericService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class UserHelper {
    
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public static String randomString( int len ){
       StringBuilder sb = new StringBuilder( len );
       for( int i = 0; i < len; i++ ) 
          sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
       return sb.toString();
    }
    
    public static UserIO getUserTokenObject(UserEntity user){
        UserIO io = new UserIO();
        
        io.setId(user.getId());
        io.setEmail(user.getEmail());
        io.setName(user.getName());
        io.setUserName(user.getUsername());
        
        return io;
    }
    
    public static void validateUser(UserModel userModel, GenericService genericService, UserService userService) {
    	assertMsg(StringUtils.isEmpty(userModel.getName()), "Name is required");
    	assertMsg(StringUtils.isEmpty(userModel.getUsername()), "User Name is required");
    	assertMsg(StringUtils.isEmpty(userModel.getEmail()), "Email is required");
    	assertMsg(StringUtils.isEmpty(userModel.getPassword()), "Password is required");
    	assertMsg(!Helper.isValid(userModel.getEmail()), "Invalid email ID");
    	
    	if(userModel.getId() == null) {
    		assertMsg(genericService.getByField(new UserEntity(), "email", userModel.getEmail()) != null, "This email is already used");
        	assertMsg(genericService.getByField(new UserEntity(), "username", userModel.getUsername()) != null, "This username is already used");
    	}else {
    		UserEntity user = userService.getUserEdit(userModel.getUsername(), userModel.getEmail(), userModel.getId());
    		assertMsg(user!=null && userModel.getUsername().equalsIgnoreCase(user.getUsername()), "This username is already used");
    		assertMsg(user!=null && userModel.getEmail().equalsIgnoreCase(user.getEmail()), "This email is already used");
    	}
    }
    
    public static UserEntity getUserEntity(UserModel userModel) throws NoSuchAlgorithmException {
    	UserEntity user = new UserEntity();
    	
    	user.setDeleted(userModel.isActive() == true ? '0' : '1');
    	user.setEmail(userModel.getEmail());
    	user.setName(userModel.getName());
    	user.setUsername(userModel.getUsername());
    	
    	if(userModel.getId() == null) {
    		user.setCreated_at(new Date());
    		user.setPassword(EncrptionUtil.getSecurePassword(userModel.getPassword()));
    	}else {
    		user.setPassword(userModel.getPassword());
    		user.setId(userModel.getId());
    		user.setUpdated_at(new Date());
    	}
    	
    	return user;
    }
    
    public static UserModel getUserModel(UserEntity entity) throws NoSuchAlgorithmException {
    	UserModel user = new UserModel();
    	
    	user.setActive(entity.getDeleted() == '1' ? false : true);
    	user.setCreated_by(entity.getCreated_by());
    	user.setCreated_at(DateUtil.formateDateWithTime(entity.getCreated_at()));
    	user.setEmail(entity.getEmail());
    	user.setId(entity.getId());
    	user.setName(entity.getName());
    	user.setPassword(entity.getPassword());
    	user.setUsername(entity.getUsername());
    	user.setUpdated_at(entity.getUpdated_at() == null  ? "" : DateUtil.formateDateWithTime(entity.getUpdated_at()));
    	user.setUpdated_by(entity.getUpdated_by());
    	
    	return user;
    }
    
    public static void assertMsg(boolean condition, String msg) {
    	if(condition) {
    		throw new IllegalArgumentException(msg);
    	}
    }
    
    public static ResponseEntity<ErpResponse<String>> loginUser(UserModel userInput, UserService userService) {
    	
    	ErpResponse<String> response = new ErpResponse<>();
        
        boolean error = false;
        String msg = null;
        
        if(userInput == null){
            error = true;
            msg = "Invalid or missing username password.";
        }
        
        String username = userInput.getUsername();
        String password = userInput.getPassword();
        
        if(Helper.isEmpty(username) || Helper.isEmpty(password)){
            error = true;
            msg = "Missing username or password.";
        }
        
        if(error){
            response.setCode("401");
            response.setData("");
            response.setError(true);
            response.setMsg(msg);
            return new ResponseEntity<ErpResponse<String>>(response, HttpStatus.BAD_REQUEST);
        }
        
        String encPassword = null;
        UserEntity user = null;
        
        try {
            encPassword = EncrptionUtil.getSecurePassword(password.trim());
            user = userService.validateUser(username.trim(), encPassword);
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        
        if(user == null){
            error = true;
            msg = "Wrong username and password";
        }
        
        String jwtToken=null;
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        if(!error){
            try {
                jwtToken = JwtTokenUtils.getJwtToken(ow.writeValueAsString(UserHelper.getUserTokenObject(user)));
            } catch (JsonProcessingException e) {
                error = true;
                msg = "Error generating user session.";
                e.printStackTrace();
            }
        }
        
        if(error){
            response.setCode("401");
            response.setData("");
            response.setError(true);
            response.setMsg(msg);
            return new ResponseEntity<ErpResponse<String>>(response, HttpStatus.BAD_REQUEST);
        }
        
        response.setCode("200");
        response.setData(jwtToken);
        response.setError(false);
        response.setMsg(user.getName());
        
        return new ResponseEntity<ErpResponse<String>>(response, HttpStatus.OK);
    }
   
}
