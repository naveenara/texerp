package com.erp.rest.interceptor;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.erp.helper.Helper;
import com.erp.helper.JwtTokenUtils;
import com.erp.rest.model.ErpResponse;
import com.erp.rest.model.UserIO;

@Provider
public class AuthenticationFilter implements ContainerRequestFilter{
    
    @Context
    private ResourceInfo resourceInfo;
    
    @Context
    private transient HttpServletRequest servletRequest;
    
    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "Basic";
    private static final Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED)
                                                        .entity("You cannot access this resource").build();
    private static final Response ACCESS_FORBIDDEN = Response.status(Response.Status.FORBIDDEN)
                                                        .entity("Access blocked for all users !!").build();
    
    @Override
    public void filter(ContainerRequestContext requestContext)
    {
        final MultivaluedMap<String, String> headers = requestContext.getHeaders();
        
        final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);
        
        Response responseForInvalidRequest = validateToken(authorization, requestContext);
        
        String a = requestContext.getUriInfo().getRequestUri().getHost();
        int b = requestContext.getUriInfo().getRequestUri().getPort();
        String c = requestContext.getUriInfo().getRequestUri().getPath();
        String d =  requestContext.getUriInfo().getRequestUri().getUserInfo();
        
        Boolean isProtected = true;
        
        if(!Helper.isEmpty(c)){
            if(c.indexOf("/public/") > -1){
                isProtected = false;
            }
        }
        
        if(responseForInvalidRequest!=null && isProtected){
            requestContext.abortWith(responseForInvalidRequest);
        }
        
        if(isProtected){
            
        }
        
        //authorization.forEach(System.out::println);
        
    }
    
    private Response validateToken(List<String> authorization, ContainerRequestContext requestContext){
        ErpResponse<String> error = null;
        if(authorization==null || authorization.size() == 0){
            return null;
        }
        
        String bearerToken = authorization.get(0);
        
        if(Helper.isEmpty(bearerToken)){
            return null;
        }
        
        String[] authTokenArr = bearerToken.split(" ");
        
        if(authTokenArr.length != 2){
            error = new ErpResponse<String>("", true, "Invalid token in request, use Authorization: 'Bearer <token>'", "401");
            return Response.status(401).entity(error).build();
        }
        
        String token = authTokenArr[1];
        
        if(Helper.isEmpty(token)){
            error = new ErpResponse<String>("", true, "Empty token found in request", "401");
            return Response.status(401).entity(error).build();
        }
        
        Map<String, Object> result = JwtTokenUtils.validateToken(token);
        
        if((boolean) result.get("error")){
            error = new ErpResponse<String>("", true, (String) result.get("msg"), "401");
            return Response.status(401).entity(error).build();
        }
        
        UserIO data = (UserIO) result.get("data");
        servletRequest.setAttribute("user", data);
        
        return null;
    }
}
