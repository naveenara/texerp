package com.erp.rest.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
@EnableWebMvc
public class CorsConfiguration extends WebMvcConfigurationSupport
{
	@Override
	protected void addCorsMappings(CorsRegistry registry) {
	    //NOTE: servlet context set in "application.properties" is "/api" and request like "/api/session/login" resolves here to "/session/login"!
	    registry.addMapping("/**")
	        .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
	        .allowedOrigins("*")
	        .maxAge(-1)
	        .allowedHeaders("*")
	        .allowCredentials(false);
	    }
}