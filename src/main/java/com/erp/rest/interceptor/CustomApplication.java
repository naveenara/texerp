package com.erp.rest.interceptor;

import org.glassfish.jersey.server.ResourceConfig;

public class CustomApplication extends ResourceConfig{

    
    public CustomApplication()
    {
       // packages("org.fp");
 
        //Register Auth Filter here
    	//register(CORSFilter.class);
        register(AuthenticationFilter.class);
    }
    
}
