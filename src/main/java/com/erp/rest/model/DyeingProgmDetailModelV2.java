package com.erp.rest.model;

import com.erp.entity.DyeingProgmEntityV2;
import com.erp.entity.ShadeEntity;

public class DyeingProgmDetailModelV2 {

	private Long id;
	private ShadeEntity shade;
	private Integer balanceThan;
	private Integer addedThan;
	private Float length;
	private LotEntryModel lot;
	
	private DyeingProgmEntityV2 program;
    
    private String created_at;
	private String created_by;
	private String updated_at;
	private String updated_by;
	private Character deleted;
	private Boolean packingDone;
	private Integer batchNo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ShadeEntity getShade() {
		return shade;
	}
	public void setShade(ShadeEntity shade) {
		this.shade = shade;
	}
	public Integer getBalanceThan() {
		return balanceThan;
	}
	public void setBalanceThan(Integer balanceThan) {
		this.balanceThan = balanceThan;
	}
	public Integer getAddedThan() {
		return addedThan;
	}
	public void setAddedThan(Integer addedThan) {
		this.addedThan = addedThan;
	}
	public LotEntryModel getLot() {
		return lot;
	}
	public void setLot(LotEntryModel lot) {
		this.lot = lot;
	}
	public DyeingProgmEntityV2 getProgram() {
		return program;
	}
	public void setProgram(DyeingProgmEntityV2 program) {
		this.program = program;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public Character getDeleted() {
		return deleted;
	}
	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}
	public Boolean getPackingDone() {
		return packingDone;
	}
	public void setPackingDone(Boolean packingDone) {
		this.packingDone = packingDone;
	}
	public Float getLength() {
		return length;
	}
	public void setLength(Float length) {
		this.length = length;
	}
	public Integer getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(Integer batchNo) {
		this.batchNo = batchNo;
	}
	
}
