package com.erp.rest.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.erp.entity.PartyEntity;

public class DyeingProgmModel {

	private Long id;
	private String serialNo;
	private List<DyeingProgmDetailModel> details;
	private PartyEntity party;
	private Integer than;
	private Date programDate;
	private Boolean sent;
	
	private String created_at;
	private String created_by;
	private String updated_at;
	private String updated_by;
	private Character deleted;
	private String yearFormat;
	private String lotNo;
	
	private Integer shadeCount;
	
	private Map<Long, Integer> thansDebited;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public List<DyeingProgmDetailModel> getDetails() {
		return details;
	}
	public void setDetails(List<DyeingProgmDetailModel> details) {
		this.details = details;
	}
	public PartyEntity getParty() {
		return party;
	}
	public void setParty(PartyEntity party) {
		this.party = party;
	}
	public Integer getThan() {
		return than;
	}
	public void setThan(Integer than) {
		this.than = than;
	}
	public Date getProgramDate() {
		return programDate;
	}
	public void setProgramDate(Date programDate) {
		this.programDate = programDate;
	}
	public Boolean getSent() {
		return sent;
	}
	public void setSent(Boolean sent) {
		this.sent = sent;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public Character getDeleted() {
		return deleted;
	}
	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}
	public String getYearFormat() {
		return yearFormat;
	}
	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}
	public Map<Long, Integer> getThansDebited() {
		return thansDebited;
	}
	public void setThansDebited(Map<Long, Integer> thansDebited) {
		this.thansDebited = thansDebited;
	}
	public Integer getShadeCount() {
		return shadeCount;
	}
	public void setShadeCount(Integer shadeCount) {
		this.shadeCount = shadeCount;
	}
	public String getLotNo() {
		return lotNo;
	}
	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}
	
}
