package com.erp.rest.model;

public class ErpResponse<T> {

    private T data;
    private Boolean error;
    private String msg;
    private String code;
    
    public ErpResponse(){
        
    }
    public ErpResponse(T data, boolean error, String msg, String code){
        this.data = data;
        this.error = error;
        this.msg = msg;
        this.code = code;
    }
    
    public T getData() {
        return data;
    }
    public void setData(T data) {
        this.data = data;
    }
    public Boolean getError() {
        return error;
    }
    public void setError(Boolean error) {
        this.error = error;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    
    
}
