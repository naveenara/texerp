package com.erp.rest.model;

import java.util.Date;

public class FabricStockReportModel {

	private ShadeModel shade;
	private Integer thanFrom;
	private Integer thanTo;
	private String lotSerialNo;
	private String greySerialNo;
	private String biltyNo;
	private PartyModel party;
	private String lotNo;
	private MasterModel quality;
	private Date lotReceivedDateFrom;
	private Date lotReceivedDateTo;
	private String dyeingSerialNo;
	private Date programDateFrom;
	private Date programDateTo;
	private Boolean packingDone;
	private String criteria;
	
	public ShadeModel getShade() {
		return shade;
	}
	public void setShade(ShadeModel shade) {
		this.shade = shade;
	}
	public Integer getThanFrom() {
		return thanFrom;
	}
	public void setThanFrom(Integer thanFrom) {
		this.thanFrom = thanFrom;
	}
	public Integer getThanTo() {
		return thanTo;
	}
	public void setThanTo(Integer thanTo) {
		this.thanTo = thanTo;
	}
	public String getLotSerialNo() {
		return lotSerialNo;
	}
	public void setLotSerialNo(String lotSerialNo) {
		this.lotSerialNo = lotSerialNo;
	}
	public String getGreySerialNo() {
		return greySerialNo;
	}
	public void setGreySerialNo(String greySerialNo) {
		this.greySerialNo = greySerialNo;
	}
	public String getBiltyNo() {
		return biltyNo;
	}
	public void setBiltyNo(String biltyNo) {
		this.biltyNo = biltyNo;
	}
	public PartyModel getParty() {
		return party;
	}
	public void setParty(PartyModel party) {
		this.party = party;
	}
	public String getLotNo() {
		return lotNo;
	}
	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}
	public MasterModel getQuality() {
		return quality;
	}
	public void setQuality(MasterModel quality) {
		this.quality = quality;
	}
	public Date getLotReceivedDateFrom() {
		return lotReceivedDateFrom;
	}
	public void setLotReceivedDateFrom(Date lotReceivedDateFrom) {
		this.lotReceivedDateFrom = lotReceivedDateFrom;
	}
	public Date getLotReceivedDateTo() {
		return lotReceivedDateTo;
	}
	public void setLotReceivedDateTo(Date lotReceivedDateTo) {
		this.lotReceivedDateTo = lotReceivedDateTo;
	}
	public String getDyeingSerialNo() {
		return dyeingSerialNo;
	}
	public void setDyeingSerialNo(String dyeingSerialNo) {
		this.dyeingSerialNo = dyeingSerialNo;
	}
	public Date getProgramDateFrom() {
		return programDateFrom;
	}
	public void setProgramDateFrom(Date programDateFrom) {
		this.programDateFrom = programDateFrom;
	}
	public Date getProgramDateTo() {
		return programDateTo;
	}
	public void setProgramDateTo(Date programDateTo) {
		this.programDateTo = programDateTo;
	}
	public Boolean getPackingDone() {
		return packingDone;
	}
	public void setPackingDone(Boolean packingDone) {
		this.packingDone = packingDone;
	}
	public String getCriteria() {
		return criteria;
	}
	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}
	
}
