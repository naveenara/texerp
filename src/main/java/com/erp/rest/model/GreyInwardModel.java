package com.erp.rest.model;

import java.util.Date;

import com.erp.entity.MasterEntity;
import com.erp.entity.PartyEntity;

public class GreyInwardModel {

	private Long id;
	private Long voucherNo;
	private String serialNo;
	private String biltyNo;
	private Integer bales;
	private MasterEntity transport;
	private PartyEntity party;
	private String lotNo;
	private MasterEntity quality;
	private Integer than;
	private Double length;
	private MasterEntity unit;
	private Integer tp;
	private String notes;
	private Boolean lotDone;
	
	private String created_at;
	private String created_by;
	private String updated_at;
	private String updated_by;
	private Character deleted;
	private Date date;
	private String lrNo;
	private String yearFormat;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(Long voucherNo) {
		this.voucherNo = voucherNo;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getBiltyNo() {
		return biltyNo;
	}
	public void setBiltyNo(String biltyNo) {
		this.biltyNo = biltyNo;
	}
	public Integer getBales() {
		return bales;
	}
	public void setBales(Integer bales) {
		this.bales = bales;
	}
	public MasterEntity getTransport() {
		return transport;
	}
	public void setTransport(MasterEntity transport) {
		this.transport = transport;
	}
	public PartyEntity getParty() {
		return party;
	}
	public void setParty(PartyEntity party) {
		this.party = party;
	}
	public String getLotNo() {
		return lotNo;
	}
	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}
	public MasterEntity getQuality() {
		return quality;
	}
	public void setQuality(MasterEntity quality) {
		this.quality = quality;
	}
	public Integer getThan() {
		return than;
	}
	public void setThan(Integer than) {
		this.than = than;
	}
	public Double getLength() {
		return length;
	}
	public void setLength(Double length) {
		this.length = length;
	}
	public MasterEntity getUnit() {
		return unit;
	}
	public void setUnit(MasterEntity unit) {
		this.unit = unit;
	}
	public Integer getTp() {
		return tp;
	}
	public void setTp(Integer tp) {
		this.tp = tp;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public Character getDeleted() {
		return deleted;
	}
	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}
	public String getYearFormat() {
		return yearFormat;
	}
	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}
	public Boolean getLotDone() {
		return lotDone;
	}
	public void setLotDone(Boolean lotDone) {
		this.lotDone = lotDone;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getLrNo() {
		return lrNo;
	}
	public void setLrNo(String lrNo) {
		this.lrNo = lrNo;
	}
	
}
