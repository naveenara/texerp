package com.erp.rest.model;

import java.util.Date;

import com.erp.entity.MasterEntity;

public class GreyInwardReportModel {

	private Long voucherNo;
	private String serialNo;
	private String biltyNo;
	private Integer balesFrom;
	private Integer balesTo;
	private MasterEntity transport;
	private String party;
	private String lotNo;
	private MasterEntity quality;
	private Integer than;
	private Double lengthFrom;
	private Double lengthTo;
	private Date created_from;
	private Date created_to;
	private Integer tp;
	private String criteria;
	private Boolean lotDone;
	
	public Long getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(Long voucherNo) {
		this.voucherNo = voucherNo;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getBiltyNo() {
		return biltyNo;
	}
	public void setBiltyNo(String biltyNo) {
		this.biltyNo = biltyNo;
	}
	public Integer getBalesFrom() {
		return balesFrom;
	}
	public void setBalesFrom(Integer balesFrom) {
		this.balesFrom = balesFrom;
	}
	public Integer getBalesTo() {
		return balesTo;
	}
	public void setBalesTo(Integer balesTo) {
		this.balesTo = balesTo;
	}
	public MasterEntity getTransport() {
		return transport;
	}
	public void setTransport(MasterEntity transport) {
		this.transport = transport;
	}
	public String getParty() {
		return party;
	}
	public void setParty(String party) {
		this.party = party;
	}
	public String getLotNo() {
		return lotNo;
	}
	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}
	public MasterEntity getQuality() {
		return quality;
	}
	public void setQuality(MasterEntity quality) {
		this.quality = quality;
	}
	public Integer getThan() {
		return than;
	}
	public void setThan(Integer than) {
		this.than = than;
	}
	public Double getLengthFrom() {
		return lengthFrom;
	}
	public void setLengthFrom(Double lengthFrom) {
		this.lengthFrom = lengthFrom;
	}
	public Double getLengthTo() {
		return lengthTo;
	}
	public void setLengthTo(Double lengthTo) {
		this.lengthTo = lengthTo;
	}
	public Date getCreated_from() {
		return created_from;
	}
	public void setCreated_from(Date created_from) {
		this.created_from = created_from;
	}
	public Date getCreated_to() {
		return created_to;
	}
	public void setCreated_to(Date created_to) {
		this.created_to = created_to;
	}
	public Integer getTp() {
		return tp;
	}
	public void setTp(Integer tp) {
		this.tp = tp;
	}
	public String getCriteria() {
		return criteria;
	}
	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}
	public Boolean getLotDone() {
		return lotDone;
	}
	public void setLotDone(Boolean lotDone) {
		this.lotDone = lotDone;
	}
	
}
