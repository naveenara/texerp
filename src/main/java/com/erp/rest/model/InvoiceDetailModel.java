package com.erp.rest.model;

import com.erp.entity.ProcessPackingEntity;

public class InvoiceDetailModel {
	
	private Long id;
	private ProcessPackingEntity pp;
	private Float rate;
	private Float amount;
	
	private InvoiceModel invoice;
	
	private String created_at;
	private String created_by;
	private String updated_at;
	private String updated_by;
	private Character deleted;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ProcessPackingEntity getPp() {
		return pp;
	}
	public void setPp(ProcessPackingEntity pp) {
		this.pp = pp;
	}
	public Float getRate() {
		return rate;
	}
	public void setRate(Float rate) {
		this.rate = rate;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public InvoiceModel getInvoice() {
		return invoice;
	}
	public void setInvoice(InvoiceModel invoice) {
		this.invoice = invoice;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public Character getDeleted() {
		return deleted;
	}
	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}

}
