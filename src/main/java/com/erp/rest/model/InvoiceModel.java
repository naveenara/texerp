package com.erp.rest.model;

import java.util.Date;
import java.util.List;

import com.erp.entity.MasterEntity;
import com.erp.entity.PartyEntity;

public class InvoiceModel {

	private Long id;
	private String serialNo;
	private Date invoiceDate;
	private String ewayBillNo;
	private MasterEntity transport;
	private PartyEntity billTo;
	private PartyEntity shipTo;
	private String placeOfSupply;
	private Integer than;
	private Integer length;
	private Double amount;
	private String greyLrNo;
	private String partyLocation;
	
	private List<InvoiceDetailModel> details;
	
	private String created_at;
	private String created_by;
	private String updated_at;
	private String updated_by;
	private Character deleted;
	private String yearFormat;
	
	public String getGreyLrNo() {
		return greyLrNo;
	}
	public void setGreyLrNo(String greyLrNo) {
		this.greyLrNo = greyLrNo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getEwayBillNo() {
		return ewayBillNo;
	}
	public void setEwayBillNo(String ewayBillNo) {
		this.ewayBillNo = ewayBillNo;
	}
	public MasterEntity getTransport() {
		return transport;
	}
	public void setTransport(MasterEntity transport) {
		this.transport = transport;
	}
	public PartyEntity getBillTo() {
		return billTo;
	}
	public void setBillTo(PartyEntity billTo) {
		this.billTo = billTo;
	}
	public PartyEntity getShipTo() {
		return shipTo;
	}
	public void setShipTo(PartyEntity shipTo) {
		this.shipTo = shipTo;
	}
	public String getPlaceOfSupply() {
		return placeOfSupply;
	}
	public void setPlaceOfSupply(String placeOfSupply) {
		this.placeOfSupply = placeOfSupply;
	}
	public Integer getThan() {
		return than;
	}
	public void setThan(Integer than) {
		this.than = than;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public List<InvoiceDetailModel> getDetails() {
		return details;
	}
	public void setDetails(List<InvoiceDetailModel> details) {
		this.details = details;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public Character getDeleted() {
		return deleted;
	}
	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}
	public String getYearFormat() {
		return yearFormat;
	}
	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}
	public String getPartyLocation() {
		return partyLocation;
	}
	public void setPartyLocation(String partyLocation) {
		this.partyLocation = partyLocation;
	}
	
}
