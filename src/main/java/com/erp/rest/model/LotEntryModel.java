package com.erp.rest.model;

import java.util.Date;

import com.erp.entity.MasterEntity;

public class LotEntryModel {

	private Long id;
	private String serialNo;
	private GreyInwardModel grey;
	private Integer balance;
	private Integer received;
	private Float weight;
	private MasterEntity unit;
	
	private String created_at;
	private String created_by;
	private String updated_at;
	private String updated_by;
	private Character deleted;
	private Date receivedDate;
	private String yearFormat;
	private Boolean completed;
	
	private Boolean showAvailable;
	
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	
	public GreyInwardModel getGrey() {
		return grey;
	}
	public void setGrey(GreyInwardModel grey) {
		this.grey = grey;
	}
	
	public Integer getBalance() {
		return balance;
	}
	public void setBalance(Integer balance) {
		this.balance = balance;
	}
	public Integer getReceived() {
		return received;
	}
	public void setReceived(Integer received) {
		this.received = received;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public Character getDeleted() {
		return deleted;
	}
	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}
	public String getYearFormat() {
		return yearFormat;
	}
	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}
	
	public Date getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	public MasterEntity getUnit() {
		return unit;
	}
	public void setUnit(MasterEntity unit) {
		this.unit = unit;
	}
	public Boolean getShowAvailable() {
		return showAvailable;
	}
	public void setShowAvailable(Boolean showAvailable) {
		this.showAvailable = showAvailable;
	}
	public Boolean getCompleted() {
		return completed;
	}
	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}
	public Float getWeight() {
		return weight;
	}
	public void setWeight(Float weight) {
		this.weight = weight;
	}
	
}
