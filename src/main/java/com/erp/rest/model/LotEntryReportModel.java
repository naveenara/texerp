package com.erp.rest.model;

import java.util.Date;

public class LotEntryReportModel {

	private PartyModel party;
	private String lotNo;
	private String biltyNo;
	private String greySerialNo;
	private Integer thanFrom;
	private Integer thanTo;
	private String lotSerialNo;
	private Date lotReceivedDateFrom;
	private Date lotReceivedDateTo;
	private MasterModel quality;
	private Boolean lotDone;
	private String criteria;
	
	public PartyModel getParty() {
		return party;
	}
	public void setParty(PartyModel party) {
		this.party = party;
	}
	public String getLotNo() {
		return lotNo;
	}
	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}
	public String getBiltyNo() {
		return biltyNo;
	}
	public void setBiltyNo(String biltyNo) {
		this.biltyNo = biltyNo;
	}
	public String getGreySerialNo() {
		return greySerialNo;
	}
	public void setGreySerialNo(String greySerialNo) {
		this.greySerialNo = greySerialNo;
	}
	public Integer getThanFrom() {
		return thanFrom;
	}
	public void setThanFrom(Integer thanFrom) {
		this.thanFrom = thanFrom;
	}
	public Integer getThanTo() {
		return thanTo;
	}
	public void setThanTo(Integer thanTo) {
		this.thanTo = thanTo;
	}
	public String getLotSerialNo() {
		return lotSerialNo;
	}
	public void setLotSerialNo(String lotSerialNo) {
		this.lotSerialNo = lotSerialNo;
	}
	public Date getLotReceivedDateFrom() {
		return lotReceivedDateFrom;
	}
	public void setLotReceivedDateFrom(Date lotReceivedDateFrom) {
		this.lotReceivedDateFrom = lotReceivedDateFrom;
	}
	public Date getLotReceivedDateTo() {
		return lotReceivedDateTo;
	}
	public void setLotReceivedDateTo(Date lotReceivedDateTo) {
		this.lotReceivedDateTo = lotReceivedDateTo;
	}
	public MasterModel getQuality() {
		return quality;
	}
	public void setQuality(MasterModel quality) {
		this.quality = quality;
	}
	public Boolean getLotDone() {
		return lotDone;
	}
	public void setLotDone(Boolean lotDone) {
		this.lotDone = lotDone;
	}
	public String getCriteria() {
		return criteria;
	}
	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}
	
}
