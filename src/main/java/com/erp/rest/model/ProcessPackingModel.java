package com.erp.rest.model;

import java.util.Date;

import com.erp.entity.DyeingProgmDetailEntityV2;
import com.erp.entity.MasterEntity;

public class ProcessPackingModel {
	private Long id;
	private String serialNo;
	private DyeingProgmDetailEntityV2 detail;
	private Double length;
	private MasterEntity unit;
	private MasterEntity process;
	private String lrNo;
	private String fold;
	private String notes;
	private Date deliveryDate;
	private Long detailId;
	private Long baleNo;
	
	private Date created_at;
	private String created_by;
	private Date updated_at;
	private String updated_by;
	private Character deleted;
	private String yearFormat;
	private Boolean invoiceDone;
	
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	
	public DyeingProgmDetailEntityV2 getDetail() {
		return detail;
	}
	public void setDetail(DyeingProgmDetailEntityV2 detail) {
		this.detail = detail;
	}
	public Double getLength() {
		return length;
	}
	public void setLength(Double length) {
		this.length = length;
	}
	public MasterEntity getUnit() {
		return unit;
	}
	public void setUnit(MasterEntity unit) {
		this.unit = unit;
	}
	public MasterEntity getProcess() {
		return process;
	}
	public void setProcess(MasterEntity process) {
		this.process = process;
	}
	public String getLrNo() {
		return lrNo;
	}
	public void setLrNo(String lrNo) {
		this.lrNo = lrNo;
	}
	public String getFold() {
		return fold;
	}
	public void setFold(String fold) {
		this.fold = fold;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Long getDetailId() {
		return detailId;
	}
	public void setDetailId(Long detailId) {
		this.detailId = detailId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public Character getDeleted() {
		return deleted;
	}
	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}
	public String getYearFormat() {
		return yearFormat;
	}
	public void setYearFormat(String yearFormat) {
		this.yearFormat = yearFormat;
	}
	public Long getBaleNo() {
		return baleNo;
	}
	public void setBaleNo(Long baleNo) {
		this.baleNo = baleNo;
	}
	public Boolean getInvoiceDone() {
		return invoiceDone;
	}
	public void setInvoiceDone(Boolean invoiceDone) {
		this.invoiceDone = invoiceDone;
	}

}
