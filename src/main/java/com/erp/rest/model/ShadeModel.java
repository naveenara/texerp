package com.erp.rest.model;

import java.util.ArrayList;
import java.util.List;

import com.erp.entity.MasterEntity;
import com.erp.entity.PartyEntity;
import com.erp.entity.ShadeDetailEntity;

public class ShadeModel {

	private Long id;
	private String name;
	private PartyEntity party;
	private MasterEntity color;
	private Character deleted;
	private String created_by;
	private String created_at;
	private String updated_at;
	private String updated_by;
	//private Set<MasterEntity> composition = new HashSet<MasterEntity>();
	private List<ShadeDetailEntity> details = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public PartyEntity getParty() {
		return party;
	}
	public void setParty(PartyEntity party) {
		this.party = party;
	}
	public MasterEntity getColor() {
		return color;
	}
	public void setColor(MasterEntity color) {
		this.color = color;
	}
	public Character getDeleted() {
		return deleted;
	}
	public void setDeleted(Character deleted) {
		this.deleted = deleted;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public List<ShadeDetailEntity> getDetails() {
		return details;
	}
	public void setDetails(List<ShadeDetailEntity> details) {
		this.details = details;
	}
	
}
