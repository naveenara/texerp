package com.erp.rest.model.report;

import java.util.Date;

import com.erp.rest.model.PartyModel;

public class PartyLotWiseModel {

	private Date from;
	private Date to;
	private PartyModel partyModel;
	private String reportType;
	
	public Date getFrom() {
		return from;
	}
	public void setFrom(Date from) {
		this.from = from;
	}
	public Date getTo() {
		return to;
	}
	public void setTo(Date to) {
		this.to = to;
	}
	public PartyModel getPartyModel() {
		return partyModel;
	}
	public void setPartyModel(PartyModel partyModel) {
		this.partyModel = partyModel;
	}
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	
}
