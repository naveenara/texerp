package com.erp.rest.model.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.erp.entity.LotEntryEntity;

public class PartyLotWiseResponseModel {

	private String partyName;
	private Long partyId;
	private Integer than;
	private Integer sent;
	private Integer balance;
	
	private List<Details> details;
	
	public PartyLotWiseResponseModel(){
		details = new ArrayList<>();
	}
	
	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}



	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public Integer getThan() {
		return than;
	}

	public void setThan(Integer than) {
		this.than = than;
	}

	public Integer getSent() {
		return sent;
	}

	public void setSent(Integer sent) {
		this.sent = sent;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public List<Details> getDetails() {
		return details;
	}

	public void setDetails(List<Details> details) {
		this.details = details;
	}
	
	public void addDetail(LotEntryEntity lot) {
		Details detail = new Details();
		
		detail.setGreyNo(lot.getGrey().getSerialNo());
		detail.setLotDate(lot.getReceivedDate());
		detail.setBalance(lot.getBalance() == null ? 0 : lot.getBalance());
		detail.setLotNo(lot.getGrey().getLotNo());
		detail.setLrNo(lot.getGrey().getLrNo() == null ? "-" : lot.getGrey().getLrNo());
		detail.setQuality(lot.getGrey().getQuality() != null ? lot.getGrey().getQuality().getName(): "-");
		detail.setSent(lot.getReceived() - lot.getBalance());
		detail.setThan(lot.getReceived() == null ? 0 : lot.getReceived());
		
		details.add(detail);
	}
 }


class Details{
	
	private String greyNo;
	private Date lotDate;
	private String lotNo;
	private String quality;
	private String lrNo;
	private Integer than;
	private Integer sent;
	private Integer balance;
	public String getGreyNo() {
		return greyNo;
	}
	public void setGreyNo(String greyNo) {
		this.greyNo = greyNo;
	}
	public Date getLotDate() {
		return lotDate;
	}
	public void setLotDate(Date lotDate) {
		this.lotDate = lotDate;
	}
	public String getLotNo() {
		return lotNo;
	}
	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}
	public String getLrNo() {
		return lrNo;
	}
	public void setLrNo(String lrNo) {
		this.lrNo = lrNo;
	}
	public Integer getThan() {
		return than;
	}
	public void setThan(Integer than) {
		this.than = than;
	}
	public Integer getSent() {
		return sent;
	}
	public void setSent(Integer sent) {
		this.sent = sent;
	}
	public Integer getBalance() {
		return balance;
	}
	public void setBalance(Integer balance) {
		this.balance = balance;
	}
	
}