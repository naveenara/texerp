package com.erp.rest.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ChartsService {

	public <E> List<Object[]> getDayWiseCount(E e, Date tillDate, String table);
	public Map<String, Integer> getTaskChart(Date tillDate);
	
}
