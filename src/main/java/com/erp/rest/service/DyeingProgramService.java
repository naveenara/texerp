package com.erp.rest.service;

import java.util.List;

import com.erp.entity.DyeingProgmDetailEntity;
import com.erp.entity.DyeingProgmDetailEntityV2;
import com.erp.entity.DyeingProgmEntity;
import com.erp.entity.DyeingProgmEntityV2;

public interface DyeingProgramService {
	
	public DyeingProgmEntity getDyeingProgram(Long id);
	public List<DyeingProgmDetailEntity> getDetails(Long dpId);
	
	public DyeingProgmEntityV2 getDyeingProgramV2(Long id);
	public List<DyeingProgmDetailEntityV2> getDetailsV2(Long dpId);
	public List<DyeingProgmDetailEntityV2> searchProgramForPacking(Long partyId, String qry);

}
