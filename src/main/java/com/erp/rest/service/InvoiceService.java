package com.erp.rest.service;

import java.util.List;

import com.erp.entity.InvoiceDetailEntity;
import com.erp.entity.InvoiceEntity;

public interface InvoiceService {

	public InvoiceEntity get(Long id);
	public List<InvoiceDetailEntity> getDetails(Long dpId);
}
