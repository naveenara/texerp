package com.erp.rest.service;

import com.erp.entity.UserEntity;

public interface UserService {

	public UserEntity validateUser(String userName, String password);
	
	public UserEntity getUserEdit(String username, String email, Long id);
}
