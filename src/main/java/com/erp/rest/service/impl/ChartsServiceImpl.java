package com.erp.rest.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.erp.dao.ChartsDao;
import com.erp.rest.service.ChartsService;

@Service("chartsService")
public class ChartsServiceImpl implements ChartsService{

	@Resource
	private ChartsDao chartsDao;
	
	@Transactional
	@Override
	public <E> List<Object[]> getDayWiseCount(E e, Date tillDate, String table){
		return chartsDao.getDayWiseCount(e, tillDate, table);
	}

	@Override
	@Transactional
	public Map<String, Integer> getTaskChart(Date tillDate) {
		return chartsDao.getTaskChart(tillDate);
	}
	
}
