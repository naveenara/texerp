package com.erp.rest.service.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.erp.dao.DyeingProgramDao;
import com.erp.entity.DyeingProgmDetailEntity;
import com.erp.entity.DyeingProgmDetailEntityV2;
import com.erp.entity.DyeingProgmEntity;
import com.erp.entity.DyeingProgmEntityV2;
import com.erp.rest.service.DyeingProgramService;

@Service("dpService")
public class DyeingProgramServiceImpl implements DyeingProgramService{

	@Resource
	private DyeingProgramDao dpDao;
	
	@Override
	@Transactional
	public DyeingProgmEntity getDyeingProgram(Long id) {
		return dpDao.getDyeingProgram(id);
	}

	@Override
	@Transactional
	public List<DyeingProgmDetailEntity> getDetails(Long dpId) {
		return dpDao.getDetails(dpId);
	}

	
	
	
	@Override
	@Transactional
	public DyeingProgmEntityV2 getDyeingProgramV2(Long id) {
		return dpDao.getDyeingProgramV2(id);
	}

	@Override
	@Transactional
	public List<DyeingProgmDetailEntityV2> getDetailsV2(Long dpId) {
		return dpDao.getDetailsV2(dpId);
	}

	@Override
	@Transactional
	public List<DyeingProgmDetailEntityV2> searchProgramForPacking(Long partyId, String qry) {
		return dpDao.searchProgramForPacking(partyId, qry);
	}
}
