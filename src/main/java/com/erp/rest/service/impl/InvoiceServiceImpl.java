package com.erp.rest.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erp.dao.InvoiceDao;
import com.erp.entity.InvoiceDetailEntity;
import com.erp.entity.InvoiceEntity;
import com.erp.rest.service.InvoiceService;

@Service("invoiceService")
public class InvoiceServiceImpl implements InvoiceService{

	@Autowired
	private InvoiceDao invoiceDao;
	
	@Transactional
	@Override
	public InvoiceEntity get(Long id) {
		return this.invoiceDao.get(id);
	}

	@Transactional
	@Override
	public List<InvoiceDetailEntity> getDetails(Long dpId) {
		return this.invoiceDao.getDetails(dpId);
	}

}
