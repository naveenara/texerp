package com.erp.rest.service.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.erp.dao.ProcessPackingDao;
import com.erp.entity.ProcessPackingEntityV2;
import com.erp.rest.service.ProcessPackingService;

@Service("ppService")
public class ProcessPackingServiceImpl implements ProcessPackingService{

	@Resource
	private ProcessPackingDao ppDao;
	
	@Transactional
	@Override
	public List<Object[]> searchDyeingDetails(String qry) {
		return ppDao.searchDyeingDetails(qry);
	}

	@Transactional
	@Override
	public Long getNextBaleNo() {
		return ppDao.getNextBaleNo();
	}

	@Transactional
	@Override
	public List<Object[]> searchProcessPacking(String qry, Long partyId) {
		return ppDao.searchProcessPacking(qry, partyId);
	}

	@Transactional
	@Override
	public ProcessPackingEntityV2 get(Long id) {
		return ppDao.get(id);
	}

}
