package com.erp.rest.service.impl;

import java.text.ParseException;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.erp.dao.ReportsDao;
import com.erp.entity.DyeingProgmDetailEntity;
import com.erp.entity.GreyInwardEntity;
import com.erp.entity.LotEntryEntity;
import com.erp.rest.model.FabricStockReportModel;
import com.erp.rest.model.GreyInwardReportModel;
import com.erp.rest.model.LotEntryModel;
import com.erp.rest.model.LotEntryReportModel;
import com.erp.rest.model.report.PartyLotWiseModel;
import com.erp.rest.service.ReportsService;

@Service("reportsService")
public class ReportsServiceImpl implements ReportsService{

	@Resource
	private ReportsDao reportsDao;
	
	@Override
	@Transactional
	public List<GreyInwardEntity> searchGreyInward(GreyInwardReportModel report) throws ParseException {
		return reportsDao.searchGreyInward(report);
	}

	@Override
	@Transactional
	public List<LotEntryEntity> searchLotEntry(LotEntryModel model) throws ParseException {
		return reportsDao.searchLotEntry(model);
	}

	@Override
	@Transactional
	public List<DyeingProgmDetailEntity> searchDyeingDetailsReport(FabricStockReportModel report) throws ParseException {
		return reportsDao.searchDyeingDetailsReport(report);
	}

	@Override
	@Transactional
	public List<LotEntryEntity> searchLotEntryReport(LotEntryReportModel report) throws ParseException {
		return reportsDao.searchLotEntryReport(report);
	}

	@Override
	@Transactional
	public List<LotEntryEntity> getPartyLotWise(PartyLotWiseModel model) {
		return reportsDao.getPartyLotWise(model);
	}

	@Transactional
	@Override
	public void deleteUserReportData(Long rtype, Long typeId, Long userId) {
		reportsDao.deleteUserReportData(rtype, typeId, userId);
	}

}
