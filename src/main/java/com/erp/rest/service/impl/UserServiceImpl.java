package com.erp.rest.service.impl;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.erp.dao.UserDao;
import com.erp.entity.UserEntity;
import com.erp.rest.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Resource
	private UserDao userDao;
	
	@Transactional
	@Override
	public UserEntity validateUser(String userName, String password) {
		return userDao.validateUser(userName, password);
	}

	@Override
	@Transactional
	public UserEntity getUserEdit(String username, String email, Long id) {
		return userDao.getUserEdit(username, email, id);
	}

}
