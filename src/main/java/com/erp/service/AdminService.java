package com.erp.service;

import java.util.Collection;

import com.erp.entity.DepartmentEntity;

public interface AdminService {
    
    public Collection<DepartmentEntity> searchDept(DepartmentEntity obj);

}
