package com.erp.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.erp.dao.AdminDao;
import com.erp.entity.DepartmentEntity;
import com.erp.service.AdminService;

@Service("adminService")
@Transactional
public class AdminServiceImpl implements AdminService{

    @Autowired
    private AdminDao adminDao;
    
    @Override
    public Collection<DepartmentEntity> searchDept(DepartmentEntity obj) {
        return adminDao.searchDept(obj);
    }

}
