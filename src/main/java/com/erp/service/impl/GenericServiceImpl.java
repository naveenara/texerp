package com.erp.service.impl;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.erp.dao.GenericDao;
import com.erp.entity.MasterEntity;
import com.erp.entity.ShadeEntity;
import com.erp.exception.ApplicationException;
import com.erp.helper.QueryObject;
import com.erp.service.GenericService;

@EnableTransactionManagement
@Service("genericService")
public class GenericServiceImpl implements GenericService{

    @Autowired
    private GenericDao gdao;
    
    @Transactional
    @Override
    public <E> void add(E e) throws ApplicationException {
       this.gdao.add(e);
    }

    @Transactional
    @Override
    public <E> void saveOrUpdate(E e) throws ApplicationException{
    	this.gdao.saveOrUpdate(e);
    }
    
    @Transactional
    @Override
    public <E> void delete(final Class<E> type,Long id) throws ApplicationException {
        this.gdao.delete(type, id);
    }
    
    @Transactional
    @Override
    public <E> void delete(final Object object) throws ApplicationException{
        this.gdao.delete(object);
    }

    @Transactional
    @Override
    public <E> E findOne(final Class<E> type, final Long id) throws ApplicationException {
        return this.gdao.findOne(type, id);
    }

    @Transactional
    @Override
    public <E> void update(E e) {
        this.gdao.update(e);
    }

    @Transactional
    @Override
    public <E> List<E> getAll(final Class<E> type) {
        return this.gdao.getAll(type);
    }

    @Transactional
    @Override
    public <E> List<E> getAll(final Class<E> type, String colName, Character value){
    	return this.gdao.getAll(type, colName, value);
    }
    
    @Transactional
    @Override
    public <E> List<Object[]> searchAndRestrict(final Class<E> type, List<String> restriction, String value, List<String> projection){
    	return this.gdao.searchAndRestrict(type, restriction, value, projection);
    }
    
    @Transactional
    @Override
    public List<MasterEntity> searchMaster(String qry, Character type){
    	return this.gdao.searchMaster(qry, type);
    }
    
    @Transactional
    @Override
    public <E> String getNextNumber(String yearFormat, String tableName, int length) {
        return this.gdao.getNextNumber(yearFormat, tableName, length);
    }
    
    @Transactional
    @Override
    public <E> String getNextNumber(String yearFormat, String tableName, int length, String type){
        return this.gdao.getNextNumber(yearFormat, tableName, length, type);
    }

    @Transactional
    @Override
    public Long recordsTotal(String entity) {
        return this.gdao.recordsTotal(entity);
    }

    @Transactional
    @Override
    public <E> Integer getFilteredRecords(E type, String qry, int length, int start, String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition) throws HibernateException, JSONException {
        return this.gdao.getFilteredRecords(type, qry, length, start, entityAlias, entitySubAlias, likeCondition);
    }

    @Transactional
    @Override
    public <E> List<E> filterResult(E type, String qry, int length, int start, String sortCol, String sortDir, String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition) throws HibernateException, JSONException {
        return this.gdao.filterResult(type, qry, length, start, sortCol, sortDir, entityAlias, entitySubAlias, likeCondition);
    }

    @Transactional
    @Override
    public <E> List<E> searchGeneric(E type) throws IllegalArgumentException, IllegalAccessException{
    	return this.gdao.searchGeneric(type);
    }

    @Transactional
    @Override
    public <E> List<String> getOneColumn(E type, String colName){
        return this.gdao.getOneColumn(type, colName);
    }
    
    @Transactional
    @Override
    public <E> E getPrevEntry(E type, String docNo){
        return this.gdao.getPrevEntry(type, docNo);
    }


    @Transactional
    @Override
    public <E> List<String> getOneColumn(E type, String colName, Integer master_type){
        return this.gdao.getOneColumn(type, colName, master_type);
    }

    @Transactional
    @Override
    public void setBeamStatus(Integer jobCardId, String prop){
        this.gdao.setBeamStatus(jobCardId, prop);
    }

    @Transactional
    @Override
    public <E> List<String> getOneColumn(E type, String inp, String property){
        return this.gdao.getOneColumn(type, inp, property);
    }

    @Transactional
    @Override
    public <E> E addReturnId(E e){
        return this.gdao.addReturnId(e);
    }

    @Transactional
    @Override
    public <E> E getByName(E type, String name){
        return this.gdao.getByName(type, name);
    }

    @Transactional
    @Override
    public <E> E getByField(E type, String field, String value){
        return this.gdao.getByField(type, field, value);
    }

    @Transactional
    @Override
    public <E> E getByField(E type, String field, Integer value){
        return this.gdao.getByField(type, field, value);
    }

    @Transactional
    @Override
    public int deleteMultiple(String entity, Set<Integer> ids){
       return this.gdao.deleteMultiple(entity, ids);
    }
    
    /*@Transactional
    @Override
    public <E, T> boolean deleteva(String entity, String id, List<T> parents2, List<String> aliasName){
        
       return this.gdao.deleteva(entity, id, parents2, aliasName);
    }*/
    
    @Transactional
    @Override
    public <E, T> boolean deleteva(E entity, String id, List<T> listToCheck,  List<String> aliasName){
        
         return this.gdao.deleteva(entity, id, listToCheck, aliasName);
        
    }

    @Transactional
    @Override
    public <E> E findLongOne(E e, Long id) {
        
         return this.gdao.findLongOne(e, id);
    }

    @Override
    @Transactional
    public Long recordsTotal(String entity, String type, String value) {
        return this.gdao.recordsTotal(entity, type, value);
    }

    @Override
    @Transactional
    public <E> Integer getFilteredRecords(E type, String qry1, int length, int start, String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition, String colName, String value) throws HibernateException, JSONException {
        return this.gdao.getFilteredRecords(type, qry1, length, start, entityAlias, entitySubAlias, likeCondition, colName, value);
    }

    @Override
    @Transactional
    public <E> List<E> filterResult(E type, String qry1, int length, int start, String sortCol, String sortDir, String entityAlias, JSONArray entitySubAlias, JSONArray likeCondition, String colName, String value) throws HibernateException, JSONException {
        return this.gdao.filterResult(type, qry1, length, start, sortCol, sortDir, entityAlias, entitySubAlias, likeCondition, colName, value);
    }

    @Override
    @Transactional
    public <E> String getNextNumber(String colName, String yearFormat, String tableName, int length){
        return this.gdao.getNextNumber(colName, yearFormat, tableName, length);
    }

    @Override
    @Transactional
    public <E> E findByCode(Class<E> type, String code) throws ApplicationException {
        return this.gdao.findByCode(type, code);
    }

	@Override
	@Transactional
	public <E> List<Double> getOneColumn(E type, Double inp, String property) {
		return this.gdao.getOneColumn(type, inp, property);
	}

	@Override
	@Transactional
	public <E> List<Object[]> getDateOneRow(E type, Long id, List<String> fields) {
		return this.gdao.getDateOneRow(type, id, fields);
	}

	@Override
	@Transactional
	public <E> List<E> findByFields(Class<E> type, List<QueryObject> query, String alias, List<String> subAliasList) throws ApplicationException {
		return this.gdao.findByFields(type, query, alias, subAliasList);
	}

	@Override
	@Transactional
	public ShadeEntity getShade(Long id) {
		return this.gdao.getShade(id);
	}
}
    

